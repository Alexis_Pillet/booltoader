/***************************************************************************//**
 * @file btl_config.h
 * @brief Configuration header for EFM32/EFR32 bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef BTL_CONFIG_H
#define BTL_CONFIG_H

#include "em_device.h"

// --------------------------------
// Bootloader Version

#define BOOTLOADER_VERSION_FIRST_STAGE_MAJOR      0
#define BOOTLOADER_VERSION_FIRST_STAGE_MINOR      1
#define BOOTLOADER_VERSION_FIRST_STAGE_PATCH      0
#define BOOTLOADER_VERSION_FIRST_STAGE_CUSTOMER   0

#define BOOTLOADER_VERSION_SECOND_STAGE_MAJOR     0
#define BOOTLOADER_VERSION_SECOND_STAGE_MINOR     1
#define BOOTLOADER_VERSION_SECOND_STAGE_PATCH     0
#define BOOTLOADER_VERSION_SECOND_STAGE_CUSTOMER  0

// --------------------------------
// Bootloader Memory Layout
#define BTL_FIRST_STAGE_BASE              0x0UL
#define BTL_FIRST_STAGE_SIZE              0x800UL

#define WITH_ONE_STAGE_BOOTLOADER         // Use only the second stage bootloader

#ifndef WITH_ONE_STAGE_BOOTLOADER
#define BTL_SECOND_STAGE_BASE             (BTL_FIRST_STAGE_BASE + BTL_FIRST_STAGE_SIZE)
#define BTL_SECOND_STAGE_SIZE             (0x4000 - BTL_FIRST_STAGE_SIZE)
#else
#define BTL_SECOND_STAGE_BASE             0x0UL
#define BTL_SECOND_STAGE_SIZE             0x4000UL
#endif

#define BTL_UPGRADE_LOCATION_BASE         (BTL_SECOND_STAGE_BASE + BTL_SECOND_STAGE_SIZE)

#define BTL_APP_SPACE_BASE                0x4000
#define BTL_APP_SPACE_SIZE                (FLASH_SIZE - BTL_APP_SPACE_BASE)

#define BTL_ALLOW_DOWNGRADE               (1)

// --------------------------------
// EBL Image parser configuration
#define BTL_EBL_PARSER_ENCRYPTED_ONLY     (0)
#define BTL_EBL_PARSER_SIGNED_ONLY        (1)

// --------------------------------
// Debug plugin configuration

// Enable debug printing
#define BTL_PLUGIN_DEBUG_PRINT_no

// Enable UART debug print @ 115200-8-N-1
#define BTL_DEBUG_PRINT_UART_no

#define BTL_DEBUG_UART USART0
#define BTL_DEBUG_UART_CLOCK CMU_HFPERCLKEN0_USART0

// Use TX location 0
#define BTL_DEBUG_UART_TX_LOCATION 0
#define BTL_DEBUG_UART_TX_PORT 0
#define BTL_DEBUG_UART_TX_PIN  0

// Use enable pin
#define BTL_DEBUG_UART_EN_PORT 0
#define BTL_DEBUG_UART_EN_PIN  5

// Enable SWO debug print
//#define BTL_DEBUG_PRINT_SWO

// Enable LED debug of failure
#define BTL_PLUGIN_DEBUG_FAILURE
#define BTL_DEBUG_FAILURE_LED

#define BTL_DEBUG_LED_PORT    gpioPortB
#define BTL_DEBUG_LED_PIN     12


#define BTL_GREEN_LEDR         0x01
#define BTL_GREEN_LEDR_PORT    gpioPortB
#define BTL_GREEN_LEDR_PIN     12

#define BTL_RED_LEDR           0x02
#define BTL_RED_LEDR_PORT      gpioPortB
#define BTL_RED_LEDR_PIN       11

#define BTL_GREEN_LEDM         0x04
#define BTL_GREEN_LEDM_PORT    gpioPortA
#define BTL_GREEN_LEDM_PIN     2

#define BTL_RED_LEDM           0x08
#define BTL_RED_LEDM_PORT      gpioPortA
#define BTL_RED_LEDM_PIN       4

// --------------------------------
// Storage plugin configuration

#define BOOTLOADER_SUPPORT_STORAGE
#define BOOTLOADER_SUPPORT_RAW_STORAGE

// Enable spiflash storage plugin
#define BTL_PLUGIN_STORAGE_SPIFLASH

// Define number of slots and their layout
#define BTL_PLUGIN_STORAGE_NUM_SLOTS 3
#define BTL_PLUGIN_STORAGE_SLOTS            \
{                                           \
  {                                         \
    0x40000,      /* Start address */       \
    0x40000,      /* Slot size */           \
  },                                        \
  {                                         \
    0x80000,      /* Start address */       \
    0x40000,      /* Slot size */           \
  },                                        \
  {                                         \
    0xC0000,      /* Start address */       \
    0x40000,      /* Slot size */           \
  }                                         \
}

// Enable Macronix 8Mb Low Power flash
#define BTL_STORAGE_SPIFLASH_MACRONIX_MX25R8035F

// Custom init and shutdown commands, e.g. setting power pin etc
#define BTL_STORAGE_CUSTOM_INIT() do {      \
} while (0);
#define BTL_STORAGE_CUSTOM_SHUTDOWN() do {  \
} while (0);

#define BTL_STORAGE_READ_BUFFER_SIZE                    (256u)
#define BTL_STORAGE_VERIFICATION_CONTEXT_SIZE           (312) // Size of BootloaderVerificationContext_t

// --------------------------------
// SPI Driver configuration

#define C4BOND_LOW_POWER_MX25R8035F		// To supply the flash by the EFR32

#define BTL_DRIVER_SPI_USART                            USART0
#define BTL_DRIVER_SPI_USART_CLOCK                      cmuClock_USART0

#define BTL_DRIVER_SPI_USART_TXLOC                      USART_ROUTELOC0_TXLOC_LOC29
#define BTL_DRIVER_SPI_USART_RXLOC                      USART_ROUTELOC0_RXLOC_LOC26
#define BTL_DRIVER_SPI_USART_CLKLOC                     USART_ROUTELOC0_CLKLOC_LOC26

#define BTL_DRIVER_SPI_MOSI_PORT                        gpioPortF
#define BTL_DRIVER_SPI_MOSI_PIN                         5
#define BTL_DRIVER_SPI_MISO_PORT                        gpioPortF
#define BTL_DRIVER_SPI_MISO_PIN                         3
#define BTL_DRIVER_SPI_CLK_PORT                         gpioPortF
#define BTL_DRIVER_SPI_CLK_PIN                          4
#define BTL_DRIVER_SPI_CS_PORT                          gpioPortC
#define BTL_DRIVER_SPI_CS_PIN                           11

#define BTL_DRIVER_SPI_FLASH_SUPPLY_PORT                gpioPortC

#define BTL_DRIVER_SPI_BAUDRATE                         10000000 // See se_hal_spi_EFR32MG1P233F256GM48.c

  // SPI flash supply pin definitions
#define SE_HAL_GPIO_SPI_FLASH_SUPPLY_GPIO_DOUT_CONFIG   0x000000C0
#define SE_HAL_GPIO_SPI_FLASH_SUPPLY_PORT               gpioPortC
#define SE_HAL_GPIO_SPI_FLASH_GPIO_MODEL_MASK           0xFF000000
#define SE_HAL_GPIO_SPI_FLASH_GPIO_MODEL_CONFIG         0x44000000
#define SE_HAL_GPIO_SPI_FLASH_GPIO_DRIVE_STRENGTH_MASK  0x00010001

// --------------------------------

#endif // BTL_CONFIG_H