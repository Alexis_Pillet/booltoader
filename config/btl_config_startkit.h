/***************************************************************************//**
 * @file btl_config.h
 * @brief Configuration header for EFM32/EFR32 bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef BTL_CONFIG_H
#define BTL_CONFIG_H

#include "em_device.h"

// --------------------------------
// Bootloader Version

#define BOOTLOADER_VERSION_FIRST_STAGE_MAJOR      0
#define BOOTLOADER_VERSION_FIRST_STAGE_MINOR      1
#define BOOTLOADER_VERSION_FIRST_STAGE_PATCH      0
#define BOOTLOADER_VERSION_FIRST_STAGE_CUSTOMER   0

#define BOOTLOADER_VERSION_SECOND_STAGE_MAJOR     0
#define BOOTLOADER_VERSION_SECOND_STAGE_MINOR     1
#define BOOTLOADER_VERSION_SECOND_STAGE_PATCH     0
#define BOOTLOADER_VERSION_SECOND_STAGE_CUSTOMER  0

// --------------------------------
// Bootloader Memory Layout

#define BTL_FIRST_STAGE_BASE              0x0UL
#define BTL_FIRST_STAGE_SIZE              0x800UL

#define BTL_SECOND_STAGE_BASE             (BTL_FIRST_STAGE_BASE + BTL_FIRST_STAGE_SIZE)
#define BTL_SECOND_STAGE_SIZE             (0x4000 - BTL_FIRST_STAGE_SIZE)

#define BTL_UPGRADE_LOCATION_BASE         (BTL_SECOND_STAGE_BASE + BTL_SECOND_STAGE_SIZE)

#define BTL_APP_SPACE_BASE                0x4000
#define BTL_APP_SPACE_SIZE                (FLASH_SIZE - BTL_APP_SPACE_BASE)

#define BTL_ALLOW_DOWNGRADE               (1)

// --------------------------------
// EBL Image parser configuration
#define BTL_EBL_PARSER_ENCRYPTED_ONLY     (0)
#define BTL_EBL_PARSER_SIGNED_ONLY        (1)

// --------------------------------
// Debug plugin configuration

// Enable debug printing
#define BTL_PLUGIN_DEBUG_PRINT

// Enable UART debug print @ 115200-8-N-1
#define BTL_DEBUG_PRINT_UART

#define BTL_DEBUG_UART USART0
#define BTL_DEBUG_UART_CLOCK CMU_HFPERCLKEN0_USART0

// Use TX location 0
#define BTL_DEBUG_UART_TX_LOCATION 0
#define BTL_DEBUG_UART_TX_PORT 0
#define BTL_DEBUG_UART_TX_PIN  0

// Use enable pin
#define BTL_DEBUG_UART_EN_PORT 0
#define BTL_DEBUG_UART_EN_PIN  5

// Enable SWO debug print
//#define BTL_DEBUG_PRINT_SWO

// Enable LED debug of failure
#define BTL_PLUGIN_DEBUG_FAILURE
#define BTL_DEBUG_FAILURE_LED

#define BTL_DEBUG_LED_PORT    gpioPortC
#define BTL_DEBUG_LED_PIN     6

// --------------------------------
// Storage plugin configuration

#define BOOTLOADER_SUPPORT_STORAGE
#define BOOTLOADER_SUPPORT_RAW_STORAGE

// Enable spiflash storage plugin
#define BTL_PLUGIN_STORAGE_SPIFLASH

// Define number of slots and their layout
#define BTL_PLUGIN_STORAGE_NUM_SLOTS 2
#define BTL_PLUGIN_STORAGE_SLOTS            \
{                                           \
  {                                         \
    0x40000,      /* Start address */       \
    0x40000,      /* Slot size */           \
  },                                        \
  {                                         \
    0x80000,      /* Start address */       \
    0x40000,      /* Slot size */           \
  }                                         \
}

// Enable Macronix 8Mb Low Power flash
#define BTL_STORAGE_SPIFLASH_MACRONIX_MX25R8035F

// Custom init and shutdown commands, e.g. setting power pin etc
#define BTL_STORAGE_CUSTOM_INIT() do {      \
} while (0);
#define BTL_STORAGE_CUSTOM_SHUTDOWN() do {  \
} while (0);

#define BTL_STORAGE_READ_BUFFER_SIZE            (128)
#define BTL_STORAGE_VERIFICATION_CONTEXT_SIZE   (384)

// --------------------------------
// SPI Driver configuration

#define BTL_DRIVER_SPI_USART        USART1
#define BTL_DRIVER_SPI_USART_CLOCK  cmuClock_USART1

#define BTL_DRIVER_SPI_USART_TXLOC  USART_ROUTELOC0_TXLOC_LOC11
#define BTL_DRIVER_SPI_USART_RXLOC  USART_ROUTELOC0_RXLOC_LOC11
#define BTL_DRIVER_SPI_USART_CLKLOC USART_ROUTELOC0_CLKLOC_LOC11

#define BTL_DRIVER_SPI_MOSI_PORT    gpioPortC
#define BTL_DRIVER_SPI_MOSI_PIN     6
#define BTL_DRIVER_SPI_MISO_PORT    gpioPortC
#define BTL_DRIVER_SPI_MISO_PIN     7
#define BTL_DRIVER_SPI_CLK_PORT     gpioPortC
#define BTL_DRIVER_SPI_CLK_PIN      8
#define BTL_DRIVER_SPI_CS_PORT      gpioPortA
#define BTL_DRIVER_SPI_CS_PIN       4

#define BTL_DRIVER_SPI_BAUDRATE     12000000U

// --------------------------------

#endif // BTL_CONFIG_H