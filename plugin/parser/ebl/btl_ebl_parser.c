/***************************************************************************//**
 * @file btl_ebl_parser.c
 * @brief EBL image file parser
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "plugin/parser/btl_image_parser_interface.h"
#include "btl_ebl_format.h"
#include "btl_ebl_parser.h"

#include "plugin/security/btl_security_aes.h"
#include "plugin/security/btl_security_sha256.h"
#include "plugin/security/btl_security_ecdsa.h"
#include "plugin/security/btl_crc32.h"
#include "plugin/security/btl_security_tokens.h"

// Debug
#include "plugin/debug/btl_debug.h"

#define EBL_PARSER_ARRAY_TO_U16_REV(array, offset) (((array)[offset] << 8) | (array)[offset + 1])
#define EBL_PARSER_ARRAY_TO_U32_REV(array, offset) (((array)[offset] << 24) | ((array)[offset + 1] << 16) | ((array)[offset + 2] << 8) | ((array)[offset + 3] << 0))

/**************************************************************************//**
 * Save remainder of input buffer to local buffer
 *
 * @param context Context variable
 * @param buffer input buffer to copy from
 * @param offset current offset in input buffer (first byte to process)
 * @param length length of input buffer
 * @returns True if pushing succeeded, false if there would have been a problem.
 *
 * Saves the remainder of the input buffer to a local buffer, to catch situations
 *  where the input buffer doesn't line up with EBL tag boundaries.
 *****************************************************************************/
static bool ebl_pushBuffer(EblParserContext_t* context, uint8_t* buffer, size_t offset, size_t length) 
{
  size_t position;

  if(offset >= length) {
    return false; // Shouldn't happen, but be safe anyway
  }

  if((length - offset) + context->localBuffer.length > sizeof(context->localBuffer.buffer)) {
    return false; // Buffer overflow
  }

  while(offset < length) {
    position = (context->localBuffer.offset + context->localBuffer.length) % sizeof(context->localBuffer.buffer);
    context->localBuffer.buffer[position] = buffer[offset];
    offset++;
    context->localBuffer.length++;
  }

  return true;
}

/**************************************************************************//**
 * Copy bytes from internal + input buffer to local buffer
 *
 * @param context Context variable
 * @param localBuffer buffer to copy to
 * @param inputBuffer input buffer to copy from
 * @param inputOffset offset indicating next byte to process in the inputbuffer
 * @param inputLength size of the input buffer in bytes
 * @param numberOfBytes number of bytes requested to be copied
 * @returns True if all bytes got copied, false if not.
 *
 * Gets the first n bytes in the input queue and copies them to a specified
 *   temporary buffer. The input queue is the concatenation of the local
 *   buffer containing saved bytes from the previous call to the processing
 *   functions, and the input buffer from the current call to the processing
 *   function.
 * This function will also update the context accordingly.
 *****************************************************************************/
static bool ebl_popBuffer(EblParserContext_t* context, 
                   uint8_t* localBuffer,
                   uint8_t* inputBuffer, 
                   size_t* inputOffset, 
                   size_t inputLength, 
                   size_t numberOfBytes) 
{
  size_t bytesProcessed = 0;
  // Get data from local buffer first
  while((context->localBuffer.length > 0) && (bytesProcessed < numberOfBytes)) {
    localBuffer[bytesProcessed] = context->localBuffer.buffer[context->localBuffer.offset];
    context->localBuffer.offset = (context->localBuffer.offset + 1) % sizeof(context->localBuffer.buffer);
    context->localBuffer.length--;
    bytesProcessed++;
  }
  // Get data from new buffer when local buffer exhausted
  while((*inputOffset < inputLength) && (bytesProcessed < numberOfBytes)) {
    localBuffer[bytesProcessed] = inputBuffer[*inputOffset];
    (*inputOffset)++;
    bytesProcessed++;
  }

  if(bytesProcessed == numberOfBytes) {
    return true;
  } else {
    return false;
  }
}

/**************************************************************************//**
 * Get amount of bytes available in internal buffer and external buffer combined
 *
 * @param context Context variable
 * @param externalBytes amount of bytes available externally to the internal buffer
 * @returns amount of bytes available in internal buffer
 *
 *****************************************************************************/
static size_t ebl_getBytesAvailable(EblParserContext_t* context, size_t externalBytes) 
{
  return context->localBuffer.length + externalBytes;
}

/**************************************************************************//**
 * Get amount of bytes left until reaching the end of a tag
 *
 * @param context Context variable
 * @returns amount of bytes until reaching the end of a tag
 *
 * This function returns the amount of bytes left until reaching the end of a
 *   tag. This is the smallest value of either reaching the end of the current
 *   data-containing tag or the end of the encrypted block.
 * Beware that the amount returned might be greater than what is currently
 *   available in the buffers!
 *****************************************************************************/
static size_t ebl_getBytesUntilEndOfTag(EblParserContext_t* context) {
  size_t returnValue = 0;

  returnValue = context->lengthOfTag - context->offsetInTag;
  if(context->inEncryptedContainer
     && ((context->lengthOfEncryptedTag - context->offsetInEncryptedTag) < returnValue)) {
    returnValue = context->lengthOfEncryptedTag - context->offsetInEncryptedTag;
  }

  return returnValue;
}

/**************************************************************************//**
 * Update context with amount of parsed bytes
 *
 * @param context Context variable
 * @param consumedBytes Amount of bytes parsed by the parser
 *
 * This function will update the internal parser counters and kick the parser
 *   out of decryption mode when reaching the end of an encrypted block.
 *****************************************************************************/
static void ebl_consumeBytes(EblParserContext_t* context, size_t consumedBytes) 
{
  if(context->inEncryptedContainer) {
    context->offsetInEncryptedTag += consumedBytes;
    if(context->offsetInEncryptedTag >= context->lengthOfEncryptedTag) {
      context->inEncryptedContainer = false;
    }
  }
  context->offsetInTag += consumedBytes;
}

/**************************************************************************//**
 * "Parse" bytes until reaching the end of the current tag.
 *
 * @param context Context variable
 * @param inputOffset Current offset into this round's input buffer
 * @param inputLength Length of this round's input buffer
 * @returns True if end of tag got reached, false if end of tag was not reached
 *   before running out of input buffer.
 *
 * This function will pull bytes from the input buffer and run them through
 *   hashing, but not act on them.
 *****************************************************************************/
static bool ebl_skipToEndOfTag(EblParserContext_t* context,
                               uint8_t* inputBuffer, 
                               size_t*  inputOffset,
                               size_t   inputLength)
{
  size_t temporarySize = ebl_getBytesUntilEndOfTag(context);
  size_t internalBufferBytes = 0;
  if(context->localBuffer.length > 0) {
    // finish local buffer first
    // temporarySize = min(bytesInInternalBuffer, bytesLeftInTag)
    if(temporarySize > context->localBuffer.length) {
      temporarySize = context->localBuffer.length;
    }

    while(temporarySize > 0) {
      // Hash over all popped bytes, one by one since the buffer is circular
      if((context->localBuffer.offset + context->localBuffer.length) > sizeof(context->localBuffer.buffer)) {
        internalBufferBytes = sizeof(context->localBuffer.buffer) - context->localBuffer.offset;
      } else {
        internalBufferBytes = context->localBuffer.length;
      }

      btl_updateSha256(&(context->shaContext), &(context->localBuffer.buffer[context->localBuffer.offset]), internalBufferBytes);
      context->fileCrc = btl_crc32Stream(&(context->localBuffer.buffer[context->localBuffer.offset]), internalBufferBytes, context->fileCrc);

      if(context->inEncryptedContainer) {
        btl_processAesCtrData(&(context->aesContext), 
                              &(context->localBuffer.buffer[context->localBuffer.offset]), 
                              &(context->localBuffer.buffer[context->localBuffer.offset]), 
                              internalBufferBytes);
      }


      context->localBuffer.offset = (context->localBuffer.offset + internalBufferBytes) % sizeof(context->localBuffer.buffer);
      context->localBuffer.length -= internalBufferBytes;

      ebl_consumeBytes(context, internalBufferBytes);

      temporarySize -= internalBufferBytes;
    }
  } 

  temporarySize = ebl_getBytesUntilEndOfTag(context);
  if(temporarySize == 0) {
    return true;
  }

  if(*inputOffset < inputLength) {
    // temporarySize = min(bytesInExternalBuffer, bytesLeftInTag)
    if(temporarySize > inputLength - *inputOffset) {
      temporarySize = inputLength - *inputOffset;
    }

    // Hash over all popped bytes before decryption
    btl_updateSha256(&(context->shaContext), &(inputBuffer[*inputOffset]), temporarySize);
    context->fileCrc = btl_crc32Stream(&(inputBuffer[*inputOffset]), temporarySize, context->fileCrc);

    if(context->inEncryptedContainer) {
      btl_processAesCtrData(&(context->aesContext), &(inputBuffer[*inputOffset]), &(inputBuffer[*inputOffset]), temporarySize);
    }

    // Indicate bytes processed
    *inputOffset += temporarySize;
    ebl_consumeBytes(context, temporarySize);
  }
  return ebl_getBytesUntilEndOfTag(context) == 0;
}

static uint32_t ebl_tryGetHeader(EblParserContext_t*  context,
                                 uint8_t*             inputBuffer, 
                                 size_t*              inputOffset,
                                 size_t               inputLength,
                                 EblTagHeader_t*      eblTagHeader)
{
  uint8_t tagBuffer[4];
  if(ebl_getBytesAvailable(context, inputLength-*inputOffset) < 4) {
    ebl_pushBuffer(context, inputBuffer, *inputOffset, inputLength);
    return EBL_PARSER_ERROR_CONTINUE;
  }

  if(!ebl_popBuffer(context,
                    tagBuffer,
                    inputBuffer,
                    inputOffset,
                    inputLength,
                    4)) {
    return EBL_PARSER_ERROR_BUFFER;
  }

  // CRC32 over all data
  context->fileCrc = btl_crc32Stream(tagBuffer, 4, context->fileCrc);

  // Hash over all popped data, unless it is the signature or end tag
  // Decrypt if necessary
  if(context->inEncryptedContainer) {
    btl_updateSha256(&(context->shaContext), tagBuffer, 4);
    btl_processAesCtrData(&(context->aesContext), tagBuffer, tagBuffer, 4);

    // Fix endianness of incoming data
    eblTagHeader->tagId = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 0);
    eblTagHeader->length  = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 2);

    ebl_consumeBytes(context, 4);
  } else {
    // Fix endianness of incoming data
    eblTagHeader->tagId = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 0);
    eblTagHeader->length  = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 2);

    if(eblTagHeader->tagId != EBL_TAG_ID_SIGNATURE_ECDSA_P256
       && eblTagHeader->tagId != EBL_TAG_ID_END) {
      btl_updateSha256(&(context->shaContext), tagBuffer, 4);
    }
  }

  // Save length of this tag
  context->lengthOfTag = eblTagHeader->length;
  context->offsetInTag = 0;

  BTL_DEBUG_PRINT("tag 0x");
  BTL_DEBUG_PRINT_SHORT_HEX(eblTagHeader->tagId);
  BTL_DEBUG_PRINT(" len 0x");
  BTL_DEBUG_PRINT_SHORT_HEX(eblTagHeader->length);
  BTL_DEBUG_PRINTC('\n');

  return EBL_PARSER_ERROR_OK;
}

/**************************************************************************//**
 * Initialize the parser's context
 *
 * @param context Pointer to the specific parser's context variable
 * @returns 0 if OK, error code otherwise.
 *
 *****************************************************************************/
uint32_t btl_startParseImage(void* context)
{
  // Clean up internal state
  EblParserContext_t* parserContext = (EblParserContext_t*)context;
  parserContext->internalState = EblParserStateInit;
  parserContext->localBuffer.length = 0;
  parserContext->localBuffer.offset = 0;
  parserContext->encrypted = false;
  parserContext->inEncryptedContainer = false;
  parserContext->gotSignature = false;
  parserContext->fileCrc = BTL_CRC32_START;
  parserContext->metadataAddress = 0;

  btl_initSha256(&(parserContext->shaContext));

  return EBL_PARSER_ERROR_OK;
}

/**************************************************************************//**
 * Parse an image file in order to extract the binary and some metadata.
 *
 * @param context Pointer to the specific parser's context variable
 * @param imageProperties Pointer to the image file state variable
 * @param buffer Pointer to byte array containing data to parse
 * @param length Size in bytes of the data in buffer
 * @param callbacks Struct containing function pointers to be called by the
 *   parser to pass the binary back to BTL.
 * @returns 0 when all data processed successfully, error code otherwise.
 *
 * Presents data to the image file parser in order to be parsed. This function
 *   can be used in a streaming way, so there are no limits on the size of
 *   the input buffer, or when/how many times the parsing function gets called.
 *****************************************************************************/
uint32_t btl_parseImageData(void* context,
                            BtlImageProperties_t* imageProperties,
                            uint8_t* buffer,
                            size_t length,
                            BtlImageDataCallbacks_t* callbacks)
{
  EblParserContext_t* parserContext = (EblParserContext_t*)context;
  size_t offset = 0;
  uint8_t tagBuffer[64];
  uint16_t temporaryShort;
  uint32_t retval;
  EblTagHeader_t eblTagHeader;
  size_t   temporarySize;
  
  // This is pretty much purely a state machine...
  while(offset < length) {
    switch(parserContext->internalState) {
      // Coming from an idle state means starting anew
      // Which means we're expecting a header tag, either regular or encrypted
      case EblParserStateInit:
        // First, get tag/length combo
        retval = ebl_tryGetHeader(parserContext,
                                  buffer,
                                  &offset,
                                  length,
                                  &eblTagHeader);

        if(retval == EBL_PARSER_ERROR_CONTINUE) {
          return EBL_PARSER_ERROR_OK;
        } else if (retval != EBL_PARSER_ERROR_OK) {
          parserContext->internalState = EblParserStateError;
          return retval;
        }

        // Save length of this tag
        parserContext->lengthOfTag = eblTagHeader.length;
        parserContext->offsetInTag = 0;

        // Check tag ID for valid tag
        switch(eblTagHeader.tagId) {
          case EBL_TAG_ID_HEADER:
            if(BTL_EBL_PARSER_ENCRYPTED_ONLY) {
              return EBL_PARSER_ERROR_UNEXPECTED_TAG;
            }
            parserContext->internalState = EblParserStateHeader;
            break;
          case EBL_TAG_ID_ENC_HEADER:
            parserContext->internalState = EblParserStateEncryptionHeader;
            parserContext->encrypted = true;
            BTL_DEBUG_PRINT("Enc\n");
            break;
          case EBL_TAG_ID_METADATA:
          case EBL_TAG_ID_PROG:
          case EBL_TAG_ID_ERASEPROG:
          case EBL_TAG_ID_END:
          case EBL_TAG_ID_ENC_INIT:
          case EBL_TAG_ID_ENC_EBL_DATA:
          case EBL_TAG_ID_ENC_MAC:
          case EBL_TAG_ID_SIGNATURE_ECDSA_P256:
            parserContext->internalState = EblParserStateError;
            return EBL_PARSER_ERROR_UNEXPECTED_TAG;
          default:
            parserContext->internalState = EblParserStateError;
            return EBL_PARSER_ERROR_UNSUPPORTED_TAG;
        }
        
        break;
      
      // We've already got the header block, and are done with whatever
      // tag we were processing. Now waiting for a new tag.
      case EblParserStateIdle:
        // First, get tag/length combo, for which we need 4 bytes
        retval = ebl_tryGetHeader(parserContext,
                                  buffer,
                                  &offset,
                                  length,
                                  &eblTagHeader);

        if(retval == EBL_PARSER_ERROR_CONTINUE) {
          return EBL_PARSER_ERROR_OK;
        } else if (retval != EBL_PARSER_ERROR_OK) {
          parserContext->internalState = EblParserStateError;
          return retval;
        }

        // Check tagBuffer for valid tag/length
        if(parserContext->encrypted && !parserContext->inEncryptedContainer) {
          switch(eblTagHeader.tagId) {
            case EBL_TAG_ID_ENC_INIT:
              parserContext->internalState = EblParserStateEncryptionInit;
              break;
            case EBL_TAG_ID_ENC_EBL_DATA:
              parserContext->internalState = EblParserStateEncryptionContainer;
              parserContext->lengthOfEncryptedTag = parserContext->lengthOfTag;
              parserContext->offsetInEncryptedTag = 0;
              break;
            case EBL_TAG_ID_ENC_MAC:
              //MAC is no longer supported, since we rely on ECDSA signing instead
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_UNSUPPORTED_TAG;
            case EBL_TAG_ID_SIGNATURE_ECDSA_P256:
              parserContext->internalState = EblParserStateSignature;
              break;
            case EBL_TAG_ID_END:
              parserContext->internalState = EblParserStateFinalize;
              break;
            case EBL_TAG_ID_METADATA:
            case EBL_TAG_ID_PROG:
            case EBL_TAG_ID_ERASEPROG:
            case EBL_TAG_ID_HEADER:
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_UNEXPECTED_TAG;
            default:
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_UNSUPPORTED_TAG;
          }
        } else {
          switch(eblTagHeader.tagId) {
            case EBL_TAG_ID_HEADER:
              // Only allow header tag here if we went into decryption mode
              // since the header is encrypted as well for encrypted EBLs
              if(parserContext->inEncryptedContainer) {
                parserContext->internalState = EblParserStateHeader;
              } else {
                parserContext->internalState = EblParserStateError;
                return EBL_PARSER_ERROR_UNEXPECTED_TAG;
              }
              break;
            case EBL_TAG_ID_METADATA:
              parserContext->internalState = EblParserStateMetadata;
              break;
            case EBL_TAG_ID_PROG:
              parserContext->internalState = EblParserStateProg;
              break;
            case EBL_TAG_ID_ERASEPROG:
              parserContext->internalState = EblParserStateEraseProg;
              break;
            case EBL_TAG_ID_END:
              if(parserContext->inEncryptedContainer) {
                parserContext->internalState = EblParserStateError;
                return EBL_PARSER_ERROR_UNEXPECTED_TAG;
              }
              parserContext->internalState = EblParserStateFinalize;
              break;
            case EBL_TAG_ID_SIGNATURE_ECDSA_P256:
              if(parserContext->inEncryptedContainer) {
                parserContext->internalState = EblParserStateError;
                return EBL_PARSER_ERROR_UNEXPECTED_TAG;
              }
              parserContext->internalState = EblParserStateSignature;
              break;
            default:
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_UNSUPPORTED_TAG;
          }
        }
        break;
      
      // Received a header tag, parse information from it.
      case EblParserStateHeader:
        // Get version, signature, flashaddress
        while(parserContext->offsetInTag < 12) {
          if(ebl_getBytesAvailable(parserContext, length - offset) < 12) {
            ebl_pushBuffer(parserContext, buffer, offset, length);
            return EBL_PARSER_ERROR_OK;
          } else {
            if(!ebl_popBuffer(parserContext,
                              tagBuffer,
                              buffer,
                              &offset,
                              length,
                              12)) {
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_BUFFER;
            }

            // Hash over all popped data
            btl_updateSha256(&(parserContext->shaContext), tagBuffer, 12);
            parserContext->fileCrc = btl_crc32Stream(tagBuffer, 12, parserContext->fileCrc);

            if(parserContext->inEncryptedContainer) {
              btl_processAesCtrData(&(parserContext->aesContext), tagBuffer, tagBuffer, 12);
            }

            // 12 bytes: u16 version, u16 signature, u32 flashAddress, u32 aatCrc

            temporaryShort = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 0);
            if((temporaryShort & 0xFF00) != EBL_COMPATIBILITY_MAJOR_VERSION) {
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_VERSION_MISMATCH;
            }

            temporaryShort = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 2);
            if(temporaryShort != EBL_IMAGE_MAGIC_WORD) {
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_NO_HEADER;
            }

            parserContext->programmingAddress = EBL_PARSER_ARRAY_TO_U32_REV(tagBuffer, 4);

            // AATCRC
            // EBL_PARSER_ARRAY_TO_U32_REV(tagBuffer, 8);

            ebl_consumeBytes(parserContext, 12);
          }
        }

        // Treat the AAT as data
        while(parserContext->offsetInTag < parserContext->lengthOfTag) {
          // Need at minimum a full word
          if(ebl_getBytesAvailable(parserContext, length-offset) < 4) {
            ebl_pushBuffer(parserContext, buffer, offset, length);
            return EBL_PARSER_ERROR_OK;
          }

          // if data in the local buffer, consume that first
          if(parserContext->localBuffer.length > 0) {
            temporarySize = parserContext->localBuffer.length;
            if(temporarySize > (parserContext->lengthOfTag - parserContext->offsetInTag)) {
              temporarySize = parserContext->lengthOfTag - parserContext->offsetInTag;
            }

            // Make sure to read word-sized chunks from the buffer.
            // We can safely do the rounding up since we already verified there are 4+ bytes available
            temporarySize = ((temporarySize + 3) / 4) * 4;

            // Load into tagBuffer
            if(!ebl_popBuffer(parserContext,
                              tagBuffer,
                              buffer,
                              &offset,
                              length,
                              temporarySize)) {
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_BUFFER;
            }

            // Hash over all popped bytes before decryption
            btl_updateSha256(&(parserContext->shaContext), tagBuffer, temporarySize);
            parserContext->fileCrc = btl_crc32Stream(tagBuffer, temporarySize, parserContext->fileCrc);

            // Decrypt if necessary
            if(parserContext->inEncryptedContainer) {
              btl_processAesCtrData(&(parserContext->aesContext), tagBuffer, tagBuffer, temporarySize);
            }

            // Push back data
            if(callbacks->dataFunction != NULL) {
              callbacks->dataFunction(parserContext->programmingAddress,
                                      tagBuffer,
                                      temporarySize,
                                      BtlWriteWithErase,
                                      callbacks->callbackContext);
              parserContext->programmingAddress += temporarySize;
            }

            // Indicate bytes processed
            ebl_consumeBytes(parserContext, temporarySize);
          } 

          if(length - offset > 4) {
            temporarySize = length - offset;
            if(temporarySize > (parserContext->lengthOfTag - parserContext->offsetInTag)) {
              temporarySize = parserContext->lengthOfTag - parserContext->offsetInTag;
            }

            // Only do a word-sized chunk
            temporarySize = (temporarySize / 4) * 4;
            // Hash over all popped bytes before decryption
            btl_updateSha256(&(parserContext->shaContext), &(buffer[offset]), temporarySize);
            parserContext->fileCrc = btl_crc32Stream(&(buffer[offset]), temporarySize, parserContext->fileCrc);

            // Decrypt if necessary
            if(parserContext->inEncryptedContainer) {
              btl_processAesCtrData(&(parserContext->aesContext), &(buffer[offset]), &(buffer[offset]), temporarySize);
            }

            // Push back data
            if(callbacks->dataFunction != NULL) {
              callbacks->dataFunction(parserContext->programmingAddress,
                                      &(buffer[offset]),
                                      temporarySize,
                                      BtlWriteWithErase,
                                      callbacks->callbackContext);
              parserContext->programmingAddress += temporarySize;
            }

            // Indicate bytes processed
            offset += temporarySize;
            ebl_consumeBytes(parserContext, temporarySize);
          }
        }


        parserContext->internalState = EblParserStateIdle;
        break;

      // Received a tag with binary data to pass on. If you have custom metadata in your EBL, we'll pass
      // it on to the application (through the bootloader). Prog and Eraseprog tags are acted on by the bootloader.
      case EblParserStateMetadata:
      case EblParserStateProg:
      case EblParserStateEraseProg:
        while(parserContext->offsetInTag < 4) {
          if(ebl_getBytesAvailable(parserContext, length-offset) < 4) {
            ebl_pushBuffer(parserContext, buffer, offset, length);
            return EBL_PARSER_ERROR_OK;
          }

          if(!ebl_popBuffer(parserContext,
                            tagBuffer,
                            buffer,
                            &offset,
                            length,
                            4)) {
            parserContext->internalState = EblParserStateError;
            return EBL_PARSER_ERROR_BUFFER;
          }

          // Hash over all popped bytes before decryption
          btl_updateSha256(&(parserContext->shaContext), tagBuffer, 4);
          parserContext->fileCrc = btl_crc32Stream(tagBuffer, 4, parserContext->fileCrc);

          if(parserContext->inEncryptedContainer) {
            btl_processAesCtrData(&(parserContext->aesContext), tagBuffer, tagBuffer, 4);
          }

          parserContext->programmingAddress = EBL_PARSER_ARRAY_TO_U32_REV(tagBuffer, 0);;

          ebl_consumeBytes(parserContext, 4);
        }
        while(parserContext->offsetInTag < parserContext->lengthOfTag) {
          if(ebl_getBytesAvailable(parserContext, length-offset) < 4) {
            ebl_pushBuffer(parserContext, buffer, offset, length);
            return EBL_PARSER_ERROR_OK;
          }

          // Consume internal buffer first
          if(parserContext->localBuffer.length > 0) {
            // temporarySize = min(bytesInInternalBuffer, bytesLeftInTag)
            temporarySize = parserContext->localBuffer.length;
            if(temporarySize > (parserContext->lengthOfTag - parserContext->offsetInTag)) {
              temporarySize = parserContext->lengthOfTag - parserContext->offsetInTag;
            }

            // Make sure to read word-sized chunks from the buffer.
            // We can safely do the rounding up since we already verified there are 4+ bytes available
            temporarySize = ((temporarySize + 3) / 4) * 4;

            // Load into tagBuffer
            if(!ebl_popBuffer(parserContext,
                              tagBuffer,
                              buffer,
                              &offset,
                              length,
                              temporarySize)) {
              parserContext->internalState = EblParserStateError;
              return EBL_PARSER_ERROR_BUFFER;
            }

            // Hash over all popped bytes before decryption
            btl_updateSha256(&(parserContext->shaContext), tagBuffer, temporarySize);
            parserContext->fileCrc = btl_crc32Stream(tagBuffer, temporarySize, parserContext->fileCrc);

            // Decrypt if necessary
            if(parserContext->inEncryptedContainer) {
              btl_processAesCtrData(&(parserContext->aesContext), tagBuffer, tagBuffer, temporarySize);
            }

            // Push back data
            if(parserContext->internalState == EblParserStateMetadata
               && callbacks->metaDataFunction != NULL) {
              callbacks->metaDataFunction(parserContext->metadataAddress,
                                          tagBuffer,
                                          temporarySize,
                                          callbacks->callbackContext);
              parserContext->metadataAddress += temporarySize;
            } else if(parserContext->internalState == EblParserStateProg
                      && callbacks->dataFunction != NULL) {
              callbacks->dataFunction(parserContext->programmingAddress,
                                      tagBuffer,
                                      temporarySize,
                                      BtlWrite,
                                      callbacks->callbackContext);
              parserContext->programmingAddress += temporarySize;
            } else if(parserContext->internalState == EblParserStateEraseProg
                      && callbacks->dataFunction != NULL) {
              callbacks->dataFunction(parserContext->programmingAddress,
                                      tagBuffer,
                                      temporarySize,
                                      BtlWriteWithErase,
                                      callbacks->callbackContext);
              parserContext->programmingAddress += temporarySize;
            }

            // Indicate bytes processed
            ebl_consumeBytes(parserContext, temporarySize);
          } else if(offset < length) {
            // temporarySize = min(bytesInExternalBuffer, bytesLeftInTag)
            temporarySize = length - offset;
            if(temporarySize > (parserContext->lengthOfTag - parserContext->offsetInTag)) {
              temporarySize = parserContext->lengthOfTag - parserContext->offsetInTag;
            }

            // Only do a word-sized chunk
            temporarySize = (temporarySize / 4) * 4;

            // Hash over all popped bytes before decryption
            btl_updateSha256(&(parserContext->shaContext),
                             &(buffer[offset]),
                             temporarySize);
            parserContext->fileCrc = btl_crc32Stream(&(buffer[offset]),
                                                     temporarySize, 
                                                     parserContext->fileCrc);

            // Decrypt if necessary
            if(parserContext->inEncryptedContainer) {
              btl_processAesCtrData(&(parserContext->aesContext),
                                    &(buffer[offset]),
                                    &(buffer[offset]),
                                    temporarySize);
            }

            // Push back data
            if(parserContext->internalState == EblParserStateMetadata
               && callbacks->metaDataFunction != NULL) {
              callbacks->metaDataFunction(parserContext->metadataAddress,
                                          &(buffer[offset]),
                                          temporarySize,
                                          callbacks->callbackContext);
              parserContext->metadataAddress += temporarySize;
            } else if(parserContext->internalState == EblParserStateProg
                      && callbacks->dataFunction != NULL) {
              callbacks->dataFunction(parserContext->programmingAddress,
                                      &(buffer[offset]),
                                      temporarySize,
                                      BtlWrite,
                                      callbacks->callbackContext);
              parserContext->programmingAddress += temporarySize;
            } else if(parserContext->internalState == EblParserStateEraseProg
                      && callbacks->dataFunction != NULL) {
              callbacks->dataFunction(parserContext->programmingAddress,
                                      &(buffer[offset]),
                                      temporarySize,
                                      BtlWriteWithErase,
                                      callbacks->callbackContext);
              parserContext->programmingAddress += temporarySize;
            }

            // Indicate bytes processed
            offset += temporarySize;
            ebl_consumeBytes(parserContext, temporarySize);
          } else {
            // Ran out of buffer
            return EBL_PARSER_ERROR_OK;
          }
        }
        parserContext->internalState = EblParserStateIdle;
        break;

      // Received an end tag, start the cleanup process
      case EblParserStateFinalize:
        if(ebl_getBytesAvailable(parserContext, length-offset) < 4) {
          ebl_pushBuffer(parserContext, buffer, offset, length);
          return EBL_PARSER_ERROR_OK;
        }

        if(!ebl_popBuffer(parserContext,
                          tagBuffer,
                          buffer,
                          &offset,
                          length,
                          4)) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_BUFFER;
        }

        parserContext->fileCrc = btl_crc32Stream(tagBuffer, 4, parserContext->fileCrc);

        // Check CRC
        if(parserContext->fileCrc != BTL_CRC32_END) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_CRC;
        }
        
        // Report done to bootloader
        imageProperties->imageCompleted = true;
        parserContext->internalState = EblParserStateDone;
        return EBL_PARSER_ERROR_OK;

      // Completely done with the file, in this state we'll stop processing anything.
      case EblParserStateDone:
        return EBL_PARSER_ERROR_EOF;

      // Received an encryption initialization header, so initialize the encryption state
      case EblParserStateEncryptionInit:
        //This is a fixed size header, so let's get it all at once.
        while(parserContext->offsetInTag < 16) {
          if(ebl_getBytesAvailable(parserContext, length-offset) < 16) {
            ebl_pushBuffer(parserContext, buffer, offset, length);
            return EBL_PARSER_ERROR_OK;
          }

          if(!ebl_popBuffer(parserContext,
                            tagBuffer,
                            buffer,
                            &offset,
                            length,
                            16)) {
            parserContext->internalState = EblParserStateError;
            return EBL_PARSER_ERROR_BUFFER;
          }
          // Hash over all popped bytes before decryption
          btl_updateSha256(&(parserContext->shaContext), tagBuffer, 16);
          parserContext->fileCrc = btl_crc32Stream(tagBuffer, 16, parserContext->fileCrc);
          
          // Since we've deprecated MAC signing, we don't care about associated data.
          // Format: u32 msgLen, u8 nonce[12]     

          // Initialize AES-CCM
          btl_initAesCcm(&(parserContext->aesContext),
                         0x02,
                         &(tagBuffer[4]),
                         1, //Ember starts counter at 1?
                         btl_getImageFileEncryptionKeyPtr(),
                         128);

          ebl_consumeBytes(parserContext, 16);
        }

        // Skip over associated data
        while(parserContext->offsetInTag < parserContext->lengthOfTag) {
          if(!ebl_skipToEndOfTag(parserContext, buffer, &offset, length)) {
            // Ran out of buffer, will continue on next call
            return EBL_PARSER_ERROR_OK;
          }
        }

        parserContext->internalState = EblParserStateIdle;
        break;

      // Received an encryption header, parse information from it.
      case EblParserStateEncryptionHeader:
        // Get bytes
        if(ebl_getBytesAvailable(parserContext, length-offset) < 6) {
          ebl_pushBuffer(parserContext, buffer, offset, length);
          return EBL_PARSER_ERROR_OK;
        }

        if(!ebl_popBuffer(parserContext,
                          tagBuffer,
                          buffer,
                          &offset,
                          length,
                          6)) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_BUFFER;
        }
        // Hash over all popped bytes before decryption
        btl_updateSha256(&(parserContext->shaContext), tagBuffer, 6);
        parserContext->fileCrc = btl_crc32Stream(tagBuffer, 6, parserContext->fileCrc);

        // struct contains u16 version, u16 encType, u16 magicWord
        temporaryShort = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 0);
        if((temporaryShort & 0xFF00) != EBL_COMPATIBILITY_MAJOR_VERSION) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_VERSION_MISMATCH;
        }

        temporaryShort = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 2);
        if(temporaryShort != EBL_TYPE_ENCRYPTION_AESCCM) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_UNKNOWN_ENCRYPT;
        }

        temporaryShort = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 4);
        if(temporaryShort != EBL_IMAGE_MAGIC_WORD) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_NO_HEADER;
        }

        ebl_consumeBytes(parserContext, 6);

        parserContext->internalState = EblParserStateIdle;
        break;

      // This tag contains encrypted tags, so set up decryption and go one level down in the state machine
      case EblParserStateEncryptionContainer:
        parserContext->inEncryptedContainer = true;
        parserContext->internalState = EblParserStateIdle;
        break;

      // This tag is unsupported
      case EblParserStateEncryptionMac:
        parserContext->internalState = EblParserStateError;
        return EBL_PARSER_ERROR_UNSUPPORTED_TAG;

      // This tag contains the signature over the entire EBL, accept no more data hereafter.
      case EblParserStateSignature:
        // Make sure we have the necessary data
        while(!parserContext->gotSignature) {
          if(ebl_getBytesAvailable(parserContext, length-offset) < 64) {
            ebl_pushBuffer(parserContext, buffer, offset, length);
            return EBL_PARSER_ERROR_OK;
          }

          // Check the signature
          if(!ebl_popBuffer(parserContext,
                            tagBuffer,
                            buffer,
                            &offset,
                            length,
                            64)) {
            parserContext->internalState = EblParserStateError;
            return EBL_PARSER_ERROR_BUFFER;
          }

          parserContext->fileCrc = btl_crc32Stream(tagBuffer, 64, parserContext->fileCrc);

          btl_finalizeSha256(&(parserContext->shaContext));

          BTL_DEBUG_PRINTC('\n');
          for(temporarySize = 0; temporarySize < 32; temporarySize++) {
            BTL_DEBUG_PRINT_CHAR_HEX(parserContext->shaContext.sha[temporarySize]);
          }
          BTL_DEBUG_PRINTC('\n');

          imageProperties->imageVerified = btl_verifyEcdsaP256r1(parserContext->shaContext.sha, &tagBuffer[0], &tagBuffer[32]);
          parserContext->gotSignature = true;

          ebl_consumeBytes(parserContext, 64);
        }

        // Fetch the END tag, if we get anything else it's invalid.
        if(ebl_getBytesAvailable(parserContext, length-offset) < 8) {
          ebl_pushBuffer(parserContext, buffer, offset, length);
          return EBL_PARSER_ERROR_OK;
        }

        // Check the end tag
        if(!ebl_popBuffer(parserContext,
                          tagBuffer,
                          buffer,
                          &offset,
                          length,
                          8)) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_BUFFER;
        }

        parserContext->fileCrc = btl_crc32Stream(tagBuffer, 8, parserContext->fileCrc);

        temporaryShort = EBL_PARSER_ARRAY_TO_U16_REV(tagBuffer, 0);
        if(temporaryShort != EBL_TAG_ID_END) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_UNEXPECTED_TAG;
        }

        // Check CRC
        if(parserContext->fileCrc != BTL_CRC32_END) {
          parserContext->internalState = EblParserStateError;
          return EBL_PARSER_ERROR_CRC;
        }

        imageProperties->imageCompleted = true;

        parserContext->internalState = EblParserStateDone;
        return EBL_PARSER_ERROR_OK;
      case EblParserStateError:
        return EBL_PARSER_ERROR_EOF;
      // No default statement here guarantees a compile-time check
      //  that we caught all states
    }
  }

  // If we get here, we landed right on a tag boundary...
  return EBL_PARSER_ERROR_OK;
}