/***************************************************************************//**
 * @file btl_ebl_format.h
 * @brief Definitions for the Silicon Labs EBL format
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#ifndef BTL_EBL_FORMAT_H
#define BTL_EBL_FORMAT_H

#include <stdint.h>

/***************************************************************************//**
 * @addtogroup Plugin
 * @{
 * @addtogroup ImageParser Image Parser
 * @{
 * @addtogroup EblParser EBL Parser
 * @{
 * @addtogroup EblParserFormat EBL Format
 * @{
 * @brief EBL file specification
 * @details
 *   Specification of the EBL file format. Each struct represents a valid
 *   EBL tag, and an EBL file contains a series of tags.
 ******************************************************************************/

// -----------------------------------------------------------------------------
// EBL version

/// Magic word indicating EBL image
#define EBL_IMAGE_MAGIC_WORD                0xE350
/// Major version of the EBL spec
#define EBL_COMPATIBILITY_MAJOR_VERSION     0x0200

// -------------------------------
// Tag IDs

/// Tag ID for the EBL header tag
#define EBL_TAG_ID_HEADER                   0x0000
/// Tag ID for the EBL metadata tag
#define EBL_TAG_ID_METADATA                 0xF608
/// Tag ID for the EBL flash program tag
#define EBL_TAG_ID_PROG                     0xFE01
/// Tag ID for the EBL flash erase&program tag
#define EBL_TAG_ID_ERASEPROG                0xFD03
/// Tag ID for the EBL end tag
#define EBL_TAG_ID_END                      0xFC04

// Encryption-related tags
/// Tag ID for the EBL encryption header tag
#define EBL_TAG_ID_ENC_HEADER               0xFB05
/// Tag ID for the EBL encryption init tag
#define EBL_TAG_ID_ENC_INIT                 0xFA06
/// Tag ID for the EBL encryption data tag
#define EBL_TAG_ID_ENC_EBL_DATA             0xF907

// Signature-related tags
/// Tag ID for the EBL encryption MAC tag
#define EBL_TAG_ID_ENC_MAC                  0xF709
/// Tag ID for the EBL ECDSA secp256r1 signature tag
#define EBL_TAG_ID_SIGNATURE_ECDSA_P256     0xF70A

// -------------------------------
// EBL types

/// EBL type: Unencrypted EBL
#define EBL_TYPE_ENCRYPTION_NONE            0x0000
/// EBL type: AES-CCM encrypted EBL
#define EBL_TYPE_ENCRYPTION_AESCCM          0x0001

// -------------------------------
// Structs

/// EBL tag header. Must be the first element in all EBL tags.
typedef struct {
  uint16_t  tagId;              ///< Tag ID
  uint16_t  length;             ///< Length (in bytes) of data after this member
} EblTagHeader_t;

/// EBL header tag type.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint16_t  version;            ///< Version of the EBL spec used in this file
  uint16_t  magicWord;          ///< Magic word (EBL_IMAGE_MAGIC_WORD)
  uint32_t  flashStartAddress;  ///< Address where AAT is located
  uint32_t  aatCrc;             ///< CRC over the AAT
  uint8_t   aatBuffer[128];     ///< Buffer of the AAT contents
} EblHeader_t;

/// EBL metadata tag type.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint8_t*  metaData;           ///< Pointer to metadata contained in this tag
} EblMetadata_t;

/// EBL flash program tag type. Used with both PROG and ERASEPROG tags.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint32_t  flashStartAddress;  ///< Address to start flashing
  uint8_t   *data;              ///< Pointer to data to flash
} EblProg_t;

/// EBL end tag type.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint32_t  eblCrc;             ///< CRC32 of the entire EBL file.
} EblEnd_t;

/// EBL encryption header tag type.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint16_t  version;            ///< Version of the EBL spec used to create this file
  uint16_t  encryptionType;     ///< Type of encryption used. 1 = AES-CCM
  uint16_t  magicWord;          ///< Magic word (EBL_IMAGE_MAGIC_WORD)
} EblEncryptionHeader_t;

/// EBL encryption init tag type. Used with AES-CCM encryption.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint32_t  msgLen;             ///< Length of the cipher text in bytes
  uint8_t   nonce[12];          ///< Random nonce used for AES-CCM in this message
  uint8_t   *associatedData;    ///< Data that is authenticated but unencrypted
} EblEncryptionInitAesCcm_t;

/// EBL encryption data tag type.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint8_t   *encryptedEblData;  ///< Encrypted data. After decryption, this data must represent an integer number of unencrypted EBL tags.
} EblEncryptionData_t;

/// EBL encryption AES-CCM MAC tag type.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint8_t   eblMac[16];         ///< AES-CCM MAC
} EblEncryptionAesCcmSignature_t;

/// EBL ECDSA secp256r1 signature tag type.
typedef struct {
  EblTagHeader_t header;        ///< Tag ID and length
  uint8_t   r[32];              ///< R-point of ECDSA secp256r1 signature
  uint8_t   s[32];              ///< S-point of ECDSA secp256r1 signature
} EblSignatureEcdsaP256_t;

/** @} addtogroup EblParserFormat */
/** @} addtogroup EblParser */
/** @} addtogroup ImageParser */
/** @} addtogroup Plugin */

#endif // BTL_EBL_FORMAT_H