/***************************************************************************//**
 * @file btl_ebl_parser.h
 * @brief EBL image file parser.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#ifndef BTL_EBL_PARSER_H
#define BTL_EBL_PARSER_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "plugin/parser/btl_image_parser_interface.h"
#include "plugin/security/btl_security_aes.h"
#include "plugin/security/btl_security_sha256.h"

/***************************************************************************//**
 * @addtogroup Plugin
 * @{
 * @addtogroup ImageParser Image Parser
 * @{
 * @addtogroup EblParser EBL Parser
 * @{
 * @brief EBL parser implementation.
 * @details
 *   Image parser for EBL files. Parses EBL files based on the
 *   [EBL file format specification](@ref EblParserFormat). Callbacks are used
 *   to present data and metadata contents of the EBL file to the bootloader.
 ******************************************************************************/

// --------------------------------
// EBL parser error codes

/// Error code group: EBL parser
#define EBL_PARSER_ERROR_BASE             (0x00010000)
/// Error code: EBL successfully parsed
#define EBL_PARSER_ERROR_OK               (0x00000000)
/// Internal error code: All data consumed
#define EBL_PARSER_ERROR_CONTINUE         (EBL_PARSER_ERROR_BASE | 0x0000)
/// Error code: Parser encountered an unexpected tag ID
#define EBL_PARSER_ERROR_UNEXPECTED_TAG   (EBL_PARSER_ERROR_BASE | 0x0001)
/// @brief Error code: Parser detected an encrypted image, but invalid decoded
/// data, and assumes it means the AES key with which the image was encrypted
/// doesn't match the key stored on this device.
#define EBL_PARSER_ERROR_WRONG_KEY        (EBL_PARSER_ERROR_BASE | 0x0002)
/// @brief Error code: Parser ran out of buffer space. Try decreasing the chunk
/// size of data being pushed into the parser.
#define EBL_PARSER_ERROR_BUFFER           (EBL_PARSER_ERROR_BASE | 0x0003)
/// Error code: Unknown tag ID
#define EBL_PARSER_ERROR_UNSUPPORTED_TAG  (EBL_PARSER_ERROR_BASE | 0x0004)
/// Error code: More data is being pushed after the image is finalized or
/// parser has errored out
#define EBL_PARSER_ERROR_EOF              (EBL_PARSER_ERROR_BASE | 0x0005)
/// Error code: Image file format version doesn't match between file and parser
#define EBL_PARSER_ERROR_VERSION_MISMATCH (EBL_PARSER_ERROR_BASE | 0x0006)
/// Error code: Magic word is not present or valid in the header
#define EBL_PARSER_ERROR_NO_HEADER        (EBL_PARSER_ERROR_BASE | 0x0007)
/// Error code: CRC32 of the image file is wrong
#define EBL_PARSER_ERROR_CRC              (EBL_PARSER_ERROR_BASE | 0x0008)
/// Error code: Unknown encryption method was used
#define EBL_PARSER_ERROR_UNKNOWN_ENCRYPT  (EBL_PARSER_ERROR_BASE | 0x0009)

/// State in the EBL parser state machine
typedef enum {
  EblParserStateInit,                 ///< Initial state
  EblParserStateIdle,                 ///< Idle state
  EblParserStateHeader,               ///< Parsing header tag
  EblParserStateMetadata,             ///< Parsing metadata tag
  EblParserStateProg,                 ///< Parsing flash program tag
  EblParserStateEraseProg,            ///< Parsing flash erase&program tag
  EblParserStateFinalize,             ///< Finalizing file
  EblParserStateDone,                 ///< Parsing complete
  EblParserStateEncryptionInit,       ///< Parsing encryption init tag
  EblParserStateEncryptionHeader,     ///< Parsing encryption header tag
  EblParserStateEncryptionContainer,  ///< Parsing encryption data tag
  EblParserStateEncryptionMac,        ///< Parsing encryption MAC tag
  EblParserStateSignature,            ///< Parsing signature tag
  EblParserStateError                 ///< Error state
} EblParserState_t;

/// EBL parser buffer
typedef struct {
  /// Buffer contents
  uint8_t           buffer[64];
  /// Amount of bytes present in buffer
  size_t            length;
  /// Current reading offset into the buffer (circular)
  size_t            offset;
} EblBuffer_t;

/// Image parser context definition
typedef struct {
  /// State of the EBL parser state machine
  EblParserState_t    internalState;
  /// Buffer to handle unaligned incoming data
  EblBuffer_t         localBuffer;
  /// AES-CCM decryption (= AES-CTR) context
  BtlAesCtrContext_t  aesContext;
  /// SHA256 hashing context
  BtlSha256State_t    shaContext;
  /// Total length of the tag currently being parsed
  size_t              lengthOfTag;
  /// Current offset into tag being parsed
  size_t              offsetInTag;
  /// Total length of current encrypted data block
  size_t              lengthOfEncryptedTag;
  /// Offset into current encrypted data block
  size_t              offsetInEncryptedTag;
  /// Current address the image needs to be written to
  uint32_t            programmingAddress;
  /// Current offset of metadata being handled (starts at 0)
  uint32_t            metadataAddress;
  /// Running CRC-32 over the incoming EBL file
  uint32_t            fileCrc;
  /// Indicates we're handling an encrypted EBL file
  bool                encrypted;
  /// Indicates the parser is currently inside an encrypted block
  bool                inEncryptedContainer;
  /// Indicates the parser has parsed an ECDSA signature
  bool                gotSignature;
} EblParserContext_t;

/** @} addtogroup EblParser */
/** @} addtogroup ImageParser */
/** @} addtogroup Plugin */

#endif // BTL_EBL_PARSER_H