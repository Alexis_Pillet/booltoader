/***************************************************************************//**
 * @file btl_image_parser_interface.h
 * @brief Definition of the interface between the core bootloader and the
 *   image file parser.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#ifndef BTL_IMAGE_PARSER_INTERFACE_H
#define BTL_IMAGE_PARSER_INTERFACE_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/***************************************************************************//**
 * @addtogroup Plugin
 * @{
 * @addtogroup ImageParser Image Parser
 * @{
 * @brief Image parser plugin
 * @details
 *   Generic image parser interface for Silicon Labs bootloader.
 ******************************************************************************/

/// Structure containing state of the image file processed
typedef struct {
  /// Version number extracted from image file, used for downgrade prevention
  uint32_t    imageVersion;
  /// Flag to indicate image contains a Second Stage Bootloader
  bool        imageContainsSsb;
  /// Flag to indicate image contains an application
  bool        imageContainsApp;
  /// Flag to indicate parsing has completed
  bool        imageCompleted;
  /// Flag to indicate the image file has been validated
  bool        imageVerified;
} BtlImageProperties_t;

/// Write modes for the image data callback.
typedef enum {
  /// Data needs to be written without triggering an erase
  BtlWrite,
  /// Data needs to be written, and a page erase should happen if needed.
  BtlWriteWithErase
} BtlWriteMode_t;

/***************************************************************************//**
 * Pass the raw image data from the parser to the writing implementation
 *
 * @param address Address (inside the raw image) the data starts at
 * @param data Raw image data
 * @param length Size in bytes of raw image data. Constrained to always be
 *   a multiple of four.
 * @param mode Mode with which to write the data (if relevant).
 * @param callbackContext A context variable defined by the implementation that
 *   is implementing this callback.
 ******************************************************************************/
typedef void (*BtlPassImageDataFunc_t)(uint32_t       address, 
                                       uint8_t        *data, 
                                       size_t         length, 
                                       BtlWriteMode_t mode,
                                       void           *callbackContext);

/***************************************************************************//**
 * Pass metadata contained in the image from the parser to the implementation
 *
 * @param offset Offset of metadata (byte counter incrementing from 0)
 * @param data Raw metadata
 * @param length Size in bytes of raw metadata. Constrained to always be
 *   a multiple of four.
 * @param callbackContext A context variable defined by the implementation that
 *   is implementing this callback.
 ******************************************************************************/
typedef void (*BtlPassMetaDataFunc_t)(uint32_t  offset,
                                      uint8_t   *data, 
                                      size_t    length,
                                      void      *callbackContext);

/// Function pointers to parser callbacks
typedef struct {
  /// Opaque pointer passed to the callback functions
  void                    *callbackContext;
  /// Callback function pointer for image data
  BtlPassImageDataFunc_t  dataFunction;
  /// Callback function pointer for image metadata
  BtlPassMetaDataFunc_t   metaDataFunction;
} BtlImageDataCallbacks_t;

/***************************************************************************//**
 * Initialize the parser's context
 *
 * @param context Pointer to the specific parser's context variable
 * @return 0 if OK, error code otherwise.
 ******************************************************************************/
uint32_t btl_startParseImage(void *context);

/***************************************************************************//**
 * Parse an image file in order to extract the binary and some metadata.
 *
 * @param context Pointer to the specific parser's context variable
 * @param imageProperties Pointer to the image file state variable
 * @param buffer Pointer to byte array containing data to parse
 * @param length Size in bytes of the data in buffer
 * @param callbacks Struct containing function pointers to be called by the
 *   parser to pass the extracted binary data back to BTL.
 * @returns 0 if OK, error code (parser-specific) otherwise.
 *
 * Pushes data into the image file parser to be parsed.
 ******************************************************************************/
uint32_t btl_parseImageData(void                    *context, 
                            BtlImageProperties_t    *imageProperties, 
                            uint8_t                 *buffer, 
                            size_t                  length, 
                            BtlImageDataCallbacks_t *callbacks);

/** @} addtogroup ImageParser */
/** @} addtogroup Plugin */

#endif // BTL_IMAGE_PARSER_INTERFACE