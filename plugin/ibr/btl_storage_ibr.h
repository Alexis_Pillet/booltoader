/***************************************************************************//**
 * @file btl_storage_ibr.h
 * @brief IBR plugin for Silicon Labs Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#ifndef BTL_STORAGE_IBR_H
#define BTL_STORAGE_IBR_H

#include <stdint.h>
#include <stddef.h>
#include "plugin/storage/btl_storage.h"

/***************************************************************************//**
 * @addtogroup Plugin
 * @{
 * @addtogroup Ibr
 * @{
 * @brief Indirect Boot Record Plugin
 * @details
 *   This plugin provides the bootloader with support for storing multiple
 *   images. The Indirect Boot Record is fixed at address 0 of the storage
 *   medium, and points to the image to be installed next.
 ******************************************************************************/

/// Definition of the Indirect Boot Record
typedef struct {
  /// "SLABIBR"
  uint8_t             magic[8];
  /// Major component of version number
  uint16_t            versionMajor;
  /// Minor component of version number
  uint16_t            versionMinor;

  /// Reserved bytes according to IBR spec
  uint8_t             reserved[12];

  /// Slot to mark for app upgrade. 0xFFFFFFFF is nothing marked.
  /// After successful upgrade, this gets flashed to zero.
  uint32_t            appUpgradeAddress;
  /// Slot to mark for second stage upgrade. 0xFFFFFFFF is nothing marked.
  /// After successful upgrade, this gets flashed to zero.
  uint32_t            bootloaderUpgradeAddress;
  
  /// Checksum of the IBR
  uint32_t            crc32;
  /// Checksum of the IBR when upgrade completed
  uint32_t            crc32AfterUpgrade;
} IndirectBootRecord_t;

/***************************************************************************//**
 * Parse an IBR record
 *
 * @param ibr Pointer to raw IBR record
 * @param btlInfo Pointer to BootloadInfo_t struct to put the extracted info.
 *
 * @return True if a valid record was found and parsed, false otherwise.
 *
 ******************************************************************************/
bool storage_parseIbr(IndirectBootRecord_t* ibr, BootloadInfo_t* btlInfo);

/***************************************************************************//**
 * Construct an IBR record
 *
 * @param ibr Pointer to an IBR structure to fill out
 * @param appSlotId Slot ID of app to mark for bootloading
 * @param bootloaderSlotId Slot ID of bootloader upgrade image to mark for
 *   bootloading (-1 if not applicable).
 *
 ******************************************************************************/
void storage_createIbr(IndirectBootRecord_t* ibr,
                       int appSlotId,
                       int bootloaderSlotId);

/** @} // addtogroup Ibr */
/** @} // addtogroup Plugin */

#endif
