#include "plugin/ibr/btl_storage_ibr.h"
#include "plugin/security/btl_crc32.h"

#include <string.h>

bool storage_parseIbr(IndirectBootRecord_t* ibr, BootloadInfo_t* btlInfo)
{
  volatile uint32_t crc = BTL_CRC32_START;
  unsigned int i;
  BootloaderStorageInformation_t info;
  BootloaderStorageSlot_t slot;

  btlInfo->appSlotId = -1;
  btlInfo->bootloaderSlotId = -1;

  // Validate magic string
  if(strcmp((const char*)ibr->magic, "SLABIBR") != 0) {
    return false;
  }

  // Validate IBR up until the first CRC
  crc = btl_crc32Stream((uint8_t*)ibr, 
                        (size_t)(sizeof(*ibr) - 4), 
                        crc);

  if(crc == BTL_CRC32_END) {
    // IBR is valid, find slot belonging to this address
    storage_getInfo(&info);
    for(i = 0; i < info.numStorageSlots; i++) {
      // Check if we can find a matching slot ID
      if (storage_getSlotInfo(i, &slot) == BOOTLOADER_OK) {
        if(slot.address == ibr->appUpgradeAddress) {
          btlInfo->appSlotId = i;
        }
        if(slot.address == ibr->bootloaderUpgradeAddress) {
          btlInfo->bootloaderSlotId = i;
        }
      } else {
        return false;
      }
    }
    return true;
  } else {
    // Try second CRC to see whether second stage upgrade was
    //  completed previously
    crc = btl_crc32Stream(((uint8_t*)ibr) + (sizeof(*ibr) - 4),
                          (size_t)(4), 
                          crc);

    if(crc == BTL_CRC32_END) {
      storage_getInfo(&info);
      // Check if we can find a matching slot ID
      for(i = 0; i < info.numStorageSlots; i++) {
        if (storage_getSlotInfo(i, &slot) == BOOTLOADER_OK) {
          if(slot.address == ibr->appUpgradeAddress) {
            btlInfo->appSlotId = i;
          }
        } else {
          return false;
        }
      }
      // We found a valid CRC indicating an app upgrade is pending
      //  after having upgraded the bootloader
      return true;
    } else {
      return false;
    }
  }
}

void storage_createIbr(IndirectBootRecord_t* ibr,
                       int appSlotId,
                       int bootloaderSlotId)
{
  volatile uint32_t crc, btl;

  strcpy((char*)ibr->magic, "SLABIBR");
  ibr->versionMajor = 1;
  ibr->versionMinor = 0;
  for(unsigned int i = 0; i < sizeof(ibr->reserved); i++) {
    ibr->reserved[i] = 0xFF;
  }

  BootloaderStorageSlot_t slotInfo;

  if(appSlotId != -1
     && storage_getSlotInfo(appSlotId, &slotInfo) == BOOTLOADER_OK) {
    ibr->appUpgradeAddress = slotInfo.address;
  } else {
    ibr->appUpgradeAddress = 0xFFFFFFFFUL;
  }

  if(bootloaderSlotId != -1
     && storage_getSlotInfo(bootloaderSlotId, &slotInfo) == BOOTLOADER_OK) {
    ibr->bootloaderUpgradeAddress = slotInfo.address;
  } else {
    ibr->bootloaderUpgradeAddress = 0xFFFFFFFFUL;
  }

  // Calculate initial CRC32
  crc = BTL_CRC32_START;
  crc = btl_crc32Stream((uint8_t*)ibr, 
                        (size_t)(sizeof(*ibr) - 8), 
                        crc);
  ibr->crc32 = ~crc;

  // Calculate CRC when SSB upgrade gets done
  btl = ibr->bootloaderUpgradeAddress;
  ibr->bootloaderUpgradeAddress = 0;
  crc = BTL_CRC32_START;

  crc = btl_crc32Stream((uint8_t*)ibr, 
                        (size_t)(sizeof(*ibr) - 4), 
                        crc);

  ibr->crc32AfterUpgrade = ~crc;
  ibr->bootloaderUpgradeAddress = btl;
}