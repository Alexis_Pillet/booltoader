/***************************************************************************//**
 * @file btl_debug.c
 * @brief Debug plugin for Silicon Labs Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include "btl_debug.h"

#if defined (BTL_PLUGIN_DEBUG_PRINT)

#include <string.h>

static void btl_debugWriteNibbleHex(char nibble)
{
  nibble = nibble > 9 ? nibble + 0x37 : nibble + 0x30;
  BTL_DEBUG_PRINTC(nibble);
}

void btl_debugWriteCharHex(uint8_t number)
{
  btl_debugWriteNibbleHex((number & 0xF0) >> 4);
  btl_debugWriteNibbleHex((number & 0x0F) >> 0);
}

void btl_debugWriteShortHex(uint16_t number)
{
  btl_debugWriteCharHex((number & 0xFF00) >> 8);
  btl_debugWriteCharHex((number & 0x00FF) >> 0);
}

void btl_debugWriteWordHex(uint32_t number)
{
  btl_debugWriteShortHex((number & 0xFFFF0000UL) >> 16);
  btl_debugWriteShortHex((number & 0x0000FFFFUL) >> 0);
}

void btl_debugWriteString(char * s)
{
  for (size_t i = 0; i < strlen(s); i++) {
    btl_debugWriteChar(s[i]);
  }
}

#endif // BTL_PLUGIN_DEBUG_PRINT
