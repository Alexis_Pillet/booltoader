/***************************************************************************//**
 * @file btl_debug_uart.c
 * @brief UART debug plugin for Silicon Labs Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "btl_debug.h"

#if defined(BTL_PLUGIN_DEBUG_PRINT) && defined(BTL_DEBUG_PRINT_UART)

#include "em_device.h"

void btl_debugInit(void)
{
  // Configure clocks
  CMU->CTRL |= CMU_CTRL_HFPERCLKEN;
  CMU->HFBUSCLKEN0 |= CMU_HFBUSCLKEN0_GPIO;
  CMU->HFPERCLKEN0 |= BTL_DEBUG_UART_CLOCK;

  /* Make sure disabled first, before resetting other registers */
  BTL_DEBUG_UART->CMD = USART_CMD_RXDIS | USART_CMD_TXDIS | USART_CMD_MASTERDIS
                               | USART_CMD_RXBLOCKDIS | USART_CMD_TXTRIDIS | USART_CMD_CLEARTX
                               | USART_CMD_CLEARRX;
  BTL_DEBUG_UART->CTRL      = _USART_CTRL_RESETVALUE;
  BTL_DEBUG_UART->FRAME     = _USART_FRAME_RESETVALUE;
  BTL_DEBUG_UART->TRIGCTRL  = _USART_TRIGCTRL_RESETVALUE;
  BTL_DEBUG_UART->CLKDIV    = _USART_CLKDIV_RESETVALUE;
  BTL_DEBUG_UART->IEN       = _USART_IEN_RESETVALUE;
  BTL_DEBUG_UART->IFC       = _USART_IFC_MASK;
  BTL_DEBUG_UART->ROUTEPEN  = _USART_ROUTEPEN_RESETVALUE;
  BTL_DEBUG_UART->ROUTELOC0 = _USART_ROUTELOC0_RESETVALUE;
  BTL_DEBUG_UART->ROUTELOC1 = _USART_ROUTELOC1_RESETVALUE;

  // Configure databits, stopbits and parity
  BTL_DEBUG_UART->FRAME = USART_FRAME_DATABITS_EIGHT
                                | USART_FRAME_STOPBITS_ONE
                                | USART_FRAME_PARITY_NONE;

  // Configure oversampling and baudrate
  BTL_DEBUG_UART->CTRL |= USART_CTRL_OVS_X16;
  if(CMU->HFCLKSTATUS == CMU_HFCLKSTATUS_SELECTED_HFRCO) {
    BTL_DEBUG_UART->CLKDIV = 2384; // 19 MHz refclk => 115200 baud
  } else {
    BTL_DEBUG_UART->CLKDIV = 5080; // 38.4 MHz refclk => 115200 baud
  }
  

  // Configure TX pin
  uint32_t port = BTL_DEBUG_UART_TX_PORT;
  uint32_t pin = BTL_DEBUG_UART_TX_PIN;
#if BTL_DEBUG_UART_TX_PORT < 8
  GPIO->P[port].MODEL = (GPIO->P[port].MODEL
                         & ~(_GPIO_P_MODEL_MODE0_MASK << (pin*4)))
                        | (GPIO_P_MODEL_MODE0_PUSHPULL << (pin*4));
#else
  GPIO->P[port].MODEH = (GPIO->P[port].MODEH
                         & ~(_GPIO_P_MODEL_MODE0_MASK << ((pin-8)*4)))
                        | (GPIO_P_MODEL_MODE0_PUSHPULL << ((pin-8)*4));
#endif

  // Configure enable pin
#if defined(BTL_DEBUG_UART_EN_PORT)
  port = BTL_DEBUG_UART_EN_PORT;
  pin = BTL_DEBUG_UART_EN_PIN;
#if BTL_DEBUG_UART_EN_PORT < 8
  GPIO->P[port].MODEL = (GPIO->P[port].MODEL
                         & ~(_GPIO_P_MODEL_MODE0_MASK << (pin*4)))
                        | (GPIO_P_MODEL_MODE0_PUSHPULL << (pin*4));
#else
  GPIO->P[port].MODEH = (GPIO->P[port].MODEH
                         & ~(_GPIO_P_MODEL_MODE0_MASK << ((pin-8)*4)))
                        | (GPIO_P_MODEL_MODE0_PUSHPULL << ((pin-8)*4));
#endif
  GPIO->P[port].DOUT |= 1 << pin;
#endif

  // Configure route
  BTL_DEBUG_UART->ROUTELOC0 = BTL_DEBUG_UART_TX_LOCATION << _USART_ROUTELOC0_TXLOC_SHIFT;
  BTL_DEBUG_UART->ROUTEPEN = USART_ROUTEPEN_TXPEN;

  // Start transmitter
  BTL_DEBUG_UART->CMD = USART_CMD_TXEN;
}

void btl_debugWriteChar(char c)
{
  while (!(BTL_DEBUG_UART->STATUS & USART_STATUS_TXBL));

  BTL_DEBUG_UART->TXDATA = c;
}

#endif // BTL_DEBUG_PRINT_UART
