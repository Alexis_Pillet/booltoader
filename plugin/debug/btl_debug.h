/***************************************************************************//**
 * @file btl_debug.h
 * @brief Debug plugin for Silicon Labs Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef BTL_DEBUG_H
#define BTL_DEBUG_H

#include BTL_CONFIG_FILE

/**
 * @addtogroup Plugin
 * @{
 * @addtogroup Debug
 * @{
 * @brief Debug Plugin
 * @details
 *   This plugin provides the bootloader with support for debugging output.
 *   There are two levels of debug output -- defining @ref BTL_PLUGIN_DEBUG_FAILURE
 *   enables debug output if the bootloader fails, while defining
 *   @ref BTL_PLUGIN_DEBUG_PRINT enables debug prints at strategic points in the
 *   code.
 */

// Give debug output when a failure occurs
#if defined(BTL_PLUGIN_DEBUG_FAILURE)

void btl_debugFailure(void);

#define BTL_DEBUG_FAILURE() btl_debugFailure()

#else // No debug failure report

/// Signal that the bootloader experienced a failure
#define BTL_DEBUG_FAILURE() while (1)

#endif

// Print debug information throughout the bootloader
#if defined(BTL_PLUGIN_DEBUG_PRINT)

void btl_debugInit(void);
void btl_debugWriteChar(char c);
void btl_debugWriteString(char * s);

void btl_debugWriteCharHex(uint8_t number);
void btl_debugWriteShortHex(uint16_t number);
void btl_debugWriteWordHex(uint32_t number);

#define BTL_DEBUG_INIT()      btl_debugInit()
#define BTL_DEBUG_PRINT(str)  btl_debugWriteString(str)
#define BTL_DEBUG_PRINTC(chr) btl_debugWriteChar(chr)

#define BTL_DEBUG_PRINT_CHAR_HEX(number)  btl_debugWriteCharHex(number)
#define BTL_DEBUG_PRINT_SHORT_HEX(number) btl_debugWriteShortHex(number)
#define BTL_DEBUG_PRINT_WORD_HEX(number)  btl_debugWriteWordHex(number)

#else // No debug prints

/// Initialize debug channel
#define BTL_DEBUG_INIT() while (0)

/// Print a string to debug out
#define BTL_DEBUG_PRINT(str) (void)(str)
/// Print a character to debut out
#define BTL_DEBUG_PRINTC(chr) (void)(chr)
/// Print a single hex byte
#define BTL_DEBUG_PRINT_CHAR_HEX(number)  (void)(number)
/// Print two hex bytes
#define BTL_DEBUG_PRINT_SHORT_HEX(number) (void)(number)
/// Print a hex word
#define BTL_DEBUG_PRINT_WORD_HEX(number)  (void)(number)

#endif


/**
 * @} // addtogroup Debug
 * @} // addtogroup Plugin
 */

#endif // BTL_DEBUG_H
