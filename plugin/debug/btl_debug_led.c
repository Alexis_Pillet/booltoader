/***************************************************************************//**
 * @file btl_debug_led.c
 * @brief Debug LED implementation for Silicon Labs Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "btl_debug.h"

#if defined(BTL_PLUGIN_DEBUG_FAILURE) && defined(BTL_DEBUG_FAILURE_LED)

#include "em_cmu.h"
#include "em_gpio.h"

void btl_debugFailure(void)
{
  CMU_ClockEnable(cmuClock_GPIO, true);
  GPIO_PinModeSet(BTL_DEBUG_LED_PORT,
                  BTL_DEBUG_LED_PIN,
                  gpioModePushPull,
                  1);

  while (1) {
    for (volatile int i = 0; i < 1000000; i++);
    GPIO_PinOutClear(BTL_DEBUG_LED_PORT, BTL_DEBUG_LED_PIN);
    for (volatile int i = 0; i < 1000000; i++);
    GPIO_PinOutSet(BTL_DEBUG_LED_PORT, BTL_DEBUG_LED_PIN);
  }
}

#endif // BTL_DEBUG_FAILURE_LED
