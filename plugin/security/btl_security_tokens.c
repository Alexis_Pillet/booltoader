#include "btl_security_tokens.h"
#include "em_device.h"

uint8_t* btl_getSignedBootloaderKeyXPtr(void)
{
  return (uint8_t*)(LOCKBITS_BASE | 0x34A);
}

uint8_t* btl_getSignedBootloaderKeyYPtr(void)
{
  return (uint8_t*)(LOCKBITS_BASE | 0x36A);
}

uint8_t* btl_getImageFileEncryptionKeyPtr(void)
{
  return (uint8_t*)(LOCKBITS_BASE | 0x286);
}