#include "btl_crc32.h"

#define CRC32_POLYNOMIAL              (0xEDB88320L)

/**************************************************************************//**
 * Calculate CRC32 on input
 *
 * @param newByte Byte to append to CRC32 calculation
 * @param prevResult Previous output from CRC algorithm. Polynomial if starting
 *   a new calculation.
 * @returns Result of the CRC32 operation
 *****************************************************************************/
uint32_t btl_crc32(const uint8_t newByte, uint32_t prevResult)
{
  uint8_t jj;
  uint32_t previous;
  uint32_t oper;

  previous = (prevResult >> 8) & 0x00FFFFFFL;
  oper = (prevResult ^ newByte) & 0xFF;
  for (jj = 0; jj < 8; jj++) {
    oper = ((oper & 0x01)
                ? ((oper >> 1) ^ CRC32_POLYNOMIAL)
                : (oper >> 1));
  }

  return (previous ^ oper);
}

/**************************************************************************//**
 * Calculate CRC32 on input stream
 *
 * @param buffer buffer containing bytes to append to CRC32 calculation
 * @param length Size of the buffer in bytes
 * @param prevResult Previous output from CRC algorithm. Polynomial if starting
 *   a new calculation.
 * @returns Result of the CRC32 operation
 *****************************************************************************/
uint32_t btl_crc32Stream(const uint8_t* buffer, size_t length, uint32_t prevResult)
{
  size_t position = 0;
  for(;position < length; position++) {
    prevResult = btl_crc32(buffer[position], prevResult);
  }

  return prevResult;
}