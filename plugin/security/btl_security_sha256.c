#include "btl_security_sha256.h"

#if BTL_SECURITY_SHA256_DIGEST_LENGTH % 4 != 0
#error "SHA digest size is not a multiple of native data type"
#endif

/** This function will initialize the CCM state struct and must be called
 *  before using the struct in any processing.
 */
void btl_initSha256(BtlSha256State_t *context)
{
  mbedtls_sha256_init(&(context->shaContext));       //Zero out the context struct
  mbedtls_sha256_starts(&(context->shaContext), 0);  //Load the SHA256 IV
}

/** Push data into the SHA algorithm. If the data is not a full SHA block,
 *  mbedTLS will buffer until it has a full one.
 */
void btl_updateSha256(BtlSha256State_t *context, const uint8_t *data, size_t length) 
{
  mbedtls_sha256_update(&(context->shaContext), data, length);
}

/** Finalize the SHA hash. This will run the remainder of the buffer through
 *  the algorithm, padding and adding the counter as necessary.
 */
void btl_finalizeSha256(BtlSha256State_t *context)
{
  mbedtls_sha256_finish(&(context->shaContext), context->sha);
}

/** Verify the SHA hash contained in shaState with the one in the byte array
 *  pointed to. Check the length, too.
 */
bool btl_verifySha256(BtlSha256State_t *context, const uint8_t *sha) 
{
  unsigned int* sha_calculated = (unsigned int*)context->sha;
  unsigned int* sha_verifying  = (unsigned int*)sha;

  if(context == NULL || sha == NULL) {
    return false;
  }

  for(unsigned int word = 0; word < (BTL_SECURITY_SHA256_DIGEST_LENGTH / sizeof(unsigned int)); word++) {
    if(sha_verifying[word] != sha_calculated[word]) {
      return false;
    }
  }

  return true;
}