/***************************************************************************//**
 * @file ecc.c
 * @brief Elliptic Curve Cryptography (ECC) accelerator peripheral API
 * @version x.x.x
 *******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "em_device.h"
#if defined(CRYPTO_COUNT) && (CRYPTO_COUNT > 0)

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#include "em_crypto.h"
#include "em_assert.h"
#include "ecc.h"

/***************************************************************************//**
 * @addtogroup CRYPTOLIB
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * @addtogroup ECC
 * @brief Elliptic Curve Cryptography API Library
 * @details
 *   This API is intended for use on Silicon Laboratories
 *
 *   TBW
 *
 *   References:
 *   @li Wikipedia - Elliptic curve cryptography,
 *      http://en.wikipedia.org/wiki/Elliptic_curve_cryptography
 *
 *   @li NIST FIPS 186-4, Digital Signature Standard (DSS), July 2013
 *      http://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.186-4.pdf
 * @{
 ******************************************************************************/

/*******************************************************************************
 ******************************   TYPEDEFS   ***********************************
 ******************************************************************************/

typedef struct
{
  ECC_BigInt_t  X;  /* x coordinate of point. */
  ECC_BigInt_t  Y;  /* y coordinate of point. */
  ECC_BigInt_t  Z;  /* z coordinate of point. */
} ECC_Projective_Point_t;

typedef enum
{
#ifdef INCLUDE_ECC_P192
  eccCurveId_X962_P192,  /* secp192r1: NIST/SECG X9.62 curve over a 192 bit prime field */
#endif
#ifdef INCLUDE_ECC_P224
  eccCurveId_X962_P224,  /* secp224r1: NIST/SECG X9.62 curve over a 224 bit prime field */
#endif
#ifdef INCLUDE_ECC_P256
  eccCurveId_X962_P256,  /* secp256r1: NIST/SECG X9.62 curve over a 256 bit prime field */
#endif
  eccCurveIdMax
} ECC_CurveId_t;

/** Structure definintion of NIST GF(p) and GF(2m) curves. */
typedef struct {

  char*              name;

  /** The field size in octets. */
  int                size;

  /** The field size in bits. */
  int                bitSize;

  /** Prime (p) modulus identifier defined by the crypto hw.*/
  CRYPTO_ModulusId_TypeDef primeModulusId;

  /** Order (n) modulus identifier defined by the crypto hw.*/
  CRYPTO_ModulusId_TypeDef orderModulusId;

  /** Cofactor (h) of the curve */
  int                cofactor;

  /** The prime (p) of the curve */
  ECC_BigInt_t prime;

  /** The order (n) of the curve */
  ECC_BigInt_t order;
  
  /** The base point on the curve */
  ECC_Point_t  G;

  /** The bit mask 'L' used to generate the "bar" values in the ECMQV
      algorithm.*/
  ECC_BigInt_t L;

} ECC_Curve_Params_t;

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */

#define BIGINT_BYTES_PER_WORD  (sizeof(uint32_t))

/* Evaluates to one if bit number bitno of bn is set to one. */
#define BIGINT_BIT_IS_ONE(bn, bitno) (bn[bitno/32]&(1<<bitno%32))

#define EC_BIGINT_COPY(X, Y) memcpy(X, Y, sizeof(ECC_BigInt_t));

#define ECC_CLEAR_CRYPTO_CTRL crypto->CTRL = 0; \
  crypto->SEQCTRL = 0;                          \
  crypto->SEQCTRLB = 0

// Since we are not doing ECC encryption, but just authentication, there is
//   no danger to leak our private key through side channel attacks.
#if 0
/* In ECC_PointMul, use dummy addition in order to protect against side channel
   attacks. */
#define USE_DUMMY_ADD
static ECC_Projective_Point_t DummyResult;
#endif

#if 1
#define ISSUE_NOP_AFTER_EXEC
#endif

/** @endcond */

/*******************************************************************************
 **************************     STATIC DATA      *******************************
 ******************************************************************************/

static const ECC_Curve_Params_t ECC_Curve_Params [eccCurveIdMax] =
{
#ifdef INCLUDE_ECC_P192
  {
    "secp192r1",
    /* field size in bytes */
    24,
    /* field size in bits */
    192,
    /* CRYPTO module identifier for Prime modulus (p) */
    cryptoModulusEccP192,
    /* CRYPTO module identifier for Order modulus (n) */
    cryptoModulusEccP192Order,
    /* cofactor (h) */
    1,
    /* prime (p) */
    {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000},
    /* order (n) */
    {0xB4D22831, 0x146BC9B1, 0x99DEF836, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000},
    /* base point */
    {
      /* x coordinate of base point */
      {0x82FF1012, 0xF4FF0AFD, 0x43A18800, 0x7CBF20EB, 0xB03090F6, 0x188DA80E, 0x00000000, 0x00000000},
      /* y coordinate of base point */
      {0x1E794811, 0x73F977A1, 0x6B24CDD5, 0x631011ED, 0xFFC8DA78, 0x07192B95, 0x00000000, 0x00000000}
    },
    /* L */
    {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000001, 0x00000000, 0x00000000, 0x00000000, 0x00000000}
  },
#endif
#ifdef INCLUDE_ECC_P224
  {
    "secp224r1",
    /* field size in octets */
    28,
    /* field size in bits */
    224,
    /* CRYPTO module identifier for Prime modulus (p) */
    cryptoModulusEccP224,
    /* CRYPTO module identifier for Order modulus (n) */
    cryptoModulusEccP224Order,
    /* cofactor (h) */
    1,
    /* prime (p) */
    {0x00000001, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000},
    /* order (n) */
    {0x5C5C2A3D, 0x13DD2945, 0xE0B8F03E, 0xFFFF16A2, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000},
    /* base point */
    {
      /* x coordinate of base point */
      {0x115C1D21, 0x343280D6, 0x56C21122, 0x4A03C1D3, 0x321390B9, 0x6BB4BF7F, 0xB70E0CBD, 0x00000000},
      /* y coordinate of base point */
      {0x85007E34, 0x44D58199, 0x5A074764, 0xCD4375A0, 0x4C22DFE6, 0xB5F723FB, 0xBD376388, 0x00000000}
    },
    /* L */
    {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x0001FFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000}

  },
#endif
#ifdef INCLUDE_ECC_P256
  {
    "secp256r1",
    /* field size in octets */
    32,
    /* field size in bits */
    256,
    /* CRYPTO hw identifier for the Prime modulus (p) */
    cryptoModulusEccP256,
    /* CRYPTO hw identifier for the Order modulus (n) */
    cryptoModulusEccP256Order,
    /* Cofactor (h) */
    1,
    /* Prime (p) */
    {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000001, 0xFFFFFFFF},
    /* Order (n) */
    {0xFC632551, 0xF3B9CAC2, 0xA7179E84, 0xBCE6FAAD, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF},
    /* base point */
    {
      /* x coordinate of base point */
      {0xD898C296, 0xF4A13945, 0x2DEB33A0, 0x77037D81, 0x63A440F2, 0xF8BCE6E5, 0xE12C4247, 0x6B17D1F2},
      /* y coordinate of base point */
      {0x37BF51F5, 0xCBB64068, 0x6B315ECE, 0x2BCE3357, 0x7C0F9E16, 0x8EE7EB4A, 0xFE1A7F9B, 0x4FE342E2}
    },
    /* L */
    {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000001, 0x00000000, 0x00000000, 0x00000000}

  },
#endif
};

/*******************************************************************************
 ***********************   FORWARD DECLARATIONS    *****************************
 ******************************************************************************/

Ecode_t ECC_ProjectiveToAffine(CRYPTO_TypeDef            *crypto,
                               ECC_CurveId_t             curveId,
                               ECC_Projective_Point_t*   P,
                               ECC_Point_t*              R);


/*******************************************************************************
 **************************   STATIC FUNCTIONS   *******************************
 ******************************************************************************/

/* Returns true if the given curveId is a prime curve, or false if 2^n curve. */
bool eccPrimeCurve (ECC_CurveId_t curveId)
{
  switch (curveId)
  {
#ifdef INCLUDE_ECC_P192
    case eccCurveId_X962_P192:
      return true;
#endif
#ifdef INCLUDE_ECC_P224
    case eccCurveId_X962_P224:
      return true;
#endif
#ifdef INCLUDE_ECC_P256
    case eccCurveId_X962_P256:
      return true;
#endif
    default:
      /* Unknown curve identifier */
      EFM_ASSERT(false);
      return false;
  } /* switch (curveId) */
}

/* Get the name associated with the given ecc curve. */
char* eccCurveNameGet(ECC_CurveId_t  curveId, char* buf)
{
  if (curveId > eccCurveIdMax)
  {
    /* Unsupported curve id */
    return NULL;
  }

  return strcpy(buf, ECC_Curve_Params[curveId].name);
}

/* Get the field size in octets associated with the given ecc curve. */
int eccFieldSizeGet(ECC_CurveId_t  curveId)
{
  if (curveId > eccCurveIdMax)
  {
    /* Unsupported curve id */
    return -1;
  }

  return ECC_Curve_Params[curveId].size;
}

/* Get the field size in bits associated with the given ecc curve. */
int eccFieldBitSizeGet(ECC_CurveId_t  curveId)
{
  if (curveId > eccCurveIdMax)
  {
    /* Unsupported curve id */
    return -1;
  }

  return ECC_Curve_Params[curveId].bitSize;
}

/* Get the prime modulus type associated with the given ecc curve. */
CRYPTO_ModulusId_TypeDef eccPrimeModIdGet(ECC_CurveId_t  curveId)
{
  if (curveId > eccCurveIdMax)
  {
    /* Unsupported curve id */
    EFM_ASSERT(0);
  }
  return ECC_Curve_Params[curveId].primeModulusId;
}

/* Get the order modulus type associated with the given ecc curve. */
CRYPTO_ModulusId_TypeDef eccOrderModIdGet(ECC_CurveId_t  curveId)
{
  if (curveId > eccCurveIdMax)
  {
    /* Unsupported curve id */
    EFM_ASSERT(0);
  }
  return ECC_Curve_Params[curveId].orderModulusId;
}

/* Get the 'modulus' associated with the given ecc curve. */
Ecode_t eccPrimeGet(ECC_CurveId_t  curveId,
                      ECC_BigInt_t   p)
{
  if (curveId > eccCurveIdMax)
  {
    /* Unsupported curve id */
    return ECODE_BTL_ECC_INVALID_CURVE_ID;
  }
  memcpy (p, ECC_Curve_Params[curveId].prime, sizeof(ECC_BigInt_t));
  
  return ECODE_OK;
}

/* Get the 'order' associated with the given ecc curve. */
Ecode_t eccOrderGet(ECC_CurveId_t  curveId,
                    ECC_BigInt_t   order)
{
  if (curveId > eccCurveIdMax)
  {
    /* Unsupported curve id */
    return ECODE_BTL_ECC_INVALID_CURVE_ID;
  }

  memcpy(order, ECC_Curve_Params[curveId].order, sizeof(ECC_BigInt_t));

  return ECODE_OK;
}

#if defined( INCLUDE_ECC_VALIDATE_PUBLIC_KEY )
/* Returns the parameter b for the given ecc curve. */
static inline const uint32_t* eccGetB(ECC_CurveId_t  curveId)
{
  return ECC_Curve_Params[curveId].B;
}
#endif /* #if defined( INCLUDE_ECC_VALIDATE_PUBLIC_KEY ) */

/* Returns the base point for the fiven ecc curve. */
const ECC_Point_t* eccBasePointGet(ECC_CurveId_t  curveId)
{
  return &ECC_Curve_Params[curveId].G;
}

#if defined( INCLUDE_ECC_VALIDATE_PUBLIC_KEY )
/* Returns true if bigint is non-zero. */
static bool bigIntNonZero(ECC_BigInt_t bn)
{
  uint32_t *pbn=bn;
  int       size=sizeof(ECC_BigInt_t)/sizeof(uint32_t);
  for (; size && (0==*pbn); size--, pbn++);
  return size ? true : false;
}
#endif

/* Copies a uint8_t array to a ECC_BigInt_t representation. */
static int bin2BigInt(ECC_BigInt_t   bn,
                      const uint8_t* bin,
                      int            size)
{
  int i;
  EFM_ASSERT(size<=(int)sizeof(ECC_BigInt_t));
  memset(bn, 0, sizeof(ECC_BigInt_t));

  for(i=0; i<size; i++)
    bn[(size-i-1)/sizeof(uint32_t)] |=
      bin[i] << 8*((size-1-i) % sizeof(uint32_t));

  return 0;
}

/* Copies a uint8_t array bit by bit to a ECC_BigInt_t representation. */
static int bin2BigIntBits(ECC_BigInt_t   bn,
                          const uint8_t* bin,
                          int            size)
{
  int i;
  uint8_t mask;
  EFM_ASSERT(size<=(int)sizeof(ECC_BigInt_t) * 8);
  memset(bn, 0, sizeof(ECC_BigInt_t));

  for(i=0; i<size; i++) {
    mask = bin[i/8] & (1 << (7 - (i % 8)));
    if(mask != 0) {
      bn[(size - i - 1) / (sizeof(uint32_t) * 8)] |= 1 << ((size - i - 1) % (sizeof(uint32_t) * 8));
    }
  }

  return 0;
}

/* Returns true if a is larger than b. */
static bool bigIntLargerThan(const uint32_t* a,
                             int             aSize,
                             const uint32_t* b,
                             int             bSize)
{
  EFM_ASSERT(!(aSize%(int)sizeof(uint32_t)));
  EFM_ASSERT(!(bSize%(int)sizeof(uint32_t)));

  aSize = (aSize/sizeof(uint32_t)) - 1;
  bSize = (bSize/sizeof(uint32_t)) - 1;

  while (aSize || bSize)
  {
    if (aSize>bSize)
      if (a[aSize]) return true;
      else aSize--;
    else
      if (bSize>aSize)
        if (b[bSize]) return false;
        else bSize--;
      else
        /* aSize==bSize */
        if (a[aSize] > b[bSize]) return true;
        else
          if (a[aSize] < b[bSize]) return false;
          else
          {
            /* a[aSize]==b[bSize] */
            aSize--;
            bSize--;
          }
  }
  return false;
}

/* Returns true if a is equal to b. */
static bool bigIntCompare(uint32_t* a,
                          int       aSize,
                          uint32_t* b,
                          int       bSize)
{
  EFM_ASSERT(!(aSize%4));
  EFM_ASSERT(!(bSize%4));

  aSize = (aSize/sizeof(uint32_t)) - 1;
  bSize = (bSize/sizeof(uint32_t)) - 1;

  while (aSize || bSize)
  {
    if (aSize>bSize)
      if (a[aSize]) return false;
      else aSize--;
    else
      if (bSize>aSize)
        if (b[bSize]) return false;
        else bSize--;
      else
        /* aSize==bSize */
        if (a[aSize] != b[bSize]) return false;
        else
        {
          aSize--;
          bSize--;
        }
  }
  return true;
}

/* Returns true if the value of the DDATA0 register is equal to zero. */
__STATIC_INLINE bool CRYPTO_DData0_IsZero(CRYPTO_TypeDef *crypto,
                                          uint32_t* statusReg)
{
  CRYPTO_EXECUTE_3(crypto,
                   CRYPTO_CMD_INSTR_CCLR,
                   CRYPTO_CMD_INSTR_DEC,  /* Decrement by one which will set
                                             carry bit if DDATA0 is zero. */
                   CRYPTO_CMD_INSTR_INC   /* Increment in order to restore
                                             original value. */
                   );

  *statusReg = crypto->DSTATUS;

  return (*statusReg & CRYPTO_DSTATUS_CARRY) == CRYPTO_DSTATUS_CARRY;
}

/***************************************************************************//**
 * @brief
 *   Add two ECC points in GF(p) in projective coordinates
 *
 * @details
 *  This function implements mixed point addition in GF(p) of a point @ref P1
 *  in projective coordinates and a second point @ref P2 in affine coordinates.
 *  The result is stored in @ref R.
 *  See 
 *  https://en.wikibooks.org/wiki/Cryptography/Prime_Curve/Jacobian_Coordinates#Point_Addition_.2812M_.2B_4S.29
 *
 *  note: with this implementation it's possible to have the same memory for P1 and R.
 *
 * @param[in]  P1
 *   The point in projective coordinates
 *
 * @param[in]  P2
 *   The point in affine coordinates
 *
 * @param[out] R
 *   The destination of the result
 ******************************************************************************/
void ECC_AddPrimeMixedProjectiveAffine
(
 CRYPTO_TypeDef*            crypto,
 ECC_Projective_Point_t*    P1,
 const ECC_Point_t*         P2,
 ECC_Projective_Point_t*    R
 )
{
  ECC_BigInt_t D;

  /*
    
  Goals: A = P2->X*P1->Z²
  B = P2->Y*P1->Z³
  
  Write Operations:
  
  R1 = P1->Z
  R3 = P2->X
  R4 = P2->Y
  
  Instructions to be executed:
  
  1. R2 = R1 = P1->Z
  2. Select R1, R2
  2. R0 = R1 * R2 = P1->Z²
  3. R1 = R0 = P1->Z²
  4. Select R1, R3
  5. R0 = R1 * R3 = P2->X * P1->Z²
  6. R3 = R0 = P2->X * P1->Z²
  7. Select R1, R2
  8. R0 = R1 * R2 = P1->Z³
  9. R1 = R0 = P1->Z³
  10.Select R1, R4
  11.R0 = R1 * R4 = P2->Y * P1->Z³
  
  Read Operations:     
  
  B = R0 = P2->Y*P1->Z³
  A = R3 = P2->X*P1->Z²

  STEP 1:
  */
  
  CRYPTO_DDataWrite(&crypto->DDATA1, P1->Z);
  CRYPTO_DDataWrite(&crypto->DDATA3, P2->X);
  CRYPTO_DDataWrite(&crypto->DDATA4, P2->Y);
  
  CRYPTO_EXECUTE_12(crypto,
                    CRYPTO_CMD_INSTR_DDATA1TODDATA2,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                    CRYPTO_CMD_INSTR_MMUL
                    );

  /*
    
  Goals: C  = A - P1->X
  D  = B - P1->Y
  R->Z = P1->Z * C
  
  Write Operations:
  
  R0 = B         B is already in R0
  R1 = P1->X
  R2 = P1->Y
  R3 = A         A is already in R3
  R4 = P1->Z
  
  Instructions to be executed:
  
  1. Select R0, R2
  2. R0 = R0 - R2 = B - P1->Y = D
  3. R2 = R0 = D
  4. Select R3, R1
  5. R0 = R3 - R1 = A - P1->X = C
  6. R1 = R0 = C
  7. Select R1, R4
  8. R0 = R1 * R4 = P1->Z * C = R->Z
  
  Read Operations:     
  
  R->Z = R0 = P1->Z * C
  C  = R1 = A - P1->X
  D  = R2 = B - P1->Y
  
  STEP 2:
  */
  
  CRYPTO_DDataWrite(&crypto->DDATA1, P1->X);
  CRYPTO_DDataWrite(&crypto->DDATA2, P1->Y);
  CRYPTO_DDataWrite(&crypto->DDATA4, P1->Z);

  CRYPTO_EXECUTE_8(crypto,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA2,
                   CRYPTO_CMD_INSTR_MSUB,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA2,
                   CRYPTO_CMD_INSTR_SELDDATA3DDATA1,
                   CRYPTO_CMD_INSTR_MSUB,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                   CRYPTO_CMD_INSTR_MMUL
                   );
  
  CRYPTO_DDataRead(&crypto->DDATA0, R->Z);
  CRYPTO_DDataRead(&crypto->DDATA2, D);

  /*
    
  Goals: X1C2  = P1->X * C²
  C3    = C³
  D2    = D²
  
  Write Operations:
  
  R1 = C         C is already in R1
  R2 = D         D is already in R2
  R3 = P1->X
  
  R4 = C
  
  Instructions to be executed:
  
  1. Select R1, R4
  2. R0 = R1 * R4 = C²
  3. R1 = R0 = C²
  4. R0 = R1 * R4 = C³
  5. R4 = R0 = C³
  6. Select R1, R3
  7. R0 = R1 * R3 = P1->X * C^²
  8. R3 = R0 = P1->X * C²
  9. R1 = R2 = D
  10. Select R1, R1
  11. R0 = R1 * R1 = D²
  
  Read Operations:     
  
  D2   = R0 = D²
  X1C2 = R3 = P1->X * C²
  C3   = R4 = C³
  
  STEP 3:
  */

  CRYPTO_DDataWrite(&crypto->DDATA3, P1->X);
  CRYPTO_EXECUTE_12(crypto,
                    CRYPTO_CMD_INSTR_DDATA1TODDATA4,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA4,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                    CRYPTO_CMD_INSTR_DDATA2TODDATA1,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                    CRYPTO_CMD_INSTR_MMUL
                    );

  /*       
           
  Goals: R->X   = D2 - (C3 + 2 * X1C2) = D2 - C3 - X1C2- X1C2
  Y1C3 = P1->Y * C3
  
  Write Operations:
  
  R0 = D2        D2 is already in R0
  R1 = P1->Y
  R3 = X1C2      X1C2 is already in R3
  R4 = C3        C3 is already in R4
  
  Instructions to be executed:
  
  1. Select R0, R4
  2. R0 = R0 - R4 = D2 - C3
  3. Select R0, R3
  4. R0 = R0 - R3 = D2 - C3 - X1C2
  5. R0 = R0 - R3 = D2 - C3 - X1C2 - X1C2 = R->X
  6. R2 = R0 = R->X
  7. Select R1, R4
  8. R0 = R1 * R4 = P1->Y * C3 = Y1C3
  
  Read Operations:     
  
  Y1C3 = R0 = P1->Y * C³
  R->X   = R2 = D2 - (C3 + 2 * X1C2)
  
  STEP 4:
  */

  CRYPTO_DDataWrite(&crypto->DDATA1, P1->Y);
  
  CRYPTO_EXECUTE_8(crypto,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA4,
                   CRYPTO_CMD_INSTR_MSUB,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA3,
                   CRYPTO_CMD_INSTR_MSUB,
                   CRYPTO_CMD_INSTR_MSUB,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA2,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                   CRYPTO_CMD_INSTR_MMUL
                   );

  CRYPTO_DDataRead(&crypto->DDATA2, R->X);

  /*       
           
  Goal: R->Y = D * (X1C2 - R->X) - Y1C3
  
  Write Operations:
  
  R1 = D 
  R2 = R->X        R->X is already in R2
  R3 = X1C2      X1C2 is already in R3
  R4 = Y1C3      
  
  Instructions to be executed:
  
  1. Select R3, R2
  2. R0 = R3 - R2 = X1C2 - R->X
  3. R2 = R0 = X1C2 - R->X
  4. Select R1, R2
  5. R0 = R1 * R2 = D *(X1C2 - R->X)
  6. Select R0, R4
  7. R0 = R0 - R4
  
  Read Operations:     
  
  R->Y= R0 = D * (X1C2 - R->X) - Y1C3
  
  STEP 5:
  */  

  CRYPTO_DDataWrite(&crypto->DDATA1, D);
  CRYPTO_EXECUTE_8(crypto,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA4,
                   CRYPTO_CMD_INSTR_SELDDATA3DDATA2,
                   CRYPTO_CMD_INSTR_MSUB,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA2,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                   CRYPTO_CMD_INSTR_MMUL,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA4,
                   CRYPTO_CMD_INSTR_MSUB
                   );

  CRYPTO_DDataRead(&crypto->DDATA0, R->Y);

} /* point_add_prime_projective */

/***************************************************************************//**
 * @brief
 *   Double an ECC point in GF(p) in projective coordinates.
 *
 * @details
 *  This function implements point doubling in GF(p) in projective coordinates
 *  on the curve specified by @ref curveId.
 *  The point @ref P1 is doubled and the result is stored in @ref R.
 *
 *  @param[in]  P1       The point to double
 *  @param[out] R        The destination of the result
 ******************************************************************************/
void ECC_PointDoublePrimeProjective
(
 CRYPTO_TypeDef*                crypto,
 const ECC_Projective_Point_t*  P1,
 ECC_Projective_Point_t*        R
 )
{
  ECC_BigInt_t A;
  ECC_BigInt_t B;
  ECC_BigInt_t _2A;  /* Represents 2A */

  /*       
           
  Goals: B    = 8 * Y1^4
  Y1Y1 = Y1²
  
  Write Operations:
  
  R1 = Y1
  
  Instructions to be executed:
  
  1. R2 = R1 = Y1
  2. Select R1, R2
  3. R0 = R1 * R2 = Y1² = Y1Y1
  4. R1 = R0 = Y1²
  5. R2 = R0 = Y1²
  6. R0 = R1 * R2 = Y1^4
  7. Select R0, R0
  8. R0 = R0 + R0 = 2 * Y1^4
  9. R0 = R0 + R0 = 4 * Y1^4
  10 R0 = R0 + R0 = 8 * Y1^4
  
  Read Operations:     
  
  B    = R0 = 8 * Y1^4
  Y1Y1 = R1 = Y1²
  
  */
  
  CRYPTO_DDataWrite(&crypto->DDATA1, P1->Y);

  CRYPTO_EXECUTE_10(crypto,
                    CRYPTO_CMD_INSTR_DDATA1TODDATA2,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA2,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_SELDDATA0DDATA0,
                    CRYPTO_CMD_INSTR_MADD,
                    CRYPTO_CMD_INSTR_MADD,
                    CRYPTO_CMD_INSTR_MADD
                    );
  
  CRYPTO_DDataRead(&crypto->DDATA0, B);

  /*       
           
  Goals: A    = 4P1->X * Y1Y1 (According to http://www.dkrypt.com/home/ecc it must be 4P1->X+Y1Y1 which is not right.For details 
  see Chapter 3 (Section 3.2.3) of the book "Introduction to Identity-Based Encryption"
  by Martin Luther)
  _2A  = 2A
  
  Write Operations:
  
  R0 = P1->X
  R1 = Y1Y1       R1 already contains Y1Y1
  
  Instructions to be executed:
  
  1.  Select R0, R0
  2.  R0 = R0 + R0 = 2P1->X
  3.  R0 = R0 + R0 = 4P1->X
  4.  R3 = R0 = 4P1->X
  5.  Select R1, R3
  6.  R0 = R1 * R3 = 4P1->X * Y1Y1 = A
  7.  R3 = R0
  8.  Select R0, R3
  9.  R0 = R0 + R3 = 2A = _2A
  
  Read Operations:     
  
  A    = R3 = 4P1->X + Y1Y1
  _2A  = R0 = 2A
  
  */
  
  CRYPTO_DDataWrite(&crypto->DDATA0, P1->X);
  
  CRYPTO_EXECUTE_9(crypto,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA0,
                   CRYPTO_CMD_INSTR_MADD,
                   CRYPTO_CMD_INSTR_MADD,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                   CRYPTO_CMD_INSTR_MMUL,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA3,
                   CRYPTO_CMD_INSTR_MADD
                   );
  
  CRYPTO_DDataRead(&crypto->DDATA3, A);
  CRYPTO_DDataRead(&crypto->DDATA0, _2A);

  /*       
       
  Goals: Z1Z1 = P1->Z²
  
  Write Operations:
  
  R1 = P1->Z
  
  Instructions to be executed:
  
  1. R2 = R1 = P1->Z
  2. Select R1, R2
  3. R0 = R1 * R2 = P1->Z^² = Z1Z1 
  4. R3 = R0 = Z1Z1
  
  Read Operations:     
  
  Z1Z1 = R0 = P1->Z²
  
  */
  
  CRYPTO_DDataWrite(&crypto->DDATA1, P1->Z);
  
  CRYPTO_EXECUTE_4(crypto,
                   CRYPTO_CMD_INSTR_DDATA1TODDATA2,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                   CRYPTO_CMD_INSTR_MMUL,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA3
                   );
  
  /*       
           
  Goal: C = 3(P1->X - Z1Z1)(P1->X + Z1Z1)
  
  Write Operations:
  
  R2 = P1->X
  R3 = Z1Z1    Z1Z1 is already in R3   
  
  Instructions to be executed:
  
  1.  Select R2, R3
  2.  R0 = R2 + R3 = P1->X + Z1Z1
  3.  R1 = R0 = P1->X + Z1Z1
  4.  R0 = R2 - R3 = P1->X - Z1Z1
  5.  R2 = R0 = P1->X - Z1Z1
  6.  Select R1, R2
  7.  R0 = R1 * R2 = (P1->X + Z1Z1)(P1->X - Z1Z1)
  8.  R1 = R0 = (P1->X + Z1Z1)(P1->X - Z1Z1)
  9.  Select R0, R1
  10. R0 = R0 + R1 = 2(P1->X + Z1Z1)(P1->X - Z1Z1)
  11. R0 = R0 + R1 = 3(P1->X + Z1Z1)(P1->X - Z1Z1) = C
  12. R1 = R0 = C 
  
  Read Operations:     
  
  C = R1 = 3(P1->X - Z1Z1)(P1->X + Z1Z1)
  
  */
  
  CRYPTO_DDataWrite(&crypto->DDATA2, P1->X);
  
  CRYPTO_EXECUTE_12(crypto,
                    CRYPTO_CMD_INSTR_SELDDATA2DDATA3,
                    CRYPTO_CMD_INSTR_MADD,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                    CRYPTO_CMD_INSTR_MSUB,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA2,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                    CRYPTO_CMD_INSTR_SELDDATA0DDATA1,
                    CRYPTO_CMD_INSTR_MADD,
                    CRYPTO_CMD_INSTR_MADD,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA1
                    );
  
  /*       
           
  Goals: R->X = C² - _2A
  D = C(A - R->X)
  
  Write Operations:
  
  R1 = C          R1 already contains C
  R2 = _2A
  R3 = A
  R4 = C       
  
  Instructions to be executed:
  
  1.  R4 = R1 = C
  2.  Select R1, R4
  3.  R0 = R1 * R4 = C²
  4.  Select R0, R2
  5.  R0 = R0 - R2 = C² - _2A = R->X
  6.  R4 = R0 = R->X
  7.  Select R3, R4
  8.  R0 = R3 - R4 = A - R->X
  9.  R2 = R0 = A - R->X
  10  Select R1, R2
  11. R0 = R1 * R2 = C(A - R->X) = D
  
  Read Operations:     
  
  D  = R0 = C(A - R->X)
  R->X = R4 = C² - _2A
  
  */

  CRYPTO_DDataWrite(&crypto->DDATA2, _2A);
  CRYPTO_DDataWrite(&crypto->DDATA3, A);
  
  CRYPTO_EXECUTE_11(crypto,
                    CRYPTO_CMD_INSTR_DDATA1TODDATA4,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                    CRYPTO_CMD_INSTR_MMUL,
                    CRYPTO_CMD_INSTR_SELDDATA0DDATA2,
                    CRYPTO_CMD_INSTR_MSUB,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA4,
                    CRYPTO_CMD_INSTR_SELDDATA3DDATA4,
                    CRYPTO_CMD_INSTR_MSUB,
                    CRYPTO_CMD_INSTR_DDATA0TODDATA2,
                    CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                    CRYPTO_CMD_INSTR_MMUL
                    );
  
  CRYPTO_DDataRead(&crypto->DDATA4, R->X);
  
  /*       
           
  Goals: R->Y = D - B
  R->Z = 2 * Y1 * P1->Z
  
  Write Operations:
  
  R0 = D         R0 already contains D
  R1 = Y1          
  R2 = P1->Z
  R3 = B
  
  Instructions to be executed:
  
  1.  Select R0, R3
  2.  R0 = R0 - R3 = D - B = R->Y
  3.  R3 = R0 = R->Y
  4.  Select R1, R2
  5.  R0 = R1 * R2 = Y1 * P1->Z
  6.  Select R0, R0
  7.  R0 = R0 + R0 = 2 * Y1 * P1->Z = R->Z
  
  Read Operations:     
  
  R->Z = R0 = 2*Y1*P1->Z
  R->Y = R3 = D - B
  
  */
  
  CRYPTO_DDataWrite(&crypto->DDATA1, P1->Y);
  CRYPTO_DDataWrite(&crypto->DDATA2, P1->Z);
  CRYPTO_DDataWrite(&crypto->DDATA3, B);
  
  CRYPTO_EXECUTE_7(crypto,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA3,
                   CRYPTO_CMD_INSTR_MSUB,
                   CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                   CRYPTO_CMD_INSTR_MMUL,
                   CRYPTO_CMD_INSTR_SELDDATA0DDATA0,
                   CRYPTO_CMD_INSTR_MADD
                   );
      
  CRYPTO_DDataRead(&crypto->DDATA0, R->Z);
  CRYPTO_DDataRead(&crypto->DDATA3, R->Y);
} /* point_dbl_prime_projective */

/***************************************************************************//**
 * @brief
 *   Modular division
 *
 * @details
 *   This function computes R = X/Y mod(N) . If @p GF2m is true, the modular
 *   division is done in the GF(2m) finite field.
 *
 *  @param[in]  X        Dividend of modular division operation
 *  @param[in]  Y        Divisor of  modular division operation
 *  @param[in]  N        Modulus
 *  @param[in]  GF2m     Perform modulus division in the GF(2m) finite field.
 *  @param[out] R        The destination of the result
 *
 * @return N/A
 ******************************************************************************/
static void ECC_ModularDivision(CRYPTO_TypeDef *crypto,
                                ECC_BigInt_t   X,
                                ECC_BigInt_t   Y,
                                ECC_BigInt_t   N,
                                ECC_BigInt_t   R,
                                bool           GF2m)
{
  uint32_t            D[9];
  uint32_t            statusReg;
  uint8_t             rdata;
  uint8_t             lsb_C;
  uint8_t             lsb_D;
  uint8_t             lsb_U;
  int                 T;
  int                 k;

  /*
  ** OPTIMIZATION:
  ** 
  ** In every loop the D value is read out and written back to DDATA3. If we
  ** could add another DDATA register we could save quite a lot of data moving.

        CRYPTO_EXECUTE_WAIT_1(CRYPTO_CMD_INSTR_DDATA3TODDATA0);
        CRYPTO_DDATA0_260_BITS_READ(D);
        ...
        CRYPTO_DDATA0_260_BITS_WRITE(D);
        CRYPTO_EXECUTE_WAIT_1(CRYPTO_CMD_INSTR_DDATA0TODDATA3);

  ** In every loop the p value is written to DDATA0.  If we
  ** could add another DDATA register we could save quite a lot of data moving.
        CRYPTO_DDataWrite(&crypto->DDATA0, p);
  */

  /************** Initialize and organize data in crypto module **************/

  /*
  ** Register usage:
  **
  ** DDATA0 - holds temporary results and loads 260 bit variables in/out 
  ** DDATA1 - variable referred to as 'C' in the following algorithm
  ** DDATA2 - variable referred to as 'U' in the following algorithm
  ** DDATA3 - variable referred to as 'D' in the following algorithm
  ** DDATA4 - variable referred to as 'W' in the following algorithm
  */

  statusReg = crypto->WAC & (~_CRYPTO_WAC_MODOP_MASK);
  if (GF2m)
  {
    crypto->WAC = statusReg | CRYPTO_WAC_MODOP_BINARY;
  }
  else
  {
    crypto->WAC = statusReg | CRYPTO_WAC_MODOP_REGULAR;
  }
  
  EC_BIGINT_COPY(D, N);             /* D will hold the modulus (n) initially */
  D[8]=0;                           /* Set MSWord of D to 0. */

  CRYPTO_DDataWrite(&crypto->DDATA1, Y);  /* Set C to Y (divisor) initially */
  CRYPTO_DDataWrite(&crypto->DDATA2, X);  /* Set U to X (dividend)initially */

  CRYPTO_DDataWrite(&crypto->DDATA3, N);  /* Set D to modulus p initially   */

  CRYPTO_EXECUTE_3(crypto,
                   CRYPTO_CMD_INSTR_CLR,            /* DDATA0 = 0 */
                   CRYPTO_CMD_INSTR_DDATA0TODDATA4, /* Set W to zero initially*/
                   CRYPTO_CMD_INSTR_DDATA1TODDATA0);/* DDATA0 = C initially */

  T     = 0;
  k     = 1;

  /******************* Run main loop while 'C' is non-zero ********************/

  /* while (C != 1024'd0)  */
  while ( !CRYPTO_DData0_IsZero(crypto, &statusReg) )
  {
    lsb_C = (statusReg & _CRYPTO_DSTATUS_DDATA0LSBS_MASK) >> _CRYPTO_DSTATUS_DDATA0LSBS_SHIFT;
    if ((lsb_C & 0x1) == 0)
    {
      CRYPTO_EXECUTE_3(crypto,
                       CRYPTO_CMD_INSTR_SELDDATA1DDATA1,
                       CRYPTO_CMD_INSTR_SHRA,
                       CRYPTO_CMD_INSTR_DDATA0TODDATA1                                    
                       );
      T = T-1;  
    }
    else
    {
      if (T<0)
      {
        CRYPTO_EXECUTE_6(crypto,
                         CRYPTO_CMD_INSTR_DDATA1TODDATA0,
                         CRYPTO_CMD_INSTR_DDATA3TODDATA1,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                         CRYPTO_CMD_INSTR_DDATA2TODDATA0,
                         CRYPTO_CMD_INSTR_DDATA4TODDATA2,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA4
                         );

        if (false == GF2m)
        {
          CRYPTO_EXECUTE_1(crypto,
                           CRYPTO_CMD_INSTR_DDATA3TODDATA0);
          CRYPTO_DDATA0_260_BITS_READ(crypto, D);
        }

        T = -T;
      } 

      k = 1;
      
      if (false == GF2m)
      {
        CRYPTO_EXECUTE_2(crypto,
                         CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                         CRYPTO_CMD_INSTR_ADD                                 
                         );
        
        rdata = CRYPTO_DData0_4LSBitsRead(crypto);

        if((rdata & 0x3) != 0x0)
          k = -1;
        else
          T = T-1;
      }
      else
        T = T-1;
      
      if (false == GF2m)
      {                     
        /*  R1 = $signed(C)>>>1  */

        CRYPTO_EXECUTE_1(crypto,
                         CRYPTO_CMD_INSTR_DDATA1TODDATA0);   // to get the lsb of C

        lsb_C = CRYPTO_DData0_4LSBitsRead(crypto);
        CRYPTO_EXECUTE_4(crypto,
                         CRYPTO_CMD_INSTR_SELDDATA1DDATA1,
                         CRYPTO_CMD_INSTR_SHRA,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                         CRYPTO_CMD_INSTR_DDATA3TODDATA0    // to get the msb of R3
                         );

        /*  R3 = $signed(D)>>>1  */
        
        lsb_D = CRYPTO_DData0_4LSBitsRead(crypto);

        CRYPTO_EXECUTE_2(crypto,
                         CRYPTO_CMD_INSTR_SELDDATA3DDATA3,
                         CRYPTO_CMD_INSTR_SHRA
                         );

        if(k == 1)
        {
          if (((lsb_C & 0x1)==0x1) && ((lsb_D & 0x1)==0x1))
          {
            /*  C = R1+R3+1  */
            CRYPTO_EXECUTE_4(crypto,
                             CRYPTO_CMD_INSTR_SELDDATA0DDATA1,
                             CRYPTO_CMD_INSTR_CSET,
                             CRYPTO_CMD_INSTR_ADDC,
                             CRYPTO_CMD_INSTR_DDATA0TODDATA1
                             );
          }
          else
          {   
            /*  C = R1+R3  */
            CRYPTO_EXECUTE_3(crypto,
                             CRYPTO_CMD_INSTR_SELDDATA0DDATA1,
                             CRYPTO_CMD_INSTR_ADD,
                             CRYPTO_CMD_INSTR_DDATA0TODDATA1
                             );
          }
          /*  U = mod(R2+R4,n)  */
          CRYPTO_EXECUTE_3(crypto,
                           CRYPTO_CMD_INSTR_SELDDATA2DDATA4,
                           CRYPTO_CMD_INSTR_MADD,
                           CRYPTO_CMD_INSTR_DDATA0TODDATA2
                           );
          
        }
        else if (k == -1)
        {
          if (((lsb_C & 0x1)==0x0) && ((lsb_D & 0x1)==0x1))     
            /*  C = R1-R3-1  */
          {
            CRYPTO_EXECUTE_5(crypto,
                             CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                             CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                             CRYPTO_CMD_INSTR_CSET,
                             CRYPTO_CMD_INSTR_SUBC,
                             CRYPTO_CMD_INSTR_DDATA0TODDATA1
                             );
          }                  
          else
          {
            /*  C = R1+R3  */
            CRYPTO_EXECUTE_4(crypto,
                             CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                             CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                             CRYPTO_CMD_INSTR_SUB,
                             CRYPTO_CMD_INSTR_DDATA0TODDATA1
                             );
          }                
          /*  U = mod(R2-R4,p)  */
          
          CRYPTO_EXECUTE_3(crypto,
                           CRYPTO_CMD_INSTR_SELDDATA2DDATA4,
                           CRYPTO_CMD_INSTR_MSUB,
                           CRYPTO_CMD_INSTR_DDATA0TODDATA2
                           );

          CRYPTO_DDATA0_260_BITS_WRITE(CRYPTO, D);
          CRYPTO_EXECUTE_1(crypto,
                           CRYPTO_CMD_INSTR_DDATA0TODDATA3);

        } // if (k == -1)
                   
      } // if (false == GF2m)
                
      else         // For binary fields
      {
        CRYPTO_EXECUTE_8(crypto,
                         CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                         CRYPTO_CMD_INSTR_XOR,
                         CRYPTO_CMD_INSTR_SELDDATA0DDATA0,
                         CRYPTO_CMD_INSTR_SHR,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                         CRYPTO_CMD_INSTR_SELDDATA2DDATA4,
                         CRYPTO_CMD_INSTR_XOR,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA2                                    
                         );
        
      } // else: !if(false == GF2m)
                
                
    } // else: !if((C[31:0] & 0x1) == 0x0)
           
           
    CRYPTO_EXECUTE_1(crypto,
                     CRYPTO_CMD_INSTR_DDATA2TODDATA0);
    
    lsb_U = CRYPTO_DData0_4LSBitsRead(crypto);

    /* if ((U[31:0] & 0x1) == 0x1) */
    if((lsb_U & 0x1) == 0x1)
    {
      if (false == GF2m)
      {
        CRYPTO_EXECUTE_3(crypto,
                         CRYPTO_CMD_INSTR_SELDDATA2DDATA2,
                         CRYPTO_CMD_INSTR_SHRA,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA2
                         );

        CRYPTO_DDataWrite(&crypto->DDATA0, N);
        CRYPTO_EXECUTE_6(crypto,
                         CRYPTO_CMD_INSTR_SELDDATA0DDATA0,
                         CRYPTO_CMD_INSTR_SHR,
                         CRYPTO_CMD_INSTR_SELDDATA0DDATA2,
                         CRYPTO_CMD_INSTR_CSET,
                         CRYPTO_CMD_INSTR_ADDC,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA2
                         );
      } 
      else
      {
        CRYPTO_DDataWrite(&crypto->DDATA0, N);
        CRYPTO_EXECUTE_5(crypto,
                         CRYPTO_CMD_INSTR_SELDDATA0DDATA2,
                         CRYPTO_CMD_INSTR_XOR,
                         CRYPTO_CMD_INSTR_SELDDATA0DDATA0,
                         CRYPTO_CMD_INSTR_SHR,
                         CRYPTO_CMD_INSTR_DDATA0TODDATA2
                         );
        
      } // else: !if(false == GF2m)
      
    }
    else
    {
      CRYPTO_EXECUTE_3(crypto,
                       CRYPTO_CMD_INSTR_SELDDATA2DDATA2,
                       CRYPTO_CMD_INSTR_SHRA,
                       CRYPTO_CMD_INSTR_DDATA0TODDATA2                                   
                       );
    } 
    
    CRYPTO_EXECUTE_1(crypto,
                     CRYPTO_CMD_INSTR_DDATA1TODDATA0);

  } /* End of main loop:  while (C != 0)  */
  
  /* if (D == 1): */
  /* Decrement D by 1 and test if zero. */
  CRYPTO_EXECUTE_2(crypto,
                   CRYPTO_CMD_INSTR_DDATA3TODDATA0,
                   CRYPTO_CMD_INSTR_DEC
                   );
  if (CRYPTO_DData0_IsZero(crypto, &statusReg))
  {
    CRYPTO_DDataRead(&crypto->DDATA4, R);
  }
  else
  {
    if (false == GF2m)
    {
      CRYPTO_DDataWrite(&crypto->DDATA0, N);
      CRYPTO_EXECUTE_2(crypto,
                       CRYPTO_CMD_INSTR_SELDDATA0DDATA4,
                       CRYPTO_CMD_INSTR_SUB
                       );
    }
    else
    {
      CRYPTO_DDataWrite(&crypto->DDATA0, N);
      CRYPTO_EXECUTE_2(crypto,
                       CRYPTO_CMD_INSTR_SELDDATA0DDATA4,
                       CRYPTO_CMD_INSTR_XOR
                       );
    }
    
    CRYPTO_DDataRead(&crypto->DDATA0, R);
  }

  return;
} /* ECC_ModularDivision  */

/* Returns true if bigint is equal to the given 32bit integer. */
static bool bigIntEqual32bitVal(uint32_t *bn, int size, uint32_t val)
{
  EFM_ASSERT(size>=4);
  EFM_ASSERT(!(size%4));

  if ( *(uint32_t*)bn != val)
    return 0;

  for (bn++, size-=4; size>0 && (0==*bn); size-=4, bn++);
  return (size>0 ? false : true);
}

/***************************************************************************//**
 * @brief
 *   Perform ECC point multiplication.
 *
 * @details
 *   Perform ECC point multiplication using the simple Double-And-Add algorithm:
 *     R := 0
 *     for i from m to 0 do
 *       R := 2R (using point doubling)
 *       if di = 1 then
 *         R := R + P (using point addition)
 *     Return R
 *
 * @param[in]  P
 *   The point to multiply. Has to be affine!
 *
 * @param[in]  n
 *   Multiplicand - scalar to multiply by
 *
 * @param[out] R
 *   The destination of n*P
 *
 * @param[in]  curveId
 *   The elliptic curve identifier.
 *
 * @param[in]  asynchCallback
 *   If non-NULL, this function will operate in asynchronous mode by starting
 *   the ECC operation and return immediately (non-blocking API). When the ECC
 *   operation has completed, the ascynchCallback function will be called.
 *   If NULL, this function will operate in synchronous mode, and block until
 *   the ECC operation has completed.
 *
 * @param[in]  asynchCallbackArgument
 *   User defined parameter to be sent to callback function.
 *
 * @return Error code.
 ******************************************************************************/
Ecode_t ECC_PointMul
(
 CRYPTO_TypeDef*           crypto,
 ECC_CurveId_t             curveId,
 const ECC_Point_t*        P,
 ECC_BigInt_t              n,
 ECC_Projective_Point_t*   R
 )
{
  int                  i;
  int                  mulStart;
  ECC_BigInt_t         temp;
  /////////////////////////// Initializations ////////////////////////////
  /* temp = 1 */
  memset (temp, 0, sizeof(temp));
  temp[0] = 1;

  /* R := 0 */
  memset (R->X, 0, sizeof(R->X));
  memset (R->Y, 0, sizeof(R->Y));
  memset (R->Z, 0, sizeof(R->Z));
  /* Set modulus of CRYPTO module corresponding to specified curve id. */
  CRYPTO_ModulusSet(crypto, eccPrimeModIdGet(curveId));
  CRYPTO_MulOperandWidthSet(crypto, cryptoMulOperandModulusBits);
#ifdef INCLUDE_ECC_P256
  if (eccCurveId_X962_P256 == curveId)
    CRYPTO_ResultWidthSet(crypto, cryptoResult260Bits);
  else
#endif
    CRYPTO_ResultWidthSet(crypto, cryptoResult256Bits);
  ECC_CLEAR_CRYPTO_CTRL;

#ifdef USE_DUMMY_ADD
  mulStart = ECC_Curve_Params[curveId].bitSize-1;
  if (-1 == mulStart)
    return ECODE_BTL_ECC_INVALID_CURVE_ID;
#else
  /* Simulation speed optimization: Start at first 1 in multiplicand.
     SHOULD NOT BE DONE IN C library because it will make the implementation
     vulnerable to timing attacks. */
  for (mulStart = ECC_Curve_Params[curveId].bitSize-1;
       (mulStart > 0) && (!BIGINT_BIT_IS_ONE(n, mulStart));
       mulStart--);
#endif

  for (i=mulStart; i>=0; i--)
  {
    ECC_PointDoublePrimeProjective(crypto, R, R);

    if (BIGINT_BIT_IS_ONE(n, i))
    {
      if (!(bigIntEqual32bitVal(R->X, sizeof(R->X), 0) &&
            bigIntEqual32bitVal(R->Y, sizeof(R->Y), 0) &&
            bigIntEqual32bitVal(R->Z, sizeof(R->Z), 0) ) )
      {
        ECC_AddPrimeMixedProjectiveAffine(crypto, R, P, R);
      }
      else
      {
        EC_BIGINT_COPY(R->X, P->X);
        EC_BIGINT_COPY(R->Y, P->Y);
        EC_BIGINT_COPY(R->Z, temp);

#ifdef USE_DUMMY_ADD
        /* Dummy addition in order to protect against side channel attacks. */
        ECC_AddPrimeMixedProjectiveAffine(crypto, R, P, &DummyResult);
#endif
      }
    }
#ifdef USE_DUMMY_ADD
    else
    {
      /* Dummy addition in order to protect against side channel attacks. */
      ECC_AddPrimeMixedProjectiveAffine(crypto, R, P, &DummyResult);
    } 
#endif

  } /* for (int i=ECC_Curve_Params[curveId].bitSize-1; i>=0; i--) */

  return ECODE_OK;
} /* ECC_PointMul */

/***************************************************************************//**
 * @brief
 *   Convert projective coordinates to affine coordinates.
 *   note: P and R can point to the same memory
 *
 * @details
 * TBW
 *
 *  @param[in]  P        The X/Y components of the point with projective coordinates to convert.
 *  @param[in]  Z        The Z component of the point with projective coordinates to convert.
 *  @param[out] R        The destination of the result
 *  @param[in]  curveId  The elliptic curve identifier.
 ******************************************************************************/
Ecode_t ECC_ProjectiveToAffine(CRYPTO_TypeDef            *crypto,
                               ECC_CurveId_t             curveId,
                               ECC_Projective_Point_t*   P,
                               ECC_Point_t*              R)
                               
{
  ECC_BigInt_t    temp;
  ECC_BigInt_t    Z_inv;
  ECC_BigInt_t    modulus;
  bool            primeCurve;

  /* Set modulus of CRYPTO module corresponding to specified curve id. */  
  CRYPTO_ModulusSet(crypto, eccPrimeModIdGet(curveId));
  CRYPTO_MulOperandWidthSet(crypto, cryptoMulOperandModulusBits);
#ifdef INCLUDE_ECC_P256
  if (eccCurveId_X962_P256 == curveId)
    CRYPTO_ResultWidthSet(crypto, cryptoResult260Bits);
  else
#endif
    CRYPTO_ResultWidthSet(crypto, cryptoResult256Bits);
  ECC_CLEAR_CRYPTO_CTRL;

  primeCurve = eccPrimeCurve(curveId);

  /* mod_div_projective(1, P->Z, modType, Z_inv); */
  memset(temp, 0, sizeof(temp));
  temp[0]=1;
  eccPrimeGet(curveId, modulus);
  ECC_ModularDivision(crypto, temp, P->Z, modulus, Z_inv, !primeCurve);

  if (primeCurve)
  {
    /*
             
    Goals:
    R->X = P->X * Z_inv ^2
    R->Y = P->Y * Z_inv ^3
    
    Write Operations:
    
    R1 = Z_inv
    R3 = P->X
    R4 = P->Y
    
    Instructions to be executed:
    
    1.  R2 = R1 = Z_inv
    2.  Select R1, R2
    3.  R0 = R1 * R2 = Z_inv^2
    4.  R1 = R0 = Z_inv^2
    5.  Select R1, R3
    6.  R0 = R1 * R3 = P->X * Z_inv^2 = R->X
    7.  R3 = R0
    8.  Select R1, R2
    9.  R0 = R1 * R2 = Z_inv^3
    10. R1 = R0 = Z_inv^3
    11. Select R1, R4
    12. R0 = R1 * R4 = P->Y * Z_inv^3 = R->Y
    
    Read Operations:     
    
    R->Y = R0 = P->Y * P->Z_inv^3
    R->X = R3 = P->X * P->Z_inv^2
            
    */
    
    CRYPTO_DDataWrite(&crypto->DDATA1, Z_inv);
    CRYPTO_DDataWrite(&crypto->DDATA3, P->X);
    CRYPTO_DDataWrite(&crypto->DDATA4, P->Y);
    
    CRYPTO_EXECUTE_12(crypto,
                      CRYPTO_CMD_INSTR_DDATA1TODDATA2,
                      CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                      CRYPTO_CMD_INSTR_MMUL,
                      CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                      CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                      CRYPTO_CMD_INSTR_MMUL,
                      CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                      CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                      CRYPTO_CMD_INSTR_MMUL,
                      CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                      CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                      CRYPTO_CMD_INSTR_MMUL
                      );

    CRYPTO_DDataRead(&crypto->DDATA0, R->Y);
    CRYPTO_DDataRead(&crypto->DDATA3, R->X);
    
  } /* if (primeCurve) */
  else
  {
    /*       
             
    Goals:
    R->X = P->X * Z_inv
    R->Y = P->Y * Z_inv^2
    
    Write Operations:
    
    R1 = Z_inv
    R3 = P->X
    R4 = P->Y
    
    Instructions to be executed:
    
    1.  R2 = R1 = Z_inv
    2.  Select R1, R3
    3.  R0 = R1 * R3 = P->X * Z_inv = R->X
    4.  R3 = R0 = R->X
    5.  Select R1, R2
    6.  R0 = R1 * R2 = Z_inv^2
    7.  R1 = R0
    8.  Select R1, R4
    9.  R0 = R1 * R4 = P->Y * Z_inv^2 = R->Y
    
    Read Operations:     
            
    R->Y = R0 = P->Y * P->Z_inv^2
    R->X = R3 = P->X * P->Z_inv
    
    */
    
    CRYPTO_DDataWrite(&crypto->DDATA1, Z_inv);
    CRYPTO_DDataWrite(&crypto->DDATA3, P->X);
    CRYPTO_DDataWrite(&crypto->DDATA4, P->Y);
    
    CRYPTO_EXECUTE_9(crypto,
                     CRYPTO_CMD_INSTR_DDATA1TODDATA2,
                     CRYPTO_CMD_INSTR_SELDDATA1DDATA3,
                     CRYPTO_CMD_INSTR_MMUL,
                     CRYPTO_CMD_INSTR_DDATA0TODDATA3,
                     CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                     CRYPTO_CMD_INSTR_MMUL,
                     CRYPTO_CMD_INSTR_DDATA0TODDATA1,
                     CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                     CRYPTO_CMD_INSTR_MMUL
                     );
    
    CRYPTO_DDataRead(&crypto->DDATA0, R->Y);
    CRYPTO_DDataRead(&crypto->DDATA3, R->X);
    
  }  /* if (primeCurve) ... else */
  
  return ECODE_OK;
} /* ECC_ProjectiveoAffine */


/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/


/***************************************************************************//**
 * @brief
 *  Convert big integer from hex string to ECC_BigInt_t format.
 *
 * @details
 *  Convert a large integer from a hexadecimal string representation to a
 *  ECC_BigInt_t representation.
 *
 * @param[out] bigint      Pointer to the location where to store the result.
 * @param[in]  hex         The hex represenation of the large integer to
 *                         convert.
 *
 * @return     Error code. 
 ******************************************************************************/
Ecode_t ECC_HexToBigInt(ECC_BigInt_t bigint, const char* hex)
{
  uint32_t* ret;
  uint64_t l=0;
  int m,i,j,k,c;

  if ((hex == NULL) || (*hex == '\0'))
    return ECODE_BTL_ECC_INVALID_PARAM;

  /* Count number of hex digits. */
  for (i=0; isxdigit((unsigned char) hex[i]); i++)
    ;
  
  /* Check number of hex digits is not bigger than can fit in a bigint. */
  if (i > 2*(int)sizeof(ECC_BigInt_t))
    return ECODE_BTL_ECC_PARAM_OUT_OF_RANGE;
  
  memset(bigint, 0, sizeof(ECC_BigInt_t));
  
  ret = bigint;

  /* i is the number of hex digits; */
  
  j=i; /* least significant 'hex' */
  m=0;
  while (j > 0)
  {
    *ret = 0;

    m=(((int)BIGINT_BYTES_PER_WORD*2) <= j)?((int)BIGINT_BYTES_PER_WORD*2):j;
    l=0;
    for (;;)
    {
      c=hex[j-m];
      if ((c >= '0') && (c <= '9')) k=c-'0';
      else if ((c >= 'a') && (c <= 'f')) k=c-'a'+10;
      else if ((c >= 'A') && (c <= 'F')) k=c-'A'+10;
      else k=0; /* paranoia */
      l=(l<<4)|k;
      
      if (--m <= 0)
      {
        *ret=l;
        break;
      }
    }
    j-=(BIGINT_BYTES_PER_WORD*2);
    ret++;
  }
  return ECODE_OK;
}

/***************************************************************************//**
 * @brief
 *  Convert large integer from ECC_BigInt_t to hex string format.
 *
 * @details
 *  Convert a large integer from a ECC_BigInt_t representation to a
 *  hexadecimal string representation.
 *
 *  @param[out] hex         Buffer where to store the hexadecimal result.
 *  @param[in]  bigint      The ECC_BigInt_t represenation of the large
 *                          integer to convert.
 *
 * @return     Error code. 
 ******************************************************************************/
Ecode_t ECC_BigIntToHex(char* hex, ECC_BigInt_t bigint)
{
  int i, j;
  uint8_t nibble;

  for (i=(sizeof(ECC_BigInt_t)/sizeof(uint32_t))-1; i>=0; i--)
  {
    for (j=(BIGINT_BYTES_PER_WORD*2)-1; j>=0; j--)
    {
      nibble = (bigint[i]>>(j*4))&0xf;
      *hex++ = nibble > 9 ? nibble - 10 + 'A' : nibble + '0';
    }
  }
  /* Null terminate at end of string. */
  *hex = 0;
  return ECODE_OK;
}

/***************************************************************************//**
 * @brief
 *  Convert big integer from byte array to ECC_BigInt_t format.
 *
 * @details
 *  Convert a large integer from a byte array representation to a
 *  ECC_BigInt_t representation.
 *
 * @param[out] bigint      Pointer to the location where to store the result.
 * @param[in]  bytearray   The byte array represenation of the large integer to
 *                         convert.
 *
 * @return     Error code. 
 ******************************************************************************/
Ecode_t ECC_ByteArrayToBigInt(ECC_BigInt_t bigint, const uint8_t* bytearray)
{
  uint8_t* bigint_byte;
  int i;

  if ((bytearray == NULL) || (bigint == NULL))
    return ECODE_BTL_ECC_INVALID_PARAM;
  
  memset(bigint, 0, sizeof(ECC_BigInt_t));
  
  bigint_byte = (uint8_t*)bigint;
  
  for(i = (int)sizeof(ECC_BigInt_t) - 1; i >= 0; i--) {
    bigint_byte[sizeof(ECC_BigInt_t) - i - 1] = bytearray[i];
  }

  return ECODE_OK;
}

/***************************************************************************//**
 * @brief
 *  Convert large integer from ECC_BigInt_t to byte array format.
 *
 * @details
 *  Convert a large integer from a ECC_BigInt_t representation to a
 *  byte array representation. Caution: byte array must be big enough
 *  to contain the result!
 *
 *  @param[out] bytearray   Buffer where to store the resulting byte array.
 *  @param[in]  bigint      The ECC_BigInt_t represenation of the large
 *                          integer to convert.
 *
 * @return     Error code. 
 ******************************************************************************/
Ecode_t ECC_BigIntToByteArray(uint8_t* bytearray, ECC_BigInt_t bigint)
{
  uint8_t* bigint_byte;
  int i;

  if ((bytearray == NULL) || (bigint == NULL))
    return ECODE_BTL_ECC_INVALID_PARAM;
  
  memset(bytearray, 0, sizeof(ECC_BigInt_t));
  
  bigint_byte = (uint8_t*)bigint;
  
  for(i = (int)sizeof(ECC_BigInt_t) - 1; i >= 0; i--) {
    bytearray[sizeof(ECC_BigInt_t) - i - 1] = bigint_byte[i];
  }

  return ECODE_OK;
}

/***************************************************************************//**
 * @brief
 *  Convert integer from uint32_t to ECC_BigInt_t format.
 *
 * @details
 *  Convert a integer from an uint32_t representation to a
 *  ECC_BigInt_t representation.
 *
 * @param[out] bigint      Pointer to the location where to store the result.
 * @param[in]  value       The value to convert.
 *
 * @return     Error code. 
 ******************************************************************************/
Ecode_t ECC_UnsignedIntToBigInt(ECC_BigInt_t bigint, const uint32_t value)
{
  if (bigint == NULL)
  {
    return ECODE_BTL_ECC_INVALID_PARAM;
  }

  bigint[0] = value;
  return ECODE_OK;
}

/***************************************************************************//**
 * @brief
 *   Verify an ECDSA signature.
 *
 * @details
 *   TBW
 *
 * @param[in]  msgDigest
 *   The message digest associated with the signature.
 *
 * @param[in]  msgDigestLen
 *   The length of the message digest.
 *
 * @param[in]  publicKey
 *   Public key of entity that generated signature.
 *
 * @param[out] signature
 *   The signature to verify.
 *
 * @param[in]  curveId
 *   Identifier that specifies which ECC curve to use.
 *
 * @param[in]  asynchCallback
 *   If non-NULL, this function will operate in asynchronous mode by starting
 *   the ECC operation and return immediately (non-blocking API). When the ECC
 *   operation has completed, the ascynchCallback function will be called.
 *   If NULL, this function will operate in synchronous mode, and block until
 *   the ECC operation has completed.
 *
 * @param[in]  asynchCallbackArgument
 *   User defined parameter to be sent to callback function.
 *
 * @return     Error code.
 ******************************************************************************/
bool ECC_ECDSA_VerifySignatureP256
(
 CRYPTO_TypeDef*                     crypto,
 const uint8_t*                      msgDigest,
 int                                 msgDigestLen,
 const ECC_Point_t*                  publicKey,
 ECC_EcdsaSignature_t*               signature
 )
{
  Ecode_t                 status;
  ECC_BigInt_t            w;
  int                     size;

  ECC_Projective_Point_t  P1; //P1.Z is in use as 'temp'.
  ECC_Projective_Point_t  P2; //P2.Z is in use as 'z' in the first part, and P2.Y is in use as 'order'
  
  status = eccOrderGet(eccCurveId_X962_P256, P2.Y);
  if (ECODE_OK != status)
  {
    return false;
  }

  /* Step #1:
     Verify that the signature components 'r' and 's' are integers in the
     range [1,n-1].
  */
  size = ECC_Curve_Params[eccCurveId_X962_P256].size;
  if (size & 0x3)
  {
    size += 4 - (size & 0x3);
  }

  if (bigIntLargerThan(signature->r, sizeof(ECC_BigInt_t), P2.Y, size))
  {
    return false;
  }
  
  if (bigIntLargerThan(signature->s, sizeof(ECC_BigInt_t), P2.Y, size))
  {
    return false;
  }
  size = eccFieldSizeGet(eccCurveId_X962_P256);
  
  /* Crypto module setup. */
  ECC_CLEAR_CRYPTO_CTRL;
  CRYPTO_ModulusSet(crypto, eccOrderModIdGet(eccCurveId_X962_P256));

  /* Step #2:
     z is the 'size' or 'msgDigestLen' leftmost bits of the digest, whichever is
     smallest.
  */
  if (((eccFieldBitSizeGet(eccCurveId_X962_P256) % 8) == 0) || (msgDigestLen < size) )
  {
    bin2BigInt(P2.Z, msgDigest, msgDigestLen<size ? msgDigestLen:size);
  }
  /* Workaround for bug in M* instructions */
  else
  {
    //keep the fieldBitSize leftmost bits of the digest
    bin2BigIntBits(P2.Z,
                   msgDigest,
                   msgDigestLen<size ?
                   msgDigestLen * 8 : eccFieldBitSizeGet(eccCurveId_X962_P256));
    
    // get the bitnumber of the highest bit
    P1.Z[0] = eccFieldBitSizeGet(eccCurveId_X962_P256) - 1;
    
    if(P2.Z[P1.Z[0] / 32] & (1 << (P1.Z[0] % 32))) {
      // If the highest bit is set, there's a very high probability of z >= modulus
      // Since the CRYPTO module doesn't support modulo operations with
      // arguments bigger than the modulus, we solve this by subtracting the
      // modulus if it is actually bigger.
      eccOrderGet(eccCurveId_X962_P256, P1.Z);

      /* Set result width to 256 */
      CRYPTO_ResultWidthSet(crypto, cryptoResult256Bits);

      CRYPTO_DDataWrite(&crypto->DDATA2, P2.Z);
      CRYPTO_DDataWrite(&crypto->DDATA3, P1.Z);
      
      // this sequence equals ( z >= modulus ? z - modulus : z )
      CRYPTO_EXECUTE_5(crypto,
                       CRYPTO_CMD_INSTR_CLR,
                       CRYPTO_CMD_INSTR_SELDDATA2DDATA3,
                       CRYPTO_CMD_INSTR_SUB,
                       CRYPTO_CMD_INSTR_EXECIFCARRY,
                       CRYPTO_CMD_INSTR_DDATA2TODDATA0);
      
      // read back the result
      CRYPTO_DDataRead(&crypto->DDATA0, P2.Z);
    }
  }

  /* Step #3:
     Calculate u1=z/s mod(n) and u2=r/s mod(n)
  */
  memset(P1.Z, 0, sizeof(P1.Z));
  P1.Z[0]=1;
  crypto->WAC &= ~_CRYPTO_WAC_RESULTWIDTH_MASK;
  crypto->WAC |= CRYPTO_WAC_RESULTWIDTH_260BIT;
  ECC_ModularDivision(crypto, P1.Z, signature->s, P2.Y, w, false);

  CRYPTO_DDataWrite(&crypto->DDATA1, w);
  CRYPTO_DDataWrite(&crypto->DDATA4, P2.Z);
  CRYPTO_DDataWrite(&crypto->DDATA3, signature->r);
  CRYPTO_EXECUTE_7(crypto,
                   CRYPTO_CMD_INSTR_CLR,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA4,
                   CRYPTO_CMD_INSTR_MMUL,    /* DDATA0 = w*z = u1 */
                   CRYPTO_CMD_INSTR_DDATA0TODDATA4,
                   CRYPTO_CMD_INSTR_CLR,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA3, 
                   CRYPTO_CMD_INSTR_MMUL);   /* DDATA0 = w*r = u2 */
  CRYPTO_DDataRead(&crypto->DDATA4, P2.Z); //z = u1
  CRYPTO_DDataRead(&crypto->DDATA0, w); //w = u2
  
  /* Step #3:
     Calculate P = u1*G + u2*PulicKey
  */

  /* Multiply the base point  */
  status = ECC_PointMul(crypto,
                        eccCurveId_X962_P256,
                        eccBasePointGet(eccCurveId_X962_P256),
                        P2.Z,
                        &P1);
  
  if (ECODE_OK == status)
  {
    /* Multiply the public key  */
    status = ECC_PointMul(crypto,
                          eccCurveId_X962_P256,
                          publicKey,
                          w,
                          &P2);
  } else return false;  

  /* What follows is what previously was called ecdsaVerifySignature2 */

  /* Want to add P1 and P2, but need Affine conversion to deal with split addition */
  /* P1 = Affine(P1); P2 = Add(P1, P2); P2 = Affine(P2); */
  if(ECODE_OK == status) {
    status = ECC_ProjectiveToAffine(crypto, eccCurveId_X962_P256, &P1, (ECC_Point_t*)&P1);
  } else return false;
  
  if (ECODE_OK == status)
  {
    ECC_AddPrimeMixedProjectiveAffine(crypto, &P2, (ECC_Point_t*)&P1, &P2);
    status = ECC_ProjectiveToAffine(crypto, eccCurveId_X962_P256, &P2, (ECC_Point_t*)&P2);
  } else return false;

  if (ECODE_OK != status)
  {
    return false;
  }

  memset(P1.Z, 0, sizeof(P1.Z));
  P1.Z[0]=1;
  /* Need to take modulus of P.X... */
  CRYPTO_ModulusSet(crypto, eccOrderModIdGet(eccCurveId_X962_P256));
  crypto->WAC |= CRYPTO_WAC_MODOP_REGULAR;
  CRYPTO_DDataWrite(&crypto->DDATA1, P2.X);
  CRYPTO_DDataWrite(&crypto->DDATA2, P1.Z); // = temp = 1
  CRYPTO_EXECUTE_2(crypto,
                   CRYPTO_CMD_INSTR_SELDDATA1DDATA2,
                   CRYPTO_CMD_INSTR_MMUL);
  CRYPTO_DDataRead(&crypto->DDATA0, P2.X);

  /* Step #4:
     The signature is valid if r==P.X.
  */
  if (bigIntCompare(signature->r, sizeof(signature->r), P2.X, sizeof(P2.X)))
  {
    return true;
  }
  else
  {
    return false; // Signature is invalid.
  }
}

/** @} (end addtogroup ECC) */
/** @} (end addtogroup CRYPTOLIB) */

#endif /* defined(CRYPTO_COUNT) && (CRYPTO_COUNT > 0) */
