#include "btl_security_ecdsa.h"
#include "ecc/ecc.h"
#include "em_device.h"
#include "em_cmu.h"
#include "btl_security_tokens.h"

#include <stddef.h>
#include <string.h> // For memset

/** Verify the ECDSA signature of the SHA hash, using
 *  the public key in the relevant token, with the signature contained in
 *  the byte arrays pointed to.
 */
bool btl_verifyEcdsaP256r1(const uint8_t *sha256, const uint8_t *signatureR, const uint8_t* signatureS)
{
  if(sha256 == NULL || signatureR == NULL || signatureS == NULL)
  {
    return false;
  }

  // Re-enable the clock of the CRYPTO module, since mbedtls turned it off
  CMU_ClockEnable(cmuClock_CRYPTO, true);

  ECC_Point_t pubkey;
  memset(&pubkey, 0, sizeof(ECC_Point_t));
  ECC_ByteArrayToBigInt(pubkey.X, btl_getSignedBootloaderKeyXPtr());
  ECC_ByteArrayToBigInt(pubkey.Y, btl_getSignedBootloaderKeyYPtr());

  ECC_EcdsaSignature_t ecc_signature;
  ECC_ByteArrayToBigInt(ecc_signature.r, signatureR);
  ECC_ByteArrayToBigInt(ecc_signature.s, signatureS);

  if (ECC_ECDSA_VerifySignatureP256(CRYPTO, sha256, sizeof(ECC_BigInt_t), &pubkey, &ecc_signature)) {
    return true;
  } else {
    return false;
  }
}