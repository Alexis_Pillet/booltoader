/***************************************************************************//**
 * @file btl_storage.h
 * @brief Storage plugin for Silicon Labs Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include BTL_CONFIG_FILE

#ifdef BOOTLOADER_SUPPORT_STORAGE

#include "api/btl_interface.h"
#include "core/btl_bootload.h"
#include "plugin/debug/btl_debug.h"
#include "btl_storage.h"
#include <string.h>

const BootloaderStorageFunctions_t storageFunctions = {
  // Get information about the storage -- capabilities, configuration
  &storage_getInfo,
  // Get information about a storage slot -- size, location
  &storage_getSlotInfo,
  // Read bytes from slot into buffer
  &storage_readSlot,
  // Write bytes from buffer into slot
  &storage_writeSlot,
  // Erase an entire slot
  &storage_eraseSlot,
  // Mark a slot for bootload
  &storage_setAppImageToBootload,
  // Mark two slots for bootloader + app upgrade
  &storage_setBootloaderImageToBootload,
  // Start verification of image in a storage slot
  .initVerifyImage = &storage_initVerifySlot,
  // Continue verification of image in a storage slot
  .verifyImage = &storage_continueVerifySlot,
  // Whether storage is busy
  .isBusy = &storage_isBusy,

#ifdef BOOTLOADER_SUPPORT_RAW_STORAGE
  // Read raw bytes from storage
  &storage_readRaw,
  // Write bytes to raw storage
  &storage_writeRaw,
  // Erase storage
  &storage_eraseRaw,
#endif
};

int32_t storage_initVerifySlot(uint32_t slotId,
                            BootloaderVerificationContext_t *context,
                            size_t contextSize
                            )
{

  BTL_DEBUG_PRINT("Reqd ctx size: ");
  BTL_DEBUG_PRINT_WORD_HEX(sizeof(BootloaderVerificationContext_t));
  BTL_DEBUG_PRINTC('\n');

  if (sizeof(BootloaderVerificationContext_t) > contextSize) {
    // Context is not large enough
    return BOOTLOADER_ERROR_VERIFICATION_CONTEXT;
  }

  memset(context, 0, sizeof(BootloaderVerificationContext_t));
  BootloaderStorageSlot_t slot;
  storage_getSlotInfo(slotId, &slot);

  context->slotId = slotId;
  context->slotSize = slot.length;
  context->slotOffset = 0;
  context->errorCode = 0;

  context->imageProperties.imageVersion = 0;
  context->imageProperties.imageContainsSsb = false;
  context->imageProperties.imageContainsApp = false;
  context->imageProperties.imageCompleted = false;
  context->imageProperties.imageVerified = false;

  btl_startParseImage(&(context->parserContext));

  return BOOTLOADER_OK;
}

int32_t storage_continueVerifySlot(BootloaderVerificationContext_t *context)
{
  uint8_t readBuffer[BTL_STORAGE_READ_BUFFER_SIZE];
  const BtlImageDataCallbacks_t parseCb = {NULL, NULL, NULL};

  if ((context->errorCode == 0) && (context->slotOffset < context->slotSize)) {
    // There is still data left to parse
    storage_readSlot(context->slotId,
                     context->slotOffset,
                     (uint32_t *)readBuffer,
                     BTL_STORAGE_READ_BUFFER_SIZE);

    context->errorCode = btl_parseImageData(&(context->parserContext),
                                            &(context->imageProperties),
                                            readBuffer,
                                            BTL_STORAGE_READ_BUFFER_SIZE,
                                            (BtlImageDataCallbacks_t*)&parseCb);

    context->slotOffset += BTL_STORAGE_READ_BUFFER_SIZE;
    if (context->errorCode != EBL_PARSER_ERROR_OK
        && context->errorCode != EBL_PARSER_ERROR_EOF) {
      return BOOTLOADER_ERROR_VERIFICATION_FAILED;
    } else {
      return BOOTLOADER_ERROR_VERIFICATION_CONTINUE;
    }
  } else {
    // Parsing is complete, perform verification
    if(context->imageProperties.imageCompleted) {
      if(!BTL_ALLOW_DOWNGRADE 
         && !btl_versionAllowed(context->imageProperties.imageVersion)) {
        BTL_DEBUG_PRINT("Version na!\n");
        return BOOTLOADER_ERROR_VERIFICATION_FAILED;
      } else if(context->imageProperties.imageVerified) {
        return BOOTLOADER_ERROR_VERIFICATION_SUCCESS;
      }
    } else {
      BTL_DEBUG_PRINT("Parse fail 0x");
      BTL_DEBUG_PRINT_WORD_HEX(context->errorCode);
      BTL_DEBUG_PRINTC('\n');
      return BOOTLOADER_ERROR_VERIFICATION_FAILED;
    }
  }

  return BOOTLOADER_ERROR_VERIFICATION_FAILED;
}

#endif // BOOTLOADER_SUPPORT_STORAGE
