/***************************************************************************//**
 * @file btl_storage.h
 * @brief Storage plugin for Silicon Labs Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef BTL_STORAGE_H
#define BTL_STORAGE_H

#include BTL_CONFIG_FILE

#include <stddef.h>
#include <stdbool.h>

#include "api/btl_interface.h"
// For the parser context
#include "plugin/parser/ebl/btl_ebl_parser.h"

/***************************************************************************//**
 * @addtogroup Plugin
 * @{
 * @addtogroup Storage
 * @{
 * @brief Storage plugin
 * @details
 *   This plugin provides the bootloader with multiple storage options. All
 *   storage implementations have to provide a slot-based API to access image
 *   files to be bootloaded.
 *  
 *   Some storage implementations also support a raw storage API to access the
 *   underlying storage medium. This can be used by applications to store other
 *   data in parts of the storage medium that are not used for bootloading.
 *
 * @section storage_implementations Storage Implementations
 ******************************************************************************/

/// Dynamic information about the current state of bootloading
typedef struct {
  /// Slot ID to bootload an app from. -1 if N/A.
  int appSlotId;
  /// Slot ID for bootloading a new bootloader. -1 if N/A.
  int bootloaderSlotId;
} BootloadInfo_t;

/// Information about the storage backend
typedef struct {
  /// Type of storage
  BootloaderStorageType_t storageType;
  /// Number of slots
  uint32_t numSlots;
  /// Array of slot information.
  BootloaderStorageSlot_t slot[BTL_PLUGIN_STORAGE_NUM_SLOTS];
} BootloaderStorageLayout_t;


struct BootloaderVerificationContext {
  /// Slot ID to try and parse an image from
  uint32_t              slotId;
  /// Properties of the image being parsed
  BtlImageProperties_t  imageProperties;
  /// Context information for the image parser
  EblParserContext_t    parserContext;
  /// Size of storage slot
  uint32_t              slotSize;
  /// Offset into storage slot
  uint32_t              slotOffset;
  /// Error code returned by the image parser
  uint32_t              errorCode;
};

/***************************************************************************//**
 * Initialize the storage plugin. 
 *
 * @return @ref BOOTLOADER_OK on success, else @ref BOOTLOADER_ERROR_INIT_STORAGE
 ******************************************************************************/
int32_t storage_init(void);

/***************************************************************************//**
 * Shutdown storage plugin.
 *
 * @return @ref BOOTLOADER_OK on success, else @ref BOOTLOADER_ERROR_INIT_STORAGE
 ******************************************************************************/
int32_t storage_shutdown(void);

/***************************************************************************//**
 * Get information about the storage plugin running on the device.
 *
 * @param[out] info Pointer to @ref BootloaderStorageInformation_t struct
 ******************************************************************************/
void storage_getInfo(BootloaderStorageInformation_t *info);

/***************************************************************************//**
 * Get information about a storage slot
 *
 * @param[in]  slotId Slot ID to get info about
 * @param[out] slot   Pointer to @ref BootloaderStorageSlot_t struct
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_getSlotInfo(uint32_t slotId,
                         BootloaderStorageSlot_t *slot);

/***************************************************************************//**
 * Poll storage for which image has been marked for bootloading.
 * 
 * @param bootloadInfo Pointer to BootloadInfo_t struct to be
 *   populated by this function
 *
 * @return True if an image has been marked for bootload, False if no image has
 *         been marked for bootload
 ******************************************************************************/
bool storage_getImageToBootload(BootloadInfo_t* bootloadInfo);

/***************************************************************************//**
 * Initialize the context variable for checking a slot and trying to parse
 * the image contained in it.
 *
 * @param[in] slotId      Slot to check for valid image
 * @param[in] context     Pointer to BootloaderVerificationContext_t struct
 * @param[in] contextSize Length of the context struct
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_VERIFICATION_BASE range
 ******************************************************************************/
int32_t storage_initVerifySlot(uint32_t slotId,
                               BootloaderVerificationContext_t *context,
                               size_t contextSize);

/***************************************************************************//**
 * Check the given slot for the presence of a valid image. This function needs
 * to be called continuously until it stops returning
 * @ref BOOTLOADER_ERROR_VERIFICATION_CONTINUE.
 *
 * The function returns @ref BOOTLOADER_ERROR_VERIFICATION_SUCCESS if the 
 * image in the slot was successfully verified. For detailed information on the
 * parsed image, check imageProperties in the context variable. 
 *
 * @param[in] context Pointer to BootloaderImageParsingContext_t struct
 *
 * @return @ref BOOTLOADER_ERROR_VERIFICATION_CONTINUE if the parsing is not
 *         complete, @ref BOOTLOADER_ERROR_VERIFICATION_SUCCESS on success.
 ******************************************************************************/
int32_t storage_continueVerifySlot(BootloaderVerificationContext_t *context);

/***************************************************************************//**
 * Mark given slot for bootloading an app from it.
 *
 * Marks the given slot for bootloading a new app. If passed -1,
 * it unmarks any app currently marked.
 *
 * @param appSlotId Slot to mark for bootloading an app.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_setAppImageToBootload(int appSlotId);

/***************************************************************************//**
 * Mark given slot for bootloading a new second stage bootloader from it.
 *
 * Marks the given slot for bootloading a new second stage bootloader.
 *
 * @note Upgrading the second stage bootloader will invalidate the application
 *       currently loaded. Therefore, one must mark an application image for
 *       upgrade as well, such that the application can be restored after
 *       upgrading the bootloader. If the new bootloader and application are
 *       in the same file (same slot), then appSlotId needs to point to the same
 *       slot as bootloaderSlotId.
 *
 * @param bootloaderSlotId Slot to mark for fetching the new bootloader image from
 * @param appSlotId        Slot to mark for fetching the app image from
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_setBootloaderImageToBootload(int bootloaderSlotId,
                                             int appSlotId);

/***************************************************************************//**
 * Mark given slot as completed
 *
 * @param bootloaderSlotId Slot a new bootloader was succesfully loaded from
 * @param appSlotId        Slot a new bootloader was succesfully loaded from
 *
 * @return True if successful.
 ******************************************************************************/
bool storage_markBootloadComplete(int bootloaderSlotId,
                                  int appSlotId);

/***************************************************************************//**
 * Erase the contents of a storage slot, including all data and metadata.
 *
 * @param slotId ID of the slot.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_eraseSlot(uint32_t slotId);

/***************************************************************************//**
 * Read number of words from a storage slot.
 *
 * @param slotId     ID of the slot.
 * @param offset     The offset into the slot in bytes.
 * @param buffer     Pointer to buffer to store read data in.
 * @param numBytes   Number of bytes to read.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_readSlot(uint32_t slotId,
                      uint32_t offset,
                      uint32_t *buffer,
                      size_t   numBytes);

/***************************************************************************//**
 * Write a number of words to a storage slot.
 *
 * @param slotId   ID of the slot.
 * @param offset   The offset into the slot in bytes.
 * @param data     Pointer to data to write.
 * @param numBytes Length of data to write. Must be a multiple of 4.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_writeSlot(uint32_t slotId,
                       uint32_t offset,
                       uint32_t *data,
                       size_t  numBytes);

/***************************************************************************//**
 * Read number of words from raw storage.
 *
 * @param address    The raw address of the storage.
 * @param buffer     Pointer to buffer to store read data in.
 * @param numBytes   Number of bytes to read.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_readRaw(uint32_t address,
                     uint8_t  *buffer,
                     size_t   numBytes);

/***************************************************************************//**
 * Write a number of words to raw storage.
 *
 * @param address  The raw address of the storage.
 * @param data     Pointer to data to write.
 * @param numBytes Length of data to write. Must be a multiple of 4.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_writeRaw(uint32_t address,
                      uint8_t  *data,
                      size_t   numBytes);

/***************************************************************************//**
 * Erase the raw storage.
 *
 * @param address Start address of region to erase
 * @param length  Number of bytes to erase
 *
 * @note Some devices, such as flash-based storages, have restrictions on
 *       the alignment and size of erased regions. The details of the
 *       limitations of a particular storage can be found by reading
 *       the BootloaderStorageInformation_t struct using @ref storage_getInfo.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t storage_eraseRaw(uint32_t address, size_t length);

/***************************************************************************//**
 * Poll the storage implementation and check whether it is busy
 *
 * @return True if storage is busy
 ******************************************************************************/
bool storage_isBusy(void);

/** @} // addtogroup Storage */
/** @} // addtogroup Plugin  */

#endif // BTL_STORAGE_H
