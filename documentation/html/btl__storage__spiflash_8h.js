var btl__storage__spiflash_8h =
[
    [ "StorageSpiflashDevice_t", "group__Storage.html#ga3c6f74e4aa45699469e30feaea792b5d", [
      [ "UNKNOWN_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da08aa93e33c999511a60155872d3237eb", null ],
      [ "SPANSION_8M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5dabf46d31eba6d9cb0df5547ce7db51fe6", null ],
      [ "WINBOND_2M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da1ac94010a5151737f03827ba612bd8bf", null ],
      [ "WINBOND_8M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da5987a428d69e696f0efe2d5406aa0e25", null ],
      [ "MACRONIX_2M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da953e224c8e44c0602a7c1ee3a69ac836", null ],
      [ "MACRONIX_4M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5dac1e6de952949fd2f5b9b48ce29edb533", null ],
      [ "MACRONIX_8M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5dad2b3903def256676a0c07ac84d624825", null ],
      [ "MACRONIX_8M_LP_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da9555b6bca4dcddf7025b2126315040ea", null ],
      [ "MACRONIX_16M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da3f27277a4aa7cedaea7c68899cce795f", null ],
      [ "MACRONIX_16M_2V_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da3dafe2b97a08b344d2d4a517b3bcc53d", null ],
      [ "MACRONIX_64M_LP_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5daa58a3fa683ecb3bcd7f1342b3343ba14", null ],
      [ "ATMEL_4M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da352d087882bb9cff8deaf40c1035f1c9", null ],
      [ "ATMEL_8M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5dac43396c96efe9f61659f2b7fde8a2d88", null ],
      [ "NUMONYX_2M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5dad8afe44fac7df8dbcd1f354479e87acc", null ],
      [ "NUMONYX_4M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5daecd8d654e9672e462cbafbfbd25f9576", null ],
      [ "NUMONYX_8M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da5d420c6595b51a2b7a08b2d5cd97afd0", null ],
      [ "NUMONYX_16M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5daeb77337f0a7e9979c5d25bd50603fb63", null ],
      [ "ISSI_256K_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5dadf9cbb45e336c929769b95f44714beb3", null ],
      [ "ISSI_512K_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5dab9bbeb33c69aac75b0c4305b2db41cba", null ],
      [ "ISSI_1M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5daeda3737992d60f1aff7c7bc400518d15", null ],
      [ "ISSI_2M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da350154c7886ccbecc19cbf6f781e23f2", null ],
      [ "ISSI_4M_DEVICE", "group__Storage.html#gga3c6f74e4aa45699469e30feaea792b5da750713943d60ec48d80f8e3b9f5f3a34", null ]
    ] ]
];