var group__CommonInterface =
[
    [ "Error Codes", "group__ErrorCodes.html", "group__ErrorCodes" ],
    [ "Reset Information", "group__ResetInterface.html", "group__ResetInterface" ],
    [ "BootloaderVersion_t", "structBootloaderVersion__t.html", [
      [ "major", "structBootloaderVersion__t.html#ab94bf0407a93fceb2bece5405ca6f56c", null ],
      [ "minor", "structBootloaderVersion__t.html#abcacb7535f5797abb2a2e152083dd5b1", null ],
      [ "patch", "structBootloaderVersion__t.html#a8a0044506842900e8a1306e9720552d2", null ],
      [ "customer", "structBootloaderVersion__t.html#af6ad2c8aaa09c507ff20fe0e4394ce43", null ]
    ] ],
    [ "BootloaderInformation_t", "structBootloaderInformation__t.html", [
      [ "type", "structBootloaderInformation__t.html#a9e6653d8e7ae2516b077737c85708024", null ],
      [ "version", "structBootloaderInformation__t.html#aacb3fdc6de93c31c1053fe397cde92f8", null ],
      [ "capabilities", "structBootloaderInformation__t.html#a36c2f75904a45bdcf2077d7c729230fc", null ]
    ] ],
    [ "BareBootTable_t", "structBareBootTable__t.html", [
      [ "stackTop", "structBareBootTable__t.html#acbe8d7a123f12c3bbc0d1aa681c7d034", null ],
      [ "resetVector", "structBareBootTable__t.html#a85ecf0e4b1d04592d385083a092fbf5e", null ],
      [ "faultVectors", "structBareBootTable__t.html#a4d86fc202d2327e1023d06258ba9a992", null ]
    ] ],
    [ "FirstBootloaderTable_t", "structFirstBootloaderTable__t.html", [
      [ "type", "structFirstBootloaderTable__t.html#a8f3bd7c636788cac647e8ea203480be4", null ],
      [ "version", "structFirstBootloaderTable__t.html#a1264f5aa10c493e1e9d2e1824d8b9997", null ],
      [ "secondStageStart", "structFirstBootloaderTable__t.html#ad243d2a21a9f87880cf8a00ab2eee4cb", null ],
      [ "upgradeLocationStart", "structFirstBootloaderTable__t.html#ac85dc258aa1d6be72395102dae7987d9", null ]
    ] ],
    [ "SecondBootloaderTable_t", "structSecondBootloaderTable__t.html", [
      [ "type", "structSecondBootloaderTable__t.html#abc06752661c42535bd92739c33645972", null ],
      [ "version", "structSecondBootloaderTable__t.html#ac7d565bb62d903d5b5423ce68c186051", null ],
      [ "size", "structSecondBootloaderTable__t.html#a20c59016de7fc077898be87c20fd61a2", null ],
      [ "appStart", "structSecondBootloaderTable__t.html#a66cec22a325353dbca256592fe056cd1", null ],
      [ "endOfAppSpace", "structSecondBootloaderTable__t.html#a34a34c8eb1c01e728442a0d5a647115a", null ],
      [ "capabilities", "structSecondBootloaderTable__t.html#a3aba3fb728f85325150612cb802ebac7", null ],
      [ "init", "structSecondBootloaderTable__t.html#ad482df06b5c7ef09812e619433b030ed", null ],
      [ "deinit", "structSecondBootloaderTable__t.html#af6c07f0ed948a39b0abcdea53407c779", null ],
      [ "storage", "structSecondBootloaderTable__t.html#a15fb2585581925a242cf5d491d6c0e82", null ]
    ] ],
    [ "BOOTLOADER_CAPABILITY_EBL", "group__CommonInterface.html#ga3c1b8ac013a6b9e58d14b9b4b45007da", null ],
    [ "BOOTLOADER_CAPABILITY_EBL_SIGNATURE", "group__CommonInterface.html#ga15bd96d662269c23b2081ffb6b260d17", null ],
    [ "BOOTLOADER_CAPABILITY_EBL_ENCRYPTION", "group__CommonInterface.html#ga7ca8f187eba8ecc7eacfa9c088dc2084", null ],
    [ "BOOTLOADER_CAPABILITY_SECURE_BOOT", "group__CommonInterface.html#ga2174cc30c4ae726f9d24d26599f68ad8", null ],
    [ "BOOTLOADER_CAPABILITY_UPGRADE", "group__CommonInterface.html#ga707b1422cff90f8a55936241e3041af2", null ],
    [ "BOOTLOADER_CAPABILITY_STORAGE", "group__CommonInterface.html#ga23fd85788bda58ea6e4f28eab4b74539", null ],
    [ "BOOTLOADER_CAPABILITY_COMMUNICATION", "group__CommonInterface.html#ga8399dd23ea8f5e3232359386c37f4c63", null ],
    [ "BOOTLOADER_MAGIC_FIRST_STAGE", "group__CommonInterface.html#ga264cbf37f524c70a4e6824cbf0095ccb", null ],
    [ "BOOTLOADER_MAGIC_SECOND_STAGE", "group__CommonInterface.html#gac178a6e8c6d40fec74d21d2afae1e403", null ],
    [ "BootloaderType_t", "group__CommonInterface.html#ga8c6b9a36c4c310e50b0b3511cccbe078", [
      [ "NONE", "group__CommonInterface.html#gga8c6b9a36c4c310e50b0b3511cccbe078ac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "SL_BOOTLOADER", "group__CommonInterface.html#gga8c6b9a36c4c310e50b0b3511cccbe078ad8935a7f43596d88d93a7a5a15ed6a20", null ]
    ] ],
    [ "bootloader_getInfo", "group__CommonInterface.html#ga36c02adad76bd72c8564cae1418cdbad", null ],
    [ "bootloader_init", "group__CommonInterface.html#ga3c66502510a5302eb25149df070d898b", null ],
    [ "bootloader_deinit", "group__CommonInterface.html#ga645ab019546fe7d05e4f9599919a5363", null ],
    [ "bootloader_rebootAndInstall", "group__CommonInterface.html#ga100dba06fa6561f27e924e6eb56cfa47", null ]
];