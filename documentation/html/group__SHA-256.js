var group__SHA_256 =
[
    [ "BtlSha256State_t", "unionBtlSha256State__t.html", [
      [ "shaContext", "unionBtlSha256State__t.html#ad6fa29e719dfc88c73050d8d45a7e634", null ],
      [ "sha", "unionBtlSha256State__t.html#a620746005dabb9ab542c5bea48bfdeaf", null ]
    ] ],
    [ "BTL_SECURITY_SHA256_DIGEST_LENGTH", "group__SHA-256.html#gab2d9d701f15a5fe825f8729025cae5af", null ],
    [ "btl_initSha256", "group__SHA-256.html#ga32e58fab0e929319d0c275e592b6a379", null ],
    [ "btl_updateSha256", "group__SHA-256.html#ga16e36f1aed4297dbbcaf3724154054a3", null ],
    [ "btl_finalizeSha256", "group__SHA-256.html#gadfae75e44451ba05f19578acb0db152e", null ],
    [ "btl_verifySha256", "group__SHA-256.html#gaa45580bc1006df8c91baea33d2f08874", null ]
];