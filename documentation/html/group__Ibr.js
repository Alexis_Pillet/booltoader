var group__Ibr =
[
    [ "IndirectBootRecord_t", "structIndirectBootRecord__t.html", [
      [ "magic", "structIndirectBootRecord__t.html#a71202018684139e24f417074c5805057", null ],
      [ "versionMajor", "structIndirectBootRecord__t.html#ad05f637ec2d070c536b208d5ded38434", null ],
      [ "versionMinor", "structIndirectBootRecord__t.html#a35d16ef02d9717103fbfeaa183aa28ef", null ],
      [ "reserved", "structIndirectBootRecord__t.html#af7f344da0cede28f9ef904b6d38ea6de", null ],
      [ "appUpgradeAddress", "structIndirectBootRecord__t.html#adf445efc1dcf90ec5780e80872026cb7", null ],
      [ "bootloaderUpgradeAddress", "structIndirectBootRecord__t.html#add7b7a466c5aa98bed6d55adb4c2efa5", null ],
      [ "crc32", "structIndirectBootRecord__t.html#a12571ece8dcad60972740cb734c496f7", null ],
      [ "crc32AfterUpgrade", "structIndirectBootRecord__t.html#a41bf70c6f0ab237b85e294b8b7d0d787", null ]
    ] ],
    [ "storage_parseIbr", "group__Ibr.html#ga97b27e54a48a9cbf3cd2be74ca71797d", null ],
    [ "storage_createIbr", "group__Ibr.html#ga197c22e880a2e7a30b57bfecac6ac922", null ]
];