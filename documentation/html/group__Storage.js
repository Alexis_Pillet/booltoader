var group__Storage =
[
    [ "Spiflash Configurations", "group__SpiflashConfigs.html", null ],
    [ "BootloadInfo_t", "structBootloadInfo__t.html", [
      [ "appSlotId", "structBootloadInfo__t.html#acfb368f901bc5038920aaeb7713466b5", null ],
      [ "bootloaderSlotId", "structBootloadInfo__t.html#ae3680d54c8d6eb050811fbb020fdd9d2", null ]
    ] ],
    [ "BootloaderStorageLayout_t", "structBootloaderStorageLayout__t.html", [
      [ "storageType", "structBootloaderStorageLayout__t.html#a6ee6a9dbd538a19daf3cf23269b0deed", null ],
      [ "numSlots", "structBootloaderStorageLayout__t.html#a3c6d6dfdf83f3937b3b6ba5d1a8c6557", null ],
      [ "slot", "structBootloaderStorageLayout__t.html#a07f15bba3f92c9ee2a27348715d33341", null ]
    ] ],
    [ "BootloaderVerificationContext", "structBootloaderVerificationContext.html", [
      [ "slotId", "structBootloaderVerificationContext.html#aa1a6ea118a6f6f0dc638905404dc9ddc", null ],
      [ "imageProperties", "structBootloaderVerificationContext.html#a4db2ae1092d3ee6969c9304a4d66f4ca", null ],
      [ "parserContext", "structBootloaderVerificationContext.html#a89886263761a9bbc7aba9ebd5171bc77", null ],
      [ "slotSize", "structBootloaderVerificationContext.html#a304286fe67a62f1e32ffecc0fa18d0fb", null ],
      [ "slotOffset", "structBootloaderVerificationContext.html#a52b5f391c590e1fc157055475e0aee24", null ],
      [ "errorCode", "structBootloaderVerificationContext.html#abdbbae6c5898eedfd2f52b33284d40e1", null ]
    ] ],
    [ "StorageSpiflashDevice_t", "group__Storage.html#ga3c6f74e4aa45699469e30feaea792b5d", null ],
    [ "storage_init", "group__Storage.html#ga94a4426978ff3f09c117f90d2276d3d2", null ],
    [ "storage_shutdown", "group__Storage.html#gab819eceb2cd475bf69fbd9f0ccea8119", null ],
    [ "storage_getInfo", "group__Storage.html#ga3064ede663216fc3f7fa747475c90d66", null ],
    [ "storage_getSlotInfo", "group__Storage.html#ga1802df1bf013527657b1d6abc565187d", null ],
    [ "storage_getImageToBootload", "group__Storage.html#ga6fef573e022d7bd16bbd4a4f7058adfc", null ],
    [ "storage_initVerifySlot", "group__Storage.html#gad03a130bd27d545684d8efb2b005373a", null ],
    [ "storage_continueVerifySlot", "group__Storage.html#gad524488e9c6001f2e16ca0ea22a5b72e", null ],
    [ "storage_setAppImageToBootload", "group__Storage.html#gaa0da415f752c2cbf0c9cea3b4e5a1e6e", null ],
    [ "storage_setBootloaderImageToBootload", "group__Storage.html#ga8b0bbe4e0648a1696cb28eed5db30747", null ],
    [ "storage_markBootloadComplete", "group__Storage.html#ga91fbea9339948c1dfd20a08c622ce985", null ],
    [ "storage_eraseSlot", "group__Storage.html#ga9610199da9571e0ef1f908550f51502c", null ],
    [ "storage_readSlot", "group__Storage.html#gac333fa34bd706ab281336f16537b6c15", null ],
    [ "storage_writeSlot", "group__Storage.html#ga0341e174de48e36445c47d1338e16eac", null ],
    [ "storage_readRaw", "group__Storage.html#ga015ad4df1bc5dab66beac5ff9f0185e4", null ],
    [ "storage_writeRaw", "group__Storage.html#gafc13b13324ec97d3c82127aeac0b1478", null ],
    [ "storage_eraseRaw", "group__Storage.html#ga1db0fbd4a50f417f0cf87309ed45b706", null ],
    [ "storage_isBusy", "group__Storage.html#gaae59d446077156e9e90e1ca271f3ec2a", null ]
];