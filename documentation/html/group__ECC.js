var group__ECC =
[
    [ "ECC_Point_t", "structECC__Point__t.html", [
      [ "X", "structECC__Point__t.html#a23b4946758803fa98e59c396045fee8d", null ],
      [ "Y", "structECC__Point__t.html#a085c9d3b990e85aca6e2aa33b7223ad0", null ]
    ] ],
    [ "ECC_EcdsaSignature_t", "structECC__EcdsaSignature__t.html", [
      [ "r", "structECC__EcdsaSignature__t.html#ade4aac0e8fa53e9d22cd602237658b2f", null ],
      [ "s", "structECC__EcdsaSignature__t.html#a8a0fb19aa28acbf0e616b1eaf04eb081", null ]
    ] ],
    [ "ECODE_BTL_ECC_BASE", "group__ECC.html#ga214adb331ee9a956468991d56a6679ef", null ],
    [ "ECODE_BTL_ECC_INVALID_CURVE_ID", "group__ECC.html#gac60108c2f02c3b9e1ceefdf400558447", null ],
    [ "ECODE_BTL_ECC_PARAM_OUT_OF_RANGE", "group__ECC.html#ga67397aa1447171ea03ccc91b4a26c257", null ],
    [ "ECODE_BTL_ECC_SIGN_FAIL", "group__ECC.html#ga43e8288b306599dfa887a43d01cecc89", null ],
    [ "ECODE_BTL_ECC_SIGNATURE_INVALID", "group__ECC.html#ga24c26c98804d1aef035a27e90809feb6", null ],
    [ "ECC_BIGINT_SIZE_IN_BITS", "group__ECC.html#ga74b13beb30f0eafaf0dd979ebd96e51b", null ],
    [ "ECC_BIGINT_SIZE_IN_BYTES", "group__ECC.html#ga2f5d40dc29d58de9bf86d534a2a953a9", null ],
    [ "ECC_BIGINT_SIZE_IN_32BIT_WORDS", "group__ECC.html#ga36f016a5c876eeea1ccb4c4a57bcb372", null ],
    [ "ECC_BigInt_t", "group__ECC.html#ga7473b9142bdec741ae4e2d2be8f703f4", null ],
    [ "ECC_ECDSA_VerifySignatureP256", "group__ECC.html#ga277d9d7928b1a63a7512dfc9c52db5c4", null ],
    [ "ECC_HexToBigInt", "group__ECC.html#gaf44e7fb772b55174dcf335c54f9514e2", null ],
    [ "ECC_BigIntToHex", "group__ECC.html#gac785c57fdc324b9feab818e3d0cf9225", null ],
    [ "ECC_ByteArrayToBigInt", "group__ECC.html#ga990ce75f282b6030721229cb79161290", null ],
    [ "ECC_BigIntToByteArray", "group__ECC.html#ga899570b76c2809c84295e5dd0c7dc342", null ],
    [ "ECC_UnsignedIntToBigInt", "group__ECC.html#gaff60f872dde7ce6df9302856d075d2f8", null ]
];