var ecc_8h =
[
    [ "INCLUDE_ECC_P256", "group__ECC.html#ga9e462cc3f58328c02388b534ca9a9a73", null ],
    [ "ECODE_BTL_ECC_BASE", "group__ECC.html#ga214adb331ee9a956468991d56a6679ef", null ],
    [ "ECODE_BTL_ECC_OK", "group__ECC.html#gac8c2e83bfd1de4e28ddb1b12acc09f64", null ],
    [ "ECODE_BTL_ECC_INVALID_PARAM", "group__ECC.html#ga67b356b6e992a77660b81111f1d73434", null ],
    [ "ECODE_BTL_ECC_INVALID_CURVE_ID", "group__ECC.html#gac60108c2f02c3b9e1ceefdf400558447", null ],
    [ "ECODE_BTL_ECC_PARAM_OUT_OF_RANGE", "group__ECC.html#ga67397aa1447171ea03ccc91b4a26c257", null ],
    [ "ECODE_BTL_ECC_SIGN_FAIL", "group__ECC.html#ga43e8288b306599dfa887a43d01cecc89", null ],
    [ "ECODE_BTL_ECC_SIGNATURE_INVALID", "group__ECC.html#ga24c26c98804d1aef035a27e90809feb6", null ],
    [ "ECC_BIGINT_SIZE_IN_BITS", "group__ECC.html#ga74b13beb30f0eafaf0dd979ebd96e51b", null ],
    [ "ECC_BIGINT_SIZE_IN_BYTES", "group__ECC.html#ga2f5d40dc29d58de9bf86d534a2a953a9", null ],
    [ "ECC_BIGINT_SIZE_IN_32BIT_WORDS", "group__ECC.html#ga36f016a5c876eeea1ccb4c4a57bcb372", null ],
    [ "ECC_BigInt_t", "group__ECC.html#ga7473b9142bdec741ae4e2d2be8f703f4", null ],
    [ "ECC_ECDSA_VerifySignatureP256", "group__ECC.html#ga277d9d7928b1a63a7512dfc9c52db5c4", null ],
    [ "ECC_HexToBigInt", "group__ECC.html#gaf44e7fb772b55174dcf335c54f9514e2", null ],
    [ "ECC_BigIntToHex", "group__ECC.html#gac785c57fdc324b9feab818e3d0cf9225", null ],
    [ "ECC_ByteArrayToBigInt", "group__ECC.html#ga990ce75f282b6030721229cb79161290", null ],
    [ "ECC_BigIntToByteArray", "group__ECC.html#ga899570b76c2809c84295e5dd0c7dc342", null ],
    [ "ECC_UnsignedIntToBigInt", "group__ECC.html#gaff60f872dde7ce6df9302856d075d2f8", null ]
];