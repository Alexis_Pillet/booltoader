var group__Debug =
[
    [ "BTL_DEBUG_FAILURE", "group__Debug.html#ga091634155f800470b0c1ca25617c5c97", null ],
    [ "BTL_DEBUG_INIT", "group__Debug.html#ga0fb57cd438d6a79cf1cbec87dfe4b65c", null ],
    [ "BTL_DEBUG_PRINT", "group__Debug.html#gaa91c8e04fc2071acb5207aa93deb64b5", null ],
    [ "BTL_DEBUG_PRINTC", "group__Debug.html#ga7267e1f42a336f8ef05b362b0b7862b3", null ],
    [ "BTL_DEBUG_PRINT_CHAR_HEX", "group__Debug.html#ga6b763d7e5d886bb30a41c4335941f242", null ],
    [ "BTL_DEBUG_PRINT_SHORT_HEX", "group__Debug.html#ga3ed24eff0829f48a8a019059b67349c0", null ],
    [ "BTL_DEBUG_PRINT_WORD_HEX", "group__Debug.html#ga91ba4c6da923c75d30ec292667f86da5", null ]
];