var btl__ebl__format_8h =
[
    [ "EBL_IMAGE_MAGIC_WORD", "group__EblParserFormat.html#ga3d8fbe471970fba2d1ca38b77731dd49", null ],
    [ "EBL_COMPATIBILITY_MAJOR_VERSION", "group__EblParserFormat.html#gaf968f8d6a0564a7afd428bd3477a69ac", null ],
    [ "EBL_TAG_ID_HEADER", "group__EblParserFormat.html#ga1821c0de679c8ce09d40fec72dd8031f", null ],
    [ "EBL_TAG_ID_METADATA", "group__EblParserFormat.html#gac61fa5d9a79733865a6d64cf55f95bbd", null ],
    [ "EBL_TAG_ID_PROG", "group__EblParserFormat.html#ga8dddb4f7be61eff95a3b45d9c5333147", null ],
    [ "EBL_TAG_ID_ERASEPROG", "group__EblParserFormat.html#ga126abf0c2a521b0d29e8d7561ce6c6ff", null ],
    [ "EBL_TAG_ID_END", "group__EblParserFormat.html#ga955a373b38135b45b796f1199568d0d9", null ],
    [ "EBL_TAG_ID_ENC_HEADER", "group__EblParserFormat.html#ga504d80f2c3cf47369769edc073c9f80a", null ],
    [ "EBL_TAG_ID_ENC_INIT", "group__EblParserFormat.html#ga9fe6ad99e196189ad07d99d6c905161a", null ],
    [ "EBL_TAG_ID_ENC_EBL_DATA", "group__EblParserFormat.html#ga8bf038cbbaf69054bd1784a27f619ee3", null ],
    [ "EBL_TAG_ID_ENC_MAC", "group__EblParserFormat.html#gade7d88b45a1a4d5a28dd6154b64f323a", null ],
    [ "EBL_TAG_ID_SIGNATURE_ECDSA_P256", "group__EblParserFormat.html#gae8bfb6989b0c9d5da8bfcbe60a089443", null ],
    [ "EBL_TYPE_ENCRYPTION_NONE", "group__EblParserFormat.html#ga5749fb626fefe3a9c8b7085b4557fd7e", null ],
    [ "EBL_TYPE_ENCRYPTION_AESCCM", "group__EblParserFormat.html#gaca1359558b18137e8f258fe6c0c9530d", null ]
];