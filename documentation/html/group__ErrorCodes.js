var group__ErrorCodes =
[
    [ "BOOTLOADER_OK", "group__ErrorCodes.html#gaf5e631186d857be458f4bdb824db1884", null ],
    [ "BOOTLOADER_ERROR_INIT_BASE", "group__ErrorCodes.html#ga2eb810411540551694610ea4b84225e3", null ],
    [ "BOOTLOADER_ERROR_INIT_STORAGE", "group__ErrorCodes.html#gacbc02816160967906ae346f88428843f", null ],
    [ "BOOTLOADER_ERROR_VERIFICATION_BASE", "group__ErrorCodes.html#gab6627db634893442e362fd0aea2e9bd5", null ],
    [ "BOOTLOADER_ERROR_VERIFICATION_CONTINUE", "group__ErrorCodes.html#ga7b2f961b4383817fa65d273cd9c51f08", null ],
    [ "BOOTLOADER_ERROR_VERIFICATION_FAILED", "group__ErrorCodes.html#ga14de6e244f9e976d78978af4ac9e0dc2", null ],
    [ "BOOTLOADER_ERROR_VERIFICATION_SUCCESS", "group__ErrorCodes.html#gafad19e3ec4856029a223dd1461736c21", null ],
    [ "BOOTLOADER_ERROR_VERIFICATION_STORAGE", "group__ErrorCodes.html#gaaf5bf54c9130851ada1a9ede40991c99", null ],
    [ "BOOTLOADER_ERROR_VERIFICATION_CONTEXT", "group__ErrorCodes.html#ga57abcac37ed6bb88ab8322f8e6dcce7d", null ],
    [ "BOOTLOADER_ERROR_STORAGE_BASE", "group__ErrorCodes.html#ga6e7d9953864f32fd1a1121ffd6f1f86e", null ],
    [ "BOOTLOADER_ERROR_STORAGE_INVALID_SLOT", "group__ErrorCodes.html#ga3145435f132fe86891e5acf827f7b6c3", null ],
    [ "BOOTLOADER_ERROR_STORAGE_INVALID_ADDRESS", "group__ErrorCodes.html#gab1242aad710a41755815998e959e88fe", null ],
    [ "BOOTLOADER_ERROR_STORAGE_NEEDS_ERASE", "group__ErrorCodes.html#gaf843dab55c61f685c364dc4d6c0bda80", null ],
    [ "BOOTLOADER_ERROR_STORAGE_NEEDS_ALIGN", "group__ErrorCodes.html#ga847aa8582199cf55f49b047982176136", null ]
];