var group__EblParser =
[
    [ "EBL Format", "group__EblParserFormat.html", "group__EblParserFormat" ],
    [ "EblBuffer_t", "structEblBuffer__t.html", [
      [ "buffer", "structEblBuffer__t.html#af9e9f36800efd76f7bedbd61ea321000", null ],
      [ "length", "structEblBuffer__t.html#a9c5d14ba3001c253327c3df7a376bc25", null ],
      [ "offset", "structEblBuffer__t.html#a7b77c6060c22ae263ab2e9d6b9508217", null ]
    ] ],
    [ "EblParserContext_t", "structEblParserContext__t.html", [
      [ "internalState", "structEblParserContext__t.html#a2373ac3a8ce0848f2aa2228f8064be37", null ],
      [ "localBuffer", "structEblParserContext__t.html#a023fbad7e9c44e3dc946af51b4ef02dc", null ],
      [ "aesContext", "structEblParserContext__t.html#a5961dc8d48e556184be0808ba980eb40", null ],
      [ "shaContext", "structEblParserContext__t.html#a7c939c534b6636daf6e35ae99621f6be", null ],
      [ "lengthOfTag", "structEblParserContext__t.html#a6ce923f606552868e20df2fc979ea046", null ],
      [ "offsetInTag", "structEblParserContext__t.html#ac7984f1ebb0127633341f573e9289df2", null ],
      [ "lengthOfEncryptedTag", "structEblParserContext__t.html#af1fa78cafb3d5beaa08a09c0c0680ee6", null ],
      [ "offsetInEncryptedTag", "structEblParserContext__t.html#a78d1860a729574dbbc8c119f4a61299b", null ],
      [ "programmingAddress", "structEblParserContext__t.html#a760d9c497f3ebf3ad63471ea133a728e", null ],
      [ "metadataAddress", "structEblParserContext__t.html#a2236525591ce84c999d5823182271113", null ],
      [ "fileCrc", "structEblParserContext__t.html#af540b8703ce4cc26e1fdc50e2a91cfdd", null ],
      [ "encrypted", "structEblParserContext__t.html#a9666eef81204490c61fc30745ec6b6d8", null ],
      [ "inEncryptedContainer", "structEblParserContext__t.html#a83eff052a36df9962ad47b05c388335f", null ],
      [ "gotSignature", "structEblParserContext__t.html#a6aecca9c8c313edc7adf95e74b0f678e", null ]
    ] ],
    [ "EBL_PARSER_ERROR_BASE", "group__EblParser.html#ga35e2016ef3dba3497e5845c5a86a518b", null ],
    [ "EBL_PARSER_ERROR_OK", "group__EblParser.html#ga4779e610f888c524dde973b49fbc7166", null ],
    [ "EBL_PARSER_ERROR_CONTINUE", "group__EblParser.html#ga0e42f784418585f5a0ac910b6542467a", null ],
    [ "EBL_PARSER_ERROR_UNEXPECTED_TAG", "group__EblParser.html#gae7ef7d238a831f27008db524bf29dae7", null ],
    [ "EBL_PARSER_ERROR_WRONG_KEY", "group__EblParser.html#ga6e75e93002d601c28396cc0d75d86506", null ],
    [ "EBL_PARSER_ERROR_BUFFER", "group__EblParser.html#ga73975c56f79bd6d3c1c1572a466bb37e", null ],
    [ "EBL_PARSER_ERROR_UNSUPPORTED_TAG", "group__EblParser.html#gae43162b63ed61ea9104d9bd6f88deef7", null ],
    [ "EBL_PARSER_ERROR_EOF", "group__EblParser.html#ga53268bc68fab921c28e82bcf12659796", null ],
    [ "EBL_PARSER_ERROR_VERSION_MISMATCH", "group__EblParser.html#ga58e9fa3746945fb2d3ad527f31f6ab4f", null ],
    [ "EBL_PARSER_ERROR_NO_HEADER", "group__EblParser.html#ga157be2677970ab255031c2cc58d33853", null ],
    [ "EBL_PARSER_ERROR_CRC", "group__EblParser.html#ga5128d78d63e581171c808975094bdd74", null ],
    [ "EBL_PARSER_ERROR_UNKNOWN_ENCRYPT", "group__EblParser.html#gaf4943a6d7d46c37ab93611cd80e4e02c", null ],
    [ "EblParserState_t", "group__EblParser.html#ga7767157da0dffc9530af3da1f87b88a3", [
      [ "EblParserStateInit", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a6ae86128d96bcff1eb0017d2bea71c1b", null ],
      [ "EblParserStateIdle", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a94a7dd46ab93e4e70b8cf96fcf6b7fa3", null ],
      [ "EblParserStateHeader", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a5629e9a4519dcaae2fa38654ef6c8b24", null ],
      [ "EblParserStateMetadata", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a744e406c55a3f170ac83bea3ecc803a5", null ],
      [ "EblParserStateProg", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ae0cc235fb4b57d95c5ce90276c94a3bb", null ],
      [ "EblParserStateEraseProg", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a7a77dfdf146c3ebd73d2975ce42f75c3", null ],
      [ "EblParserStateFinalize", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a816d08790eaf7c4f8b7747561a40bff4", null ],
      [ "EblParserStateDone", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a0c0f2b2e1f5b87bad47adc5b69a2acba", null ],
      [ "EblParserStateEncryptionInit", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ab8599d1ca1c8477e5a974cabb07f7835", null ],
      [ "EblParserStateEncryptionHeader", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a985810ac96ec268e01ad4e939a076393", null ],
      [ "EblParserStateEncryptionContainer", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3addbc4f88a9747e46d0a9fbe974d0f87f", null ],
      [ "EblParserStateEncryptionMac", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ae42a4b120a100bbfece57dfcb690b573", null ],
      [ "EblParserStateSignature", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ad058844f29e64ef44ce38ca43e1fdc17", null ],
      [ "EblParserStateError", "group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3aee0cd5896467124e1e3c072e0f720e97", null ]
    ] ]
];