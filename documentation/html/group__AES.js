var group__AES =
[
    [ "BtlAesContext_t", "structBtlAesContext__t.html", [
      [ "aesContext", "structBtlAesContext__t.html#aef3ce9b6fbf26a55792e3c0fb47cdafb", null ]
    ] ],
    [ "BtlAesCtrContext_t", "structBtlAesCtrContext__t.html", [
      [ "aesContext", "structBtlAesCtrContext__t.html#a88670e7111527950231fa98bdb98d970", null ],
      [ "offsetInBlock", "structBtlAesCtrContext__t.html#a238ab1cd8a487fce1c4c091f43954383", null ],
      [ "streamBlock", "structBtlAesCtrContext__t.html#a8979f0691d02dbb6a6bdbf921d48fef8", null ],
      [ "counter", "structBtlAesCtrContext__t.html#a4c7eab9c09cb2c7787ebe163cec62340", null ]
    ] ],
    [ "btl_initAesContext", "group__AES.html#ga355be30070bcae3b284b6a46537a70e1", null ],
    [ "btl_setAesKey", "group__AES.html#gae95bc6c4be52cf6ecb01476354a83edb", null ],
    [ "btl_processAesBlock", "group__AES.html#ga8ba6c6a414ca5ecf1d3c1123d842a169", null ],
    [ "btl_initAesCcm", "group__AES.html#ga3dd71b7bf638d1104b9be6ffa45a2597", null ],
    [ "btl_processAesCtrData", "group__AES.html#gac27e63a41863b6bba38b53fecf9bea83", null ]
];