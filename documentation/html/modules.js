var modules =
[
    [ "Application Interface", "group__Interface.html", "group__Interface" ],
    [ "Bootloader Core", "group__Core.html", "group__Core" ],
    [ "Driver", "group__Driver.html", "group__Driver" ],
    [ "Plugin", "group__Plugin.html", "group__Plugin" ]
];