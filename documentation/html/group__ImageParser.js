var group__ImageParser =
[
    [ "EBL Parser", "group__EblParser.html", "group__EblParser" ],
    [ "BtlImageProperties_t", "structBtlImageProperties__t.html", [
      [ "imageVersion", "structBtlImageProperties__t.html#ae28d060ca4699f560d5f297e8773aebc", null ],
      [ "imageContainsSsb", "structBtlImageProperties__t.html#a8c63a26f9f18f78dc1662be5b3bcf8c5", null ],
      [ "imageContainsApp", "structBtlImageProperties__t.html#a53f8c0fa0e6b30d2dabdf4d432001dd4", null ],
      [ "imageCompleted", "structBtlImageProperties__t.html#a22ffb6223a269ad391ed3fda6c4c4273", null ],
      [ "imageVerified", "structBtlImageProperties__t.html#a53a69914d08d7a859dd6292e3103d8fc", null ]
    ] ],
    [ "BtlImageDataCallbacks_t", "structBtlImageDataCallbacks__t.html", [
      [ "callbackContext", "structBtlImageDataCallbacks__t.html#a8ebbe87d020519391dc27aad6a7b2d07", null ],
      [ "dataFunction", "structBtlImageDataCallbacks__t.html#afc619b1c7406a8db90a3bf0b82de1cdf", null ],
      [ "metaDataFunction", "structBtlImageDataCallbacks__t.html#a2e616e6749120cd68799411b561d486b", null ]
    ] ],
    [ "BtlPassImageDataFunc_t", "group__ImageParser.html#ga7834e6d163deae8ebe713f3176d37bdb", null ],
    [ "BtlPassMetaDataFunc_t", "group__ImageParser.html#ga756bdd4e3b955b46ea96f6c540bb0298", null ],
    [ "BtlWriteMode_t", "group__ImageParser.html#gaddb9618e95586f2847fe891dc94cd373", [
      [ "BtlWrite", "group__ImageParser.html#ggaddb9618e95586f2847fe891dc94cd373a36d4eb377db30d55aeecf81b2467d421", null ],
      [ "BtlWriteWithErase", "group__ImageParser.html#ggaddb9618e95586f2847fe891dc94cd373a933da1673343f522e99df48e6ceb50b8", null ]
    ] ],
    [ "btl_startParseImage", "group__ImageParser.html#ga002d7eca577e81ac146bfb55cbc929f6", null ],
    [ "btl_parseImageData", "group__ImageParser.html#gafee3d36ac0cb43ddfde130d820888e30", null ]
];