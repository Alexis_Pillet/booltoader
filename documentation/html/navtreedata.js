var NAVTREE =
[
  [ "Silicon Labs Bootloader", "index.html", [
    [ "Bootloader Components", "modules.html", "modules" ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ],
    [ "silabs.com", "^http://www.silabs.com", null ]
  ] ]
];

var NAVTREEINDEX =
[
"btl__bootload_8h.html",
"group__ECDSA.html#gaab3755c98b33c4148556d5e9487de37c",
"group__SpiflashConfigs.html#ga6ab220b70e060404760a409f45298d6d",
"structBootloaderVerificationContext.html#a52b5f391c590e1fc157055475e0aee24"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';