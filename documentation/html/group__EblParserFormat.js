var group__EblParserFormat =
[
    [ "EblTagHeader_t", "structEblTagHeader__t.html", [
      [ "tagId", "structEblTagHeader__t.html#ad396b37523904f1e7a2f6127c9126c7b", null ],
      [ "length", "structEblTagHeader__t.html#a54d2044c2ceda69abd34f9756842809f", null ]
    ] ],
    [ "EblHeader_t", "structEblHeader__t.html", [
      [ "header", "structEblHeader__t.html#af233133bf321c93097533aaa2a58be55", null ],
      [ "version", "structEblHeader__t.html#ad1e93be638d36c96d5d9b3843688097f", null ],
      [ "magicWord", "structEblHeader__t.html#a727c39d0d501fc26d412be7768d46334", null ],
      [ "flashStartAddress", "structEblHeader__t.html#a4d129745953e688044fe9e8366770505", null ],
      [ "aatCrc", "structEblHeader__t.html#aed85ba55db930d1741cd6d292241d3b4", null ],
      [ "aatBuffer", "structEblHeader__t.html#a2690e31f5f7c0930dbef78836489f537", null ]
    ] ],
    [ "EblMetadata_t", "structEblMetadata__t.html", [
      [ "header", "structEblMetadata__t.html#aefd78f5b69fa534babb022a9449350a9", null ],
      [ "metaData", "structEblMetadata__t.html#a27f61c9773926d96fb5d73a17d33bb0b", null ]
    ] ],
    [ "EblProg_t", "structEblProg__t.html", [
      [ "header", "structEblProg__t.html#ac1b05431241dc925e941f216fa6820e2", null ],
      [ "flashStartAddress", "structEblProg__t.html#a7312dd5414f39ee67a33f374ee827440", null ],
      [ "data", "structEblProg__t.html#ac746a54816fac24dcfe17ccd21061e1a", null ]
    ] ],
    [ "EblEnd_t", "structEblEnd__t.html", [
      [ "header", "structEblEnd__t.html#af6ce4bba1345d2e50456ea516549b123", null ],
      [ "eblCrc", "structEblEnd__t.html#a052a7d9e438c35e6572c66c749cc8f11", null ]
    ] ],
    [ "EblEncryptionHeader_t", "structEblEncryptionHeader__t.html", [
      [ "header", "structEblEncryptionHeader__t.html#a8e6e5dcbb8b28888ef058cb210990ffc", null ],
      [ "version", "structEblEncryptionHeader__t.html#a8ba1b7a2ccbd2aceabd3159752471228", null ],
      [ "encryptionType", "structEblEncryptionHeader__t.html#aa20e130cb1a843890d8716e1fe39007d", null ],
      [ "magicWord", "structEblEncryptionHeader__t.html#aed6a39e331cff28554fbfc93f00f2a7d", null ]
    ] ],
    [ "EblEncryptionInitAesCcm_t", "structEblEncryptionInitAesCcm__t.html", [
      [ "header", "structEblEncryptionInitAesCcm__t.html#a10b419a320d8dfc79d69dc230520361c", null ],
      [ "msgLen", "structEblEncryptionInitAesCcm__t.html#a1bf7a2c1e0e0963bb9beabf1d225d86f", null ],
      [ "nonce", "structEblEncryptionInitAesCcm__t.html#af467741f1e5fc1a25354bc231e1a1fd5", null ],
      [ "associatedData", "structEblEncryptionInitAesCcm__t.html#ae9f3717c2925ef86f87692dc5dc240f8", null ]
    ] ],
    [ "EblEncryptionData_t", "structEblEncryptionData__t.html", [
      [ "header", "structEblEncryptionData__t.html#aae49cd2b847b9d75cbae498cd756cbab", null ],
      [ "encryptedEblData", "structEblEncryptionData__t.html#a5fae98495d4e6eddd0f17ba0b9a86ff1", null ]
    ] ],
    [ "EblEncryptionAesCcmSignature_t", "structEblEncryptionAesCcmSignature__t.html", [
      [ "header", "structEblEncryptionAesCcmSignature__t.html#aaf5e9a24143bcbb9f82ecb095e39ce4c", null ],
      [ "eblMac", "structEblEncryptionAesCcmSignature__t.html#a21b04543fd1ff2bfe9a94b5d5c6a660a", null ]
    ] ],
    [ "EblSignatureEcdsaP256_t", "structEblSignatureEcdsaP256__t.html", [
      [ "header", "structEblSignatureEcdsaP256__t.html#a8cbd1209b3e9549ebf50dd6aaf8c185a", null ],
      [ "r", "structEblSignatureEcdsaP256__t.html#add63ddeca52601d5958a1547460a8934", null ],
      [ "s", "structEblSignatureEcdsaP256__t.html#a27127726900609ad52143ed75627b985", null ]
    ] ],
    [ "EBL_IMAGE_MAGIC_WORD", "group__EblParserFormat.html#ga3d8fbe471970fba2d1ca38b77731dd49", null ],
    [ "EBL_COMPATIBILITY_MAJOR_VERSION", "group__EblParserFormat.html#gaf968f8d6a0564a7afd428bd3477a69ac", null ],
    [ "EBL_TAG_ID_HEADER", "group__EblParserFormat.html#ga1821c0de679c8ce09d40fec72dd8031f", null ],
    [ "EBL_TAG_ID_METADATA", "group__EblParserFormat.html#gac61fa5d9a79733865a6d64cf55f95bbd", null ],
    [ "EBL_TAG_ID_PROG", "group__EblParserFormat.html#ga8dddb4f7be61eff95a3b45d9c5333147", null ],
    [ "EBL_TAG_ID_ERASEPROG", "group__EblParserFormat.html#ga126abf0c2a521b0d29e8d7561ce6c6ff", null ],
    [ "EBL_TAG_ID_END", "group__EblParserFormat.html#ga955a373b38135b45b796f1199568d0d9", null ],
    [ "EBL_TAG_ID_ENC_HEADER", "group__EblParserFormat.html#ga504d80f2c3cf47369769edc073c9f80a", null ],
    [ "EBL_TAG_ID_ENC_INIT", "group__EblParserFormat.html#ga9fe6ad99e196189ad07d99d6c905161a", null ],
    [ "EBL_TAG_ID_ENC_EBL_DATA", "group__EblParserFormat.html#ga8bf038cbbaf69054bd1784a27f619ee3", null ],
    [ "EBL_TAG_ID_ENC_MAC", "group__EblParserFormat.html#gade7d88b45a1a4d5a28dd6154b64f323a", null ],
    [ "EBL_TAG_ID_SIGNATURE_ECDSA_P256", "group__EblParserFormat.html#gae8bfb6989b0c9d5da8bfcbe60a089443", null ],
    [ "EBL_TYPE_ENCRYPTION_NONE", "group__EblParserFormat.html#ga5749fb626fefe3a9c8b7085b4557fd7e", null ],
    [ "EBL_TYPE_ENCRYPTION_AESCCM", "group__EblParserFormat.html#gaca1359558b18137e8f258fe6c0c9530d", null ]
];