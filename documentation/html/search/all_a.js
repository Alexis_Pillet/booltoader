var searchData=
[
  ['magic',['magic',['../structIndirectBootRecord__t.html#a71202018684139e24f417074c5805057',1,'IndirectBootRecord_t']]],
  ['magicword',['magicWord',['../structEblHeader__t.html#a727c39d0d501fc26d412be7768d46334',1,'EblHeader_t::magicWord()'],['../structEblEncryptionHeader__t.html#aed6a39e331cff28554fbfc93f00f2a7d',1,'EblEncryptionHeader_t::magicWord()']]],
  ['major',['major',['../structBootloaderVersion__t.html#ab94bf0407a93fceb2bece5405ca6f56c',1,'BootloaderVersion_t']]],
  ['metadata',['metaData',['../structEblMetadata__t.html#a27f61c9773926d96fb5d73a17d33bb0b',1,'EblMetadata_t']]],
  ['metadataaddress',['metadataAddress',['../structEblParserContext__t.html#a2236525591ce84c999d5823182271113',1,'EblParserContext_t']]],
  ['metadatafunction',['metaDataFunction',['../structBtlImageDataCallbacks__t.html#a2e616e6749120cd68799411b561d486b',1,'BtlImageDataCallbacks_t']]],
  ['minor',['minor',['../structBootloaderVersion__t.html#abcacb7535f5797abb2a2e152083dd5b1',1,'BootloaderVersion_t']]],
  ['msglen',['msgLen',['../structEblEncryptionInitAesCcm__t.html#a1bf7a2c1e0e0963bb9beabf1d225d86f',1,'EblEncryptionInitAesCcm_t']]]
];
