var searchData=
[
  ['ebl_5fcompatibility_5fmajor_5fversion',['EBL_COMPATIBILITY_MAJOR_VERSION',['../group__EblParserFormat.html#gaf968f8d6a0564a7afd428bd3477a69ac',1,'btl_ebl_format.h']]],
  ['ebl_5fimage_5fmagic_5fword',['EBL_IMAGE_MAGIC_WORD',['../group__EblParserFormat.html#ga3d8fbe471970fba2d1ca38b77731dd49',1,'btl_ebl_format.h']]],
  ['ebl_5fparser_5ferror_5fbase',['EBL_PARSER_ERROR_BASE',['../group__EblParser.html#ga35e2016ef3dba3497e5845c5a86a518b',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5fbuffer',['EBL_PARSER_ERROR_BUFFER',['../group__EblParser.html#ga73975c56f79bd6d3c1c1572a466bb37e',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5fcontinue',['EBL_PARSER_ERROR_CONTINUE',['../group__EblParser.html#ga0e42f784418585f5a0ac910b6542467a',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5fcrc',['EBL_PARSER_ERROR_CRC',['../group__EblParser.html#ga5128d78d63e581171c808975094bdd74',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5feof',['EBL_PARSER_ERROR_EOF',['../group__EblParser.html#ga53268bc68fab921c28e82bcf12659796',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5fno_5fheader',['EBL_PARSER_ERROR_NO_HEADER',['../group__EblParser.html#ga157be2677970ab255031c2cc58d33853',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5fok',['EBL_PARSER_ERROR_OK',['../group__EblParser.html#ga4779e610f888c524dde973b49fbc7166',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5funexpected_5ftag',['EBL_PARSER_ERROR_UNEXPECTED_TAG',['../group__EblParser.html#gae7ef7d238a831f27008db524bf29dae7',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5funknown_5fencrypt',['EBL_PARSER_ERROR_UNKNOWN_ENCRYPT',['../group__EblParser.html#gaf4943a6d7d46c37ab93611cd80e4e02c',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5funsupported_5ftag',['EBL_PARSER_ERROR_UNSUPPORTED_TAG',['../group__EblParser.html#gae43162b63ed61ea9104d9bd6f88deef7',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5fversion_5fmismatch',['EBL_PARSER_ERROR_VERSION_MISMATCH',['../group__EblParser.html#ga58e9fa3746945fb2d3ad527f31f6ab4f',1,'btl_ebl_parser.h']]],
  ['ebl_5fparser_5ferror_5fwrong_5fkey',['EBL_PARSER_ERROR_WRONG_KEY',['../group__EblParser.html#ga6e75e93002d601c28396cc0d75d86506',1,'btl_ebl_parser.h']]],
  ['ebl_5ftag_5fid_5fenc_5febl_5fdata',['EBL_TAG_ID_ENC_EBL_DATA',['../group__EblParserFormat.html#ga8bf038cbbaf69054bd1784a27f619ee3',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fenc_5fheader',['EBL_TAG_ID_ENC_HEADER',['../group__EblParserFormat.html#ga504d80f2c3cf47369769edc073c9f80a',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fenc_5finit',['EBL_TAG_ID_ENC_INIT',['../group__EblParserFormat.html#ga9fe6ad99e196189ad07d99d6c905161a',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fenc_5fmac',['EBL_TAG_ID_ENC_MAC',['../group__EblParserFormat.html#gade7d88b45a1a4d5a28dd6154b64f323a',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fend',['EBL_TAG_ID_END',['../group__EblParserFormat.html#ga955a373b38135b45b796f1199568d0d9',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5feraseprog',['EBL_TAG_ID_ERASEPROG',['../group__EblParserFormat.html#ga126abf0c2a521b0d29e8d7561ce6c6ff',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fheader',['EBL_TAG_ID_HEADER',['../group__EblParserFormat.html#ga1821c0de679c8ce09d40fec72dd8031f',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fmetadata',['EBL_TAG_ID_METADATA',['../group__EblParserFormat.html#gac61fa5d9a79733865a6d64cf55f95bbd',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fprog',['EBL_TAG_ID_PROG',['../group__EblParserFormat.html#ga8dddb4f7be61eff95a3b45d9c5333147',1,'btl_ebl_format.h']]],
  ['ebl_5ftag_5fid_5fsignature_5fecdsa_5fp256',['EBL_TAG_ID_SIGNATURE_ECDSA_P256',['../group__EblParserFormat.html#gae8bfb6989b0c9d5da8bfcbe60a089443',1,'btl_ebl_format.h']]],
  ['ebl_5ftype_5fencryption_5faesccm',['EBL_TYPE_ENCRYPTION_AESCCM',['../group__EblParserFormat.html#gaca1359558b18137e8f258fe6c0c9530d',1,'btl_ebl_format.h']]],
  ['ebl_5ftype_5fencryption_5fnone',['EBL_TYPE_ENCRYPTION_NONE',['../group__EblParserFormat.html#ga5749fb626fefe3a9c8b7085b4557fd7e',1,'btl_ebl_format.h']]],
  ['eblbuffer_5ft',['EblBuffer_t',['../structEblBuffer__t.html',1,'']]],
  ['eblcrc',['eblCrc',['../structEblEnd__t.html#a052a7d9e438c35e6572c66c749cc8f11',1,'EblEnd_t']]],
  ['eblencryptionaesccmsignature_5ft',['EblEncryptionAesCcmSignature_t',['../structEblEncryptionAesCcmSignature__t.html',1,'']]],
  ['eblencryptiondata_5ft',['EblEncryptionData_t',['../structEblEncryptionData__t.html',1,'']]],
  ['eblencryptionheader_5ft',['EblEncryptionHeader_t',['../structEblEncryptionHeader__t.html',1,'']]],
  ['eblencryptioninitaesccm_5ft',['EblEncryptionInitAesCcm_t',['../structEblEncryptionInitAesCcm__t.html',1,'']]],
  ['eblend_5ft',['EblEnd_t',['../structEblEnd__t.html',1,'']]],
  ['eblheader_5ft',['EblHeader_t',['../structEblHeader__t.html',1,'']]],
  ['eblmac',['eblMac',['../structEblEncryptionAesCcmSignature__t.html#a21b04543fd1ff2bfe9a94b5d5c6a660a',1,'EblEncryptionAesCcmSignature_t']]],
  ['eblmetadata_5ft',['EblMetadata_t',['../structEblMetadata__t.html',1,'']]],
  ['ebl_20parser',['EBL Parser',['../group__EblParser.html',1,'']]],
  ['eblparsercontext_5ft',['EblParserContext_t',['../structEblParserContext__t.html',1,'']]],
  ['ebl_20format',['EBL Format',['../group__EblParserFormat.html',1,'']]],
  ['eblparserstate_5ft',['EblParserState_t',['../group__EblParser.html#ga7767157da0dffc9530af3da1f87b88a3',1,'btl_ebl_parser.h']]],
  ['eblparserstatedone',['EblParserStateDone',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a0c0f2b2e1f5b87bad47adc5b69a2acba',1,'btl_ebl_parser.h']]],
  ['eblparserstateencryptioncontainer',['EblParserStateEncryptionContainer',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3addbc4f88a9747e46d0a9fbe974d0f87f',1,'btl_ebl_parser.h']]],
  ['eblparserstateencryptionheader',['EblParserStateEncryptionHeader',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a985810ac96ec268e01ad4e939a076393',1,'btl_ebl_parser.h']]],
  ['eblparserstateencryptioninit',['EblParserStateEncryptionInit',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ab8599d1ca1c8477e5a974cabb07f7835',1,'btl_ebl_parser.h']]],
  ['eblparserstateencryptionmac',['EblParserStateEncryptionMac',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ae42a4b120a100bbfece57dfcb690b573',1,'btl_ebl_parser.h']]],
  ['eblparserstateeraseprog',['EblParserStateEraseProg',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a7a77dfdf146c3ebd73d2975ce42f75c3',1,'btl_ebl_parser.h']]],
  ['eblparserstateerror',['EblParserStateError',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3aee0cd5896467124e1e3c072e0f720e97',1,'btl_ebl_parser.h']]],
  ['eblparserstatefinalize',['EblParserStateFinalize',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a816d08790eaf7c4f8b7747561a40bff4',1,'btl_ebl_parser.h']]],
  ['eblparserstateheader',['EblParserStateHeader',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a5629e9a4519dcaae2fa38654ef6c8b24',1,'btl_ebl_parser.h']]],
  ['eblparserstateidle',['EblParserStateIdle',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a94a7dd46ab93e4e70b8cf96fcf6b7fa3',1,'btl_ebl_parser.h']]],
  ['eblparserstateinit',['EblParserStateInit',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a6ae86128d96bcff1eb0017d2bea71c1b',1,'btl_ebl_parser.h']]],
  ['eblparserstatemetadata',['EblParserStateMetadata',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3a744e406c55a3f170ac83bea3ecc803a5',1,'btl_ebl_parser.h']]],
  ['eblparserstateprog',['EblParserStateProg',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ae0cc235fb4b57d95c5ce90276c94a3bb',1,'btl_ebl_parser.h']]],
  ['eblparserstatesignature',['EblParserStateSignature',['../group__EblParser.html#gga7767157da0dffc9530af3da1f87b88a3ad058844f29e64ef44ce38ca43e1fdc17',1,'btl_ebl_parser.h']]],
  ['eblprog_5ft',['EblProg_t',['../structEblProg__t.html',1,'']]],
  ['eblsignatureecdsap256_5ft',['EblSignatureEcdsaP256_t',['../structEblSignatureEcdsaP256__t.html',1,'']]],
  ['ebltagheader_5ft',['EblTagHeader_t',['../structEblTagHeader__t.html',1,'']]],
  ['ecc_20library',['ECC Library',['../group__ECC.html',1,'']]],
  ['ecc_2eh',['ecc.h',['../ecc_8h.html',1,'']]],
  ['ecc_5fbigint_5fsize_5fin_5f32bit_5fwords',['ECC_BIGINT_SIZE_IN_32BIT_WORDS',['../group__ECC.html#ga36f016a5c876eeea1ccb4c4a57bcb372',1,'ecc.h']]],
  ['ecc_5fbigint_5fsize_5fin_5fbits',['ECC_BIGINT_SIZE_IN_BITS',['../group__ECC.html#ga74b13beb30f0eafaf0dd979ebd96e51b',1,'ecc.h']]],
  ['ecc_5fbigint_5fsize_5fin_5fbytes',['ECC_BIGINT_SIZE_IN_BYTES',['../group__ECC.html#ga2f5d40dc29d58de9bf86d534a2a953a9',1,'ecc.h']]],
  ['ecc_5fbigint_5ft',['ECC_BigInt_t',['../group__ECC.html#ga7473b9142bdec741ae4e2d2be8f703f4',1,'ecc.h']]],
  ['ecc_5fbiginttobytearray',['ECC_BigIntToByteArray',['../group__ECC.html#ga899570b76c2809c84295e5dd0c7dc342',1,'ecc.h']]],
  ['ecc_5fbiginttohex',['ECC_BigIntToHex',['../group__ECC.html#gac785c57fdc324b9feab818e3d0cf9225',1,'ecc.h']]],
  ['ecc_5fbytearraytobigint',['ECC_ByteArrayToBigInt',['../group__ECC.html#ga990ce75f282b6030721229cb79161290',1,'ecc.h']]],
  ['ecc_5fecdsa_5fverifysignaturep256',['ECC_ECDSA_VerifySignatureP256',['../group__ECC.html#ga277d9d7928b1a63a7512dfc9c52db5c4',1,'ecc.h']]],
  ['ecc_5fecdsasignature_5ft',['ECC_EcdsaSignature_t',['../structECC__EcdsaSignature__t.html',1,'']]],
  ['ecc_5fhextobigint',['ECC_HexToBigInt',['../group__ECC.html#gaf44e7fb772b55174dcf335c54f9514e2',1,'ecc.h']]],
  ['ecc_5fpoint_5ft',['ECC_Point_t',['../structECC__Point__t.html',1,'']]],
  ['ecc_5funsignedinttobigint',['ECC_UnsignedIntToBigInt',['../group__ECC.html#gaff60f872dde7ce6df9302856d075d2f8',1,'ecc.h']]],
  ['ecdsa',['ECDSA',['../group__ECDSA.html',1,'']]],
  ['ecode_5fbtl_5fecc_5fbase',['ECODE_BTL_ECC_BASE',['../group__ECC.html#ga214adb331ee9a956468991d56a6679ef',1,'ecc.h']]],
  ['ecode_5fbtl_5fecc_5finvalid_5fcurve_5fid',['ECODE_BTL_ECC_INVALID_CURVE_ID',['../group__ECC.html#gac60108c2f02c3b9e1ceefdf400558447',1,'ecc.h']]],
  ['ecode_5fbtl_5fecc_5fparam_5fout_5fof_5frange',['ECODE_BTL_ECC_PARAM_OUT_OF_RANGE',['../group__ECC.html#ga67397aa1447171ea03ccc91b4a26c257',1,'ecc.h']]],
  ['ecode_5fbtl_5fecc_5fsign_5ffail',['ECODE_BTL_ECC_SIGN_FAIL',['../group__ECC.html#ga43e8288b306599dfa887a43d01cecc89',1,'ecc.h']]],
  ['ecode_5fbtl_5fecc_5fsignature_5finvalid',['ECODE_BTL_ECC_SIGNATURE_INVALID',['../group__ECC.html#ga24c26c98804d1aef035a27e90809feb6',1,'ecc.h']]],
  ['encrypted',['encrypted',['../structEblParserContext__t.html#a9666eef81204490c61fc30745ec6b6d8',1,'EblParserContext_t']]],
  ['encryptedebldata',['encryptedEblData',['../structEblEncryptionData__t.html#a5fae98495d4e6eddd0f17ba0b9a86ff1',1,'EblEncryptionData_t']]],
  ['encryptiontype',['encryptionType',['../structEblEncryptionHeader__t.html#aa20e130cb1a843890d8716e1fe39007d',1,'EblEncryptionHeader_t']]],
  ['endofappspace',['endOfAppSpace',['../structSecondBootloaderTable__t.html#a34a34c8eb1c01e728442a0d5a647115a',1,'SecondBootloaderTable_t']]],
  ['erase',['erase',['../structBootloaderStorageFunctions.html#a86f964227919fe24051dae7aad6704db',1,'BootloaderStorageFunctions']]],
  ['errorcode',['errorCode',['../structBootloaderVerificationContext.html#abdbbae6c5898eedfd2f52b33284d40e1',1,'BootloaderVerificationContext']]],
  ['error_20codes',['Error Codes',['../group__ErrorCodes.html',1,'']]]
];
