var searchData=
[
  ['callbackcontext',['callbackContext',['../structBtlImageDataCallbacks__t.html#a8ebbe87d020519391dc27aad6a7b2d07',1,'BtlImageDataCallbacks_t']]],
  ['capabilities',['capabilities',['../structBootloaderInformation__t.html#a36c2f75904a45bdcf2077d7c729230fc',1,'BootloaderInformation_t::capabilities()'],['../structSecondBootloaderTable__t.html#a3aba3fb728f85325150612cb802ebac7',1,'SecondBootloaderTable_t::capabilities()'],['../structBootloaderStorageInformation__t.html#a9ef2cd56fc5d7b60a5a282294070b5e5',1,'BootloaderStorageInformation_t::capabilities()']]],
  ['capabilitiesmask',['capabilitiesMask',['../structBootloaderStorageImplementationInformation__t.html#a18a12a54fc8f2ec179155f9ade6b71da',1,'BootloaderStorageImplementationInformation_t']]],
  ['common_20application_20interface',['Common Application Interface',['../group__CommonInterface.html',1,'']]],
  ['counter',['counter',['../structBtlAesCtrContext__t.html#a4c7eab9c09cb2c7787ebe163cec62340',1,'BtlAesCtrContext_t']]],
  ['crc32',['crc32',['../structIndirectBootRecord__t.html#a12571ece8dcad60972740cb734c496f7',1,'IndirectBootRecord_t::crc32()'],['../group__CRC32.html',1,'(Global Namespace)']]],
  ['crc32afterupgrade',['crc32AfterUpgrade',['../structIndirectBootRecord__t.html#a41bf70c6f0ab237b85e294b8b7d0d787',1,'IndirectBootRecord_t']]],
  ['custom',['CUSTOM',['../group__StorageInterface.html#ggac5a52f72db78d655840177f0e034d37ca945d6010d321d9fe75cbba7b6f37f3b5',1,'btl_interface_storage.h']]],
  ['customer',['customer',['../structBootloaderVersion__t.html#af6ad2c8aaa09c507ff20fe0e4394ce43',1,'BootloaderVersion_t']]]
];
