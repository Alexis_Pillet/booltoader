var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvwxy",
  1: "befis",
  2: "be",
  3: "bdefrs",
  4: "abcdefghilmnoprstuvwxy",
  5: "be",
  6: "bes",
  7: "bceins",
  8: "s",
  9: "abcdefiprst",
  10: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

