var btl__interface__storage_8h =
[
    [ "BOOTLOADER_STORAGE_IMPL_INFO_VERSION", "group__StorageInterface.html#gaa3ec112096e9442dff6c12b3adf7b7ca", null ],
    [ "BOOTLOADER_STORAGE_IMPL_INFO_VERSION_MAJOR", "group__StorageInterface.html#ga1f4839ed2d057841b664d9b3a26f5246", null ],
    [ "BOOTLOADER_STORAGE_IMPL_INFO_VERSION_MAJOR_MASK", "group__StorageInterface.html#gac2d221e19a293930ec7b2e915c21708b", null ],
    [ "BOOTLOADER_STORAGE_IMPL_CAPABILITY_ERASE_SUPPORTED", "group__StorageInterface.html#ga329ca9f415fb1ba646d86eb67344f82a", null ],
    [ "BOOTLOADER_STORAGE_IMPL_CAPABILITY_PAGE_ERASE_REQUIRED", "group__StorageInterface.html#ga2c8c8994d789e65d91aa9c5e0eea7f94", null ],
    [ "BOOTLOADER_STORAGE_IMPL_CAPABILITY_BLOCKING_WRITE", "group__StorageInterface.html#ga1ec762d826e7358a9e099a0fec71fd09", null ],
    [ "BOOTLOADER_STORAGE_IMPL_CAPABILITY_BLOCKING_ERASE", "group__StorageInterface.html#gab1b14dc36ecc7926f556019332c02808", null ],
    [ "BootloaderVerificationContext_t", "group__StorageInterface.html#ga69f678f25c04b62199c0774bc3a4848f", null ],
    [ "BootloaderStorageFunctions_t", "group__StorageInterface.html#gaa89e144ac8860e6438fa05de6ef4339c", null ],
    [ "BootloaderStorageType_t", "group__StorageInterface.html#gac5a52f72db78d655840177f0e034d37c", [
      [ "SPIFLASH", "group__StorageInterface.html#ggac5a52f72db78d655840177f0e034d37caaf1113ff5007b46ad4e50ef301ed299b", null ],
      [ "INTERNAL_FLASH", "group__StorageInterface.html#ggac5a52f72db78d655840177f0e034d37ca5e341ba74bb0afd21095eabe0f8eba9b", null ],
      [ "CUSTOM", "group__StorageInterface.html#ggac5a52f72db78d655840177f0e034d37ca945d6010d321d9fe75cbba7b6f37f3b5", null ]
    ] ],
    [ "bootloader_getStorageInfo", "group__StorageInterface.html#gafc4cc80fcbb0bfea595915b11a6b5828", null ],
    [ "bootloader_getStorageSlotInfo", "group__StorageInterface.html#ga08d0906c70cd68f8119b8f11319cc5ff", null ],
    [ "bootloader_readStorage", "group__StorageInterface.html#ga622c6fec24b6fdb09da33e5d03f427ca", null ],
    [ "bootloader_writeStorage", "group__StorageInterface.html#ga4dc72a79771e16f51e9ebd3bb4ce1d76", null ],
    [ "bootloader_eraseStorageSlot", "group__StorageInterface.html#gae14880e1483f25bd67f1b96e5345862f", null ],
    [ "bootloader_setAppImageToBootload", "group__StorageInterface.html#ga0e3954e326b3c8c5a7c44d1b51316e85", null ],
    [ "bootloader_setBootloaderImageToBootload", "group__StorageInterface.html#ga980580ddfcbfe9593882f8f1e6e9bff1", null ],
    [ "bootloader_verifyImage", "group__StorageInterface.html#gacf8b4a14e28845d12edf1fb8baea4649", null ],
    [ "bootloader_storageIsBusy", "group__StorageInterface.html#ga60cb9344aa4453edf0bc73f18029d389", null ]
];