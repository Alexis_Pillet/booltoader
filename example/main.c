
#include "btl_interface.h"

#include "em_cmu.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_gpio.h"

#include "retargetserial.h"
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

int main(void)
{
  EMU_DCDCInit_TypeDef dcdcInit = EMU_DCDCINIT_DEFAULT;
  CMU_HFXOInit_TypeDef hfxoInit = CMU_HFXOINIT_DEFAULT;

  /* Chip errata */
  CHIP_Init();

  /* Init DCDC regulator and HFXO */
  EMU_DCDCInit(&dcdcInit);
  CMU_HFXOInit(&hfxoInit);

  /* Switch HFCLK to HFXO and disable HFRCO */
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
  CMU_OscillatorEnable(cmuOsc_HFRCO, false, false);

  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);

  /* Initialize USART and map LF to CRLF */
  RETARGET_SerialInit();
  RETARGET_SerialCrLf(1);

  /* Enable VCOM */
  GPIO_PinModeSet(BSP_BCC_ENABLE_PORT, BSP_BCC_ENABLE_PIN, gpioModePushPull, 1);

  printf("\nBootloader API demo\n");

  BootloaderInformation_t bootloaderInfo;
  bootloader_getInfo(&bootloaderInfo);
  printf("Bootloader type: ");
  if(bootloaderInfo.type == SL_BOOTLOADER) {
    printf("Silicon Labs Bootloader version %d.%d.%d (customer version %d)\n",
           bootloaderInfo.version.major,
           bootloaderInfo.version.minor,
           bootloaderInfo.version.patch,
           bootloaderInfo.version.customer);
    printf("Bootloader capabilities:\n");
    if (bootloaderInfo.capabilities & BOOTLOADER_CAPABILITY_EBL) {
      printf("  EBL parsing\n");
    }
    if (bootloaderInfo.capabilities & BOOTLOADER_CAPABILITY_SECURE_BOOT) {
      printf("  Secure boot\n");
    }
    if (bootloaderInfo.capabilities & BOOTLOADER_CAPABILITY_UPGRADE) {
      printf("  Self-upgrade\n");
    }
    if (bootloaderInfo.capabilities & BOOTLOADER_CAPABILITY_STORAGE) {
      printf("  Storage\n");
    }
    if (bootloaderInfo.capabilities & BOOTLOADER_CAPABILITY_COMMUNICATION) {
      printf("  Communication\n");
    }
  } else {
    printf("No Bootloader\n");
    printf("Exiting program.\n");
    return 0;
  }

  printf("Initializing bootloader...");
  if (bootloader_init() == BOOTLOADER_OK) {
    printf("success!\n");
  } else {
    printf("FAILED!\n");
  }

  printf("Erase storage slot 0...");
  if (bootloader_eraseStorageSlot(0) == BOOTLOADER_OK) {
    printf("success!\n");
  } else {
    printf("FAILED!\n");
  }

  printf("Write data to slot 0...");
  uint8_t writedata[64];
  for (int i = 0; i < 64; i++) {
    writedata[i] = i;
  }
  if (bootloader_writeStorage(0, 0, (uint32_t*)writedata, 64) == BOOTLOADER_OK) {
    printf("success!\n");
  } else {
    printf("FAILED!\n");
  }

  printf("Read data back from slot 0...");
  uint8_t readdata[64] = {0};
  if (bootloader_readStorage(0, 0, (uint32_t*)readdata, 64) == BOOTLOADER_OK) {
    if (memcmp(readdata, writedata, 64) == 0) {
      printf("success!\n");
    } else {
      printf("FAILED: memcmp\n");
    }
  } else {
    printf("FAILED: readStorage\n");
  }

  printf("Write data using raw API...");
  if (bootloader_writeRawStorage(0x40040, writedata, 64) == BOOTLOADER_OK) {
    printf("success!\n");
  } else {
    printf("FAILED!\n");
  }

  printf("Read data back from slot 0 using slot API...");
  for (int i = 0; i < 64; i++) {
    readdata[i] = 0;
  }
  if (bootloader_readStorage(0, 0x40, (uint32_t*)readdata, 64) == BOOTLOADER_OK) {
    if (memcmp(readdata, writedata, 64) == 0) {
      printf("success!\n");
    } else {
      printf("FAILED: memcmp\n");
    }
  } else {
    printf("FAILED: readStorage\n");
  }

  printf("Deinitializing bootloader...");
  if (bootloader_deinit() == BOOTLOADER_OK) {
    printf("success!\n");
  } else {
    printf("FAILED!\n");
  }

  while(1);
}