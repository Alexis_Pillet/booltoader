/***************************************************************************//**
 * @file btl_driver_delay.c
 * @brief Hardware driver layer for simple delay on EXX32.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include BTL_CONFIG_FILE
#include "btl_driver_delay.h"

static const uint32_t iterations_per_microsecond = 3;

void delay_microseconds(uint32_t usecs)
{
  volatile uint64_t iterations = iterations_per_microsecond * usecs;

  while(iterations--);
}