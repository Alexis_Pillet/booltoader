/***************************************************************************//**
 * @file btl_driver_delay.h
 * @brief Hardware driver layer for simple delay.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef BTL_DRIVER_DELAY_H
#define BTL_DRIVER_DELAY_H

/**
 * @addtogroup Driver
 * @brief Hardware drivers for bootloader
 * @{
 * @addtogroup Delay
 * @brief Basic delay functionality
 * @details Simple delay routines for use with plugins that require small delays.
 * @{
 */

/**
 * Delay for a number of microseconds
 * 
 * @param usecs Number of microseconds to delay
 */
void delay_microseconds(uint32_t usecs);

/**
 * @} // addtogroup Delay
 * @} // addtogroup Driver
 */

#endif // BTL_DRIVER_DELAY_H