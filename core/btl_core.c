/***************************************************************************//**
 * @file btl_core.c
 * @brief Core functionality for Silicon Labs bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include BTL_CONFIG_FILE

#include "btl_core.h"
#include <stdbool.h>

#include "plugin/debug/btl_debug.h"
#ifdef BOOTLOADER_SUPPORT_STORAGE
#include "plugin/storage/btl_storage.h"
#endif

int32_t btl_init(void)
{
  int32_t retval = BOOTLOADER_OK;

#ifdef BOOTLOADER_SUPPORT_STORAGE
  retval = storage_init();
  if (retval != BOOTLOADER_OK) {
    return retval;
  }
#endif

  return retval;
}

int32_t btl_deinit(void)
{
  int32_t retval = BOOTLOADER_OK;

#ifdef BOOTLOADER_SUPPORT_STORAGE
  retval = storage_shutdown();
  if (retval != BOOTLOADER_OK) {
    return retval;
  }
#endif

  return retval;  
}

/**
* This function is called when the bootloader fails, or is otherwise stuck.
* Reference implementation: blink PF5.
*/
void btl_failure(void)
{
  // Report failure over debug interface
  BTL_DEBUG_FAILURE();

  // Stall forever, in case debug implementation doesn't
  while(1);
}