/***************************************************************************//**
 * @file btl_bootload.c
 * @brief Bootloading functionality for the Silicon Labs bootloader
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include BTL_CONFIG_FILE

#include "btl_bootload.h"

// BTL_SBT table
#include "api/btl_interface.h"

// Security algorithms
#include "plugin/security/btl_security_sha256.h"
#include "plugin/security/btl_security_ecdsa.h"

// Parser
#include "plugin/parser/ebl/btl_ebl_parser.h"

// Storage (external & internal)
#include "plugin/storage/btl_storage.h"
#include "core/flash/btl_internal_flash.h"

// Debug
#include "plugin/debug/btl_debug.h"

// Get memcpy
#include <string.h>

// --------------------------------
// Local type declarations

typedef struct {
  /// Buffer for withholding the ECDSA secure boot signature
  uint8_t     signature[64];
  /// Cached value of the secure boot pointer
  uint32_t    signatureAddress;
  /// Flag to indicate this is the first data callback
  bool        firstPage;
} CallbackContext_t;

// --------------------------------
// Callback Functions

/***************************************************************************//**
 * Image data callback implementation
 *
 * @param address         Address (inside the raw image) the data starts at
 * @param data            Raw image data
 * @param length          Size in bytes of raw image data. Constrained to always
 *                        be a multiple of four.
 * @param mode            Mode with which to write the data (if relevant).
 * @param callbackContext A context variable defined by the implementation that
 *                        is implementing this callback. In this implementation,
 *                        it is a pointer to a @ref CallbackContext_t struct.
 ******************************************************************************/
void btl_bootloadImageDataCallback(uint32_t address, 
                                   uint8_t* data,
                                   size_t length,
                                   BtlWriteMode_t mode,
                                   void *callbackContext)
{
  // Check if addresses to write to are within writeable space
  if((address < (uint32_t)(BTL_SBT->appStart)) 
     || ((address + length) > (uint32_t)(BTL_SBT->endOfAppSpace))) {
    BTL_DEBUG_PRINT("Wr OOB 0x");
    BTL_DEBUG_PRINT_WORD_HEX(address);
    BTL_DEBUG_PRINTC('\n');
    return;
  }

  // Intercept and store vector 13's value (secure boot pointer)
  if((address <= (uint32_t)(BTL_SBT->appStart + 13))
     && ((address + length) >= (uint32_t)(BTL_SBT->appStart + 13))) {
    // Vector 13 is included in this call
    uint32_t offset = (uint32_t)(BTL_SBT->appStart + 13) - address;
    ((CallbackContext_t*)callbackContext)->signatureAddress = *((uint32_t*)(&(data[offset])));
  }

  // Check if we should erase a page
  if(mode == BtlWriteWithErase) {
    if (((CallbackContext_t*)callbackContext)->firstPage) {
      // Always erase the first page
      flash_erasePage((uint32_t*)(address & ~(FLASH_PAGE_SIZE - 1)));
      ((CallbackContext_t*)callbackContext)->firstPage = false;
    } else if (address % FLASH_PAGE_SIZE == 0) {
      // Erase the page if write starts at a page boundary
      flash_erasePage((uint32_t*)(address));
    }

    // Erase all pages that start inside the write range
    for (uint32_t pageAddress = (address + FLASH_PAGE_SIZE) & ~(FLASH_PAGE_SIZE - 1);
         pageAddress < (address + length);
         pageAddress += FLASH_PAGE_SIZE) {
      flash_erasePage((uint32_t *)pageAddress);
    }
  }

  BTL_DEBUG_PRINT("Wr ");
  BTL_DEBUG_PRINT_WORD_HEX(address);
  BTL_DEBUG_PRINT(" | ");
  BTL_DEBUG_PRINT_WORD_HEX(length);
  BTL_DEBUG_PRINTC('\n');

  flash_writeBuffer((uint32_t*) address, (const uint32_t*) data, length);
}

/***************************************************************************//**
 * Image metadata callback implementation
 *
 * @param offset          Offset of metadata (byte counter incrementing from 0)
 * @param data            Raw metadata
 * @param length          Size in bytes of raw metadata. Constrained to always
 *                        be a multiple of four.
 * @param callbackContext A context variable defined by the implementation that
 *                        is implementing this callback. In this implementation,
 *                        it is a pointer to a @ref CallbackContext_t struct.
 ******************************************************************************/
void btl_bootloadMetaDataCallback(uint32_t offset,
                                  uint8_t *data,
                                  size_t length,
                                  void *callbackContext)
{
  (void) offset;
  (void) data;
  (void) length;
  (void) callbackContext;
  BTL_DEBUG_PRINT("meta\n");
}

// --------------------------------
// Bootloading Functions

bool btl_bootloadSlot(uint32_t slotId, uint32_t version)
{
  (void)version;
  uint8_t readBuffer[BTL_STORAGE_READ_BUFFER_SIZE];
  uint32_t storageSlotOffset = 0;
  uint32_t retval = 0;

  CallbackContext_t callbackContext = {.signature = {0},
                                       .signatureAddress = BTL_APP_SPACE_BASE + 0x80,
                                       .firstPage = true};

  BtlImageDataCallbacks_t parseCb = {.callbackContext = (void*)&callbackContext, 
                                     .dataFunction = btl_bootloadImageDataCallback,
                                     .metaDataFunction = btl_bootloadMetaDataCallback};

  BtlImageProperties_t imageProps = {.imageVersion = 0,
                                     .imageContainsSsb = false,
                                     .imageContainsApp = false,
                                     .imageCompleted = false,
                                     .imageVerified = false};

  EblParserContext_t parserContext;

  BootloaderStorageSlot_t slot;
  storage_getSlotInfo(slotId, &slot);

  BTL_DEBUG_PRINT("BTL\n");

  // We'll be touching internal flash
  flash_init();

  btl_startParseImage(&parserContext);

  // Run through the image and flash it
  while((0 == retval) && (storageSlotOffset < slot.length)) {
    storage_readSlot(slotId, storageSlotOffset, (uint32_t *)readBuffer, BTL_STORAGE_READ_BUFFER_SIZE);
    retval = btl_parseImageData(&parserContext, &imageProps, readBuffer, BTL_STORAGE_READ_BUFFER_SIZE, &parseCb);
    storageSlotOffset += BTL_STORAGE_READ_BUFFER_SIZE;
  };

  if(!imageProps.imageCompleted) {
    BTL_DEBUG_PRINT("Image fail 0x");
    BTL_DEBUG_PRINT_WORD_HEX(retval);
    BTL_DEBUG_PRINTC('\n');
  } else {
    if(imageProps.imageVerified) {
      // Mark image as bootloaded
      storage_markBootloadComplete(-1, slotId);
    }
  }

  // Done with flashing to internal flash
  flash_deinit();

  return true;
}

bool btl_verifyApp(void)
{
  BtlSha256State_t shaState;

  uint32_t appSp = *(BTL_SBT->appStart);
  uint32_t appPc = *(BTL_SBT->appStart + 1);
  uint32_t appSignature = *(BTL_SBT->appStart + 13);

  // --------------------------------
  // Check sanity of initial SP, PC and signature pointer

  // Check that SP points to RAM
  if((appSp < RAM_MEM_BASE) || (appSp > (RAM_MEM_BASE + RAM_MEM_SIZE))) {
    return false;
  }

  // Check that PC points to application flash
  if( (appPc < (uint32_t)BTL_SBT->appStart) 
      || (appPc > (uint32_t)BTL_SBT->endOfAppSpace) ) {
    return false;
  }

  // Check that signature is in application flash
  if( (appSignature < (uint32_t)BTL_SBT->appStart) 
      || (appSignature > (uint32_t)BTL_SBT->endOfAppSpace) ) {
    return false;
  }

  // SHA-256 of the entire application (appStart until signature)
  btl_initSha256(&shaState);
  btl_updateSha256(&shaState, (const uint8_t*) BTL_SBT->appStart, appSignature - (uint32_t)BTL_SBT->appStart);
  btl_finalizeSha256(&shaState);
  if(btl_verifyEcdsaP256r1(shaState.sha, (uint8_t*)appSignature, ((uint8_t*)appSignature) + 32)) {
    return true;
  } else {
    BTL_DEBUG_PRINT("Sig fail\n");
    return false;
  }
}

bool btl_versionAllowed(uint32_t version)
{
  (void)version;
  return true;
}
