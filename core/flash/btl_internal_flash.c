/***************************************************************************//**
 * @file btl_internal_flash.c
 * @brief Abstraction of internal flash read and write routines.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include "core/flash/btl_internal_flash.h"
#include "em_device.h"
#include "em_msc.h"
#include "em_cmu.h"

void flash_init(void)
{
  // Init the MSC
  MSC_Init();
}

void flash_deinit(void)
{
  // De-init MSC
  MSC_Deinit();
}

bool flash_erasePage(uint32_t *address)
{
  MSC_Status_TypeDef retval = MSC_ErasePage(address);
  return retval == mscReturnOk;
}

bool flash_writeWord(uint32_t *address, uint32_t data)
{
  MSC_Status_TypeDef retval = MSC_WriteWordFast(address, &data, 4);
  return retval == mscReturnOk;
}

bool flash_writeBuffer(uint32_t       *address,
                       const uint32_t *data,
                       size_t         length)
{
  MSC_Status_TypeDef retval = MSC_WriteWordFast(address, data, length);
  return retval == mscReturnOk;
}