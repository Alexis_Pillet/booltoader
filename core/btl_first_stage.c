/***************************************************************************//**
 * @file btl_first_stage.c
 * @brief Main file for First Stage Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include BTL_CONFIG_FILE
#include "api/btl_interface.h"

#include "core/btl_util.h"

#include "plugin/debug/btl_debug.h"

#include "em_device.h"

int main(void)
{
  BTL_DEBUG_INIT();
  BTL_DEBUG_PRINT("First stage entered\n");
}

const FirstBootloaderTable_t firstBootloaderTable SL_ATTRIBUTE_SECTION(".boot_table") = {
  .type = BOOTLOADER_MAGIC_FIRST_STAGE,
  .version = {
    BOOTLOADER_VERSION_FIRST_STAGE_MAJOR,
    BOOTLOADER_VERSION_FIRST_STAGE_MINOR,
    BOOTLOADER_VERSION_FIRST_STAGE_PATCH,
    BOOTLOADER_VERSION_FIRST_STAGE_CUSTOMER
  },
  .secondStageStart = (uint32_t *)BTL_SECOND_STAGE_BASE,
  .upgradeLocationStart = (uint32_t *)BTL_UPGRADE_LOCATION_BASE
};

/*
* This function gets executed before ANYTHING got initialized.
* So, no using global variables here, and avoid stack usage.
*/
void SystemInit2(void) {
  bool enterSecondStage = true;
  // Check if we came from EM4. If so, we need to short-circuit
  //   into booting the next stage.
  if (RMU->RSTCAUSE == RMU_RSTCAUSE_EM4RST) {
    // Enter second stage
  }
  // TODO: Add check for second stage upgrade

  if (enterSecondStage) {
      BareBootTable_t * secondStage = (BareBootTable_t *)(BTL_FBT->secondStageStart);

    /* Don't change vector table for second stage; should use first stage reset vector
     * TODO: ??? */
//    SCB->VTOR = ((uint32_t)(secondStage));

    // Move SP to second stage
    __set_MSP((uint32_t)secondStage->stackTop);
    __set_PSP((uint32_t)secondStage->stackTop);

    // Move PC to second stage
    secondStage->resetVector();

    // Should never return here
  }

  // Enter first stage to perform second stage upgrade
}
