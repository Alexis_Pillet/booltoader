/***************************************************************************//**
 * @file btl_bootload.h
 * @brief Bootloading functionality for the Silicon Labs bootloader
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#ifndef BTL_BOOTLOADING_H
#define BTL_BOOTLOADING_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

/***************************************************************************//**
 * @addtogroup Core Bootloader Core
 * @{
 * @addtogroup Bootload
 * @brief Methods to verify and bootload application images
 * @details 
 * @{
 ******************************************************************************/

/**
* This function verifies the app that currently resides in flash space.
* 
* @returns True if the image signature is valid
*/
bool btl_verifyApp(void);

/**
 * Bootload an image contained in a slot
 *
 * @param slotId Slot ID to bootload from
 * @param version Cached version number of the image contained in the slot
 *   (used for downgrade prevention)
 * @returns True if operation succeeded
 *
 * @note Calling this function assumes the image located in slotId has been
 *   validated first!
 */
bool btl_bootloadSlot(uint32_t slotId, uint32_t version);

/**
 * Check whether a given version number is allowed for a new image
 *
 * @param version The version number to check
 *
 * @return True if the new version is allowed
 */
bool btl_versionAllowed(uint32_t version);

/** @} addtogroup bootload */
/** @} addtogroup core */

#endif