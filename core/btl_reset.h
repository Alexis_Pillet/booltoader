/***************************************************************************//**
 * @file btl_reset.h
 * @brief Reset cause signalling for the Silicon Labs bootloader
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#ifndef BTL_RESET_H
#define BTL_RESET_H

/***************************************************************************//**
 * @addtogroup Core Bootloader Core
 * @{
 * @addtogroup Reset
 * @brief Methods to reset from the bootloader to the app
 * @details 
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * Reset from the bootloader with a reset cause
 *
 * @param resetReason A reset reason as defined in
 *                    [the bootloader interface](@ref ResetInterface)
 ******************************************************************************/
void reset_resetWithReason(uint16_t resetReason);

/** @} addtogroup reset */

#endif // BTL_RESET_H