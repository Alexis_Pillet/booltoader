/***************************************************************************//**
 * @file btl_reset.c
 * @brief Reset cause signalling for the Silicon Labs bootloader
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "em_device.h"
#include "api/btl_reset_info.h"

void reset_resetWithReason(uint16_t resetReason)
{
  BootloaderResetCause_t *cause = (BootloaderResetCause_t *) (RAM_MEM_BASE);

  cause->reason = resetReason;
  cause->signature = BOOTLOADER_RESET_SIGNATURE_VALID;

  // Trigger a software system reset
  RMU->CTRL = (RMU->CTRL & ~_RMU_CTRL_SYSRMODE_MASK) | RMU_CTRL_SYSRMODE_FULL;
  NVIC_SystemReset();
}
