/***************************************************************************//**
 * @file btl_second_stage.c
 * @brief Main file for Second Stage Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include BTL_CONFIG_FILE
#include "api/btl_interface.h"

#include "core/btl_util.h"
#include "core/btl_core.h"
#include "core/btl_reset.h"
#include "core/btl_bootload.h"

#include "plugin/debug/btl_debug.h"
#include "plugin/storage/btl_storage.h"

#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_chip.h"

// --------------------------------
// Local function declarations
bool btl_checkResetReason(void);
bool btl_enterBootloader(void);

// Main (second stage) bootloader implementation
#define IBR_SIZE 40

int main(void)
{                        
  uint32_t address = 0;
  uint8_t i = 0;
  
  CHIP_Init();

  btl_init();
  
  GPIO_PinModeSet(gpioPortB, 11, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortA, 4, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortF, 7, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortB, 12, gpioModePushPull, 1);
  GPIO_PinModeSet(gpioPortA, 2, gpioModePushPull, 1);
  GPIO_PinModeSet(gpioPortF, 6, gpioModePushPull, 1);
    
  for( i=0; i<=15; i++ )
  {
    address = ( 0x00010000 * i );
    if( BOOTLOADER_OK != storage_eraseRaw( address, 0x10000 ) )
    { 
      __ASM volatile("BKPT #01");
      break; 
    }
    GPIO_PinOutToggle(gpioPortB, 11);
    GPIO_PinOutToggle(gpioPortA, 4);
    GPIO_PinOutToggle(gpioPortF, 7);
  }
  
  while( 1 )
  {
    GPIO_PinOutSet(gpioPortB, 11);
    GPIO_PinOutSet(gpioPortA, 4);
    GPIO_PinOutSet(gpioPortF, 7);
    
    for (volatile int i = 0; i < 1000000; i++);
    GPIO_PinOutToggle(gpioPortB, 12);
    GPIO_PinOutToggle(gpioPortA, 2);
    GPIO_PinOutToggle(gpioPortF, 6);
  }
}

extern const BootloaderStorageFunctions_t storageFunctions;

const SecondBootloaderTable_t secondBootloaderTable SL_ATTRIBUTE_SECTION(".boot_table") = {
  .type = BOOTLOADER_MAGIC_SECOND_STAGE,
  .version = {
    BOOTLOADER_VERSION_SECOND_STAGE_MAJOR,
    BOOTLOADER_VERSION_SECOND_STAGE_MINOR,
    BOOTLOADER_VERSION_SECOND_STAGE_PATCH,
    BOOTLOADER_VERSION_SECOND_STAGE_CUSTOMER
  },
  .size = BTL_SECOND_STAGE_SIZE,
  .appStart = (void *)(BTL_APP_SPACE_BASE),
  .endOfAppSpace = (void *)(BTL_APP_SPACE_BASE + BTL_APP_SPACE_SIZE),
  .capabilities = (0
                   | BOOTLOADER_CAPABILITY_EBL
                   | BOOTLOADER_CAPABILITY_EBL_SIGNATURE
                   | BOOTLOADER_CAPABILITY_EBL_ENCRYPTION
                   | BOOTLOADER_CAPABILITY_SECURE_BOOT
#ifdef BOOTLOADER_SUPPORT_STORAGE
                   | BOOTLOADER_CAPABILITY_STORAGE
#endif
#ifdef BOOTLOADER_SUPPORT_COMMUNICATION
                   | BOOTLOADER_CAPABILITY_COMMUNICATION
#endif
                   ),
  .init = &btl_init,
  .deinit = &btl_deinit,

  .storage = &storageFunctions
};

/**
* This function gets executed before ANYTHING got initialized.
* So, no using global variables here!
*/
void SystemInit2(void) 
{
//  // Initialize debug before first debug print
//  BTL_DEBUG_INIT();
//
//  // Assumption: We should enter the app
//  bool enterApp = true;
//  // Assumption: The app should be verified
//  bool verifyApp = true;
//
//  // Check if we came from EM4. If any other bit than the EM4 bit it set, we
//  // can't know whether this was really an EM4 reset, and we need to do further
//  // checking.
//  if(RMU->RSTCAUSE == RMU_RSTCAUSE_EM4RST) {
//    // We came from EM4, app doesn't need to be verified
//    verifyApp = false;
//  } else if (btl_enterBootloader()) {
//    // We want to enter the bootloader, app doesn't need to be verified
//    enterApp = false;
//    verifyApp = false;
//  }
//  
//  // App should be verified
//  if (verifyApp) {
//    // If app verification fails, enter bootloader instead
//    enterApp = btl_verifyApp();
//  }
//
//  if (enterApp) {
//    BareBootTable_t * app = (BareBootTable_t *)(BTL_SBT->appStart);
//
//    // Point to application vector table
//    SCB->VTOR = ((uint32_t)(app));
//
//    // Move SP to app
//    __set_MSP((uint32_t)app->stackTop);
//    __set_PSP((uint32_t)app->stackTop);
//
//    // Move PC to app
//    app->resetVector();
//
//    // Should never return here
//  }

  // Enter bootloader
}


/**
 * Check whether we should enter the bootloader
 *
 * @return True if the bootloader should be entered
 */
bool btl_enterBootloader(void) {
  if (btl_checkResetReason()) {
    return true;
  }

  // TODO: Add functionality for forcing the bootloader by pressing gpio

  return false;
}

/**
 * Check the reset reason left for us.
 */
bool btl_checkResetReason(void)
{
  BootloaderResetCause_t* resetCause = (BootloaderResetCause_t*) (RAM_MEM_BASE);

  BTL_DEBUG_PRINT("Rstc: 0x");
  BTL_DEBUG_PRINT_SHORT_HEX(resetCause->reason);
  BTL_DEBUG_PRINTC('\n');

  if (RMU->RSTCAUSE & RMU_RSTCAUSE_SYSREQRST) {
    // Check if we were asked to run the bootloader...
    switch (resetCause->signature) {
      case BOOTLOADER_RESET_SIGNATURE_VALID:
        switch (resetCause->reason) {
          case BOOTLOADER_RESET_REASON_BOOTLOAD:
          case BOOTLOADER_RESET_REASON_FORCE:
            // Asked to go into bootload mode
            return true;
          default:
            break;
        }
        break;
      default:
        // Not asked to boot into bootloader
        break;
    }
  }

  return false;
}
