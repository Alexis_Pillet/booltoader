/***************************************************************************//**
 * @file btl_main.c
 * @brief Main file for Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include BTL_CONFIG_FILE
#include "api/btl_interface.h"

#include "core/btl_util.h"
#include "core/btl_core.h"
#include "core/btl_reset.h"
#include "core/btl_bootload.h"

#include "plugin/debug/btl_debug.h"
#include "plugin/storage/btl_storage.h"

#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_chip.h"

// --------------------------------
// Local function declarations
bool btl_checkResetReason(void);
bool btl_enterBootloader(void);

void btl_led_blink( int time, unsigned char led, char blink )
{      
  int i;
  char j; 
  
  for(j = 0; j < blink; j++ )
  {
    for (i = 0; i <= time; i++);
    if( ( led & BTL_GREEN_LEDR ) == BTL_GREEN_LEDR )
        GPIO_PinOutToggle( BTL_GREEN_LEDR_PORT, BTL_GREEN_LEDR_PIN );
    
    if( ( led & BTL_RED_LEDR ) == BTL_RED_LEDR )
        GPIO_PinOutToggle( BTL_RED_LEDR_PORT, BTL_RED_LEDR_PIN );
    
    if( ( led & BTL_GREEN_LEDM ) == BTL_GREEN_LEDM )
        GPIO_PinOutToggle( BTL_GREEN_LEDM_PORT, BTL_GREEN_LEDM_PIN );
    
    if( ( led & BTL_RED_LEDM ) == BTL_RED_LEDM )
        GPIO_PinOutToggle( BTL_RED_LEDM_PORT, BTL_RED_LEDM_PIN );
  }
}


// Main (second stage) bootloader implementation
int main(void)
{
  CHIP_Init();
  /*
  // Enabling HFXO will add a hefty code size penalty (~1k)!
  CMU_HFXOInit_TypeDef hfxoInit = CMU_HFXOINIT_DEFAULT;
  CMU_HFXOInit(&hfxoInit);
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
  CMU_OscillatorEnable(cmuOsc_HFRCO, false, false);
  */
  BTL_DEBUG_PRINT("BTL entry\n");

  btl_init();
  
  GPIO_PinModeSet(BTL_GREEN_LEDR_PORT, BTL_GREEN_LEDR_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(BTL_RED_LEDR_PORT, BTL_RED_LEDR_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(BTL_GREEN_LEDM_PORT, BTL_GREEN_LEDM_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(BTL_RED_LEDM_PORT, BTL_RED_LEDM_PIN, gpioModePushPull, 1);

  BootloadInfo_t btlInfo;
  bool bootload = storage_getImageToBootload(&btlInfo);
  BTL_DEBUG_PRINT("Slot: ");
  BTL_DEBUG_PRINT_WORD_HEX(btlInfo.appSlotId);
  BTL_DEBUG_PRINTC('\n');

  if(bootload && (btlInfo.appSlotId != -1)) {
    
    
    // Set led
    btl_led_blink( 0, ( BTL_RED_LEDR | BTL_RED_LEDM ), 1 );
      
    // Get info about the image marked for bootloading
    BootloaderVerificationContext_t verificationContext;
    storage_initVerifySlot(btlInfo.appSlotId, &verificationContext, sizeof(BootloaderVerificationContext_t));

    int32_t retval = BOOTLOADER_ERROR_VERIFICATION_CONTINUE;
    do {
      retval = storage_continueVerifySlot(&verificationContext);                
    } while (retval == BOOTLOADER_ERROR_VERIFICATION_CONTINUE);
    
    // Clear led
    btl_led_blink( 0, ( BTL_RED_LEDR | BTL_RED_LEDM ), 1 );
    
    if (retval == BOOTLOADER_ERROR_VERIFICATION_SUCCESS) {
      // Blink 3 times to signify image OK, and write to the internal flash
      btl_led_blink( 1000000, ( BTL_GREEN_LEDR | BTL_GREEN_LEDM ), 6 );
        
      // Set led
      btl_led_blink( 0, ( BTL_RED_LEDR | BTL_RED_LEDM ), 1 );
    
      // Image has been validated, so bootload.
      if(!btl_bootloadSlot(verificationContext.slotId,
                           verificationContext.imageProperties.imageVersion)) {
        BTL_DEBUG_PRINT("Btl fail\n");   
        
        // Clear led
        btl_led_blink( 0, ( BTL_RED_LEDR | BTL_RED_LEDM ), 1 );
        
        // Blink 5 times to signify that the bootload/write failed
        btl_led_blink( 500000, ( BTL_RED_LEDR | BTL_RED_LEDM ), 10 );
        
        reset_resetWithReason(BOOTLOADER_RESET_REASON_BADIMAGE);
      }      
      // Clear led
      btl_led_blink( 0, ( BTL_RED_LEDR | BTL_RED_LEDM ), 1 );
      
      // Blink 3 times to signify loading OK
      btl_led_blink( 1000000, ( BTL_GREEN_LEDR | BTL_GREEN_LEDM ), 6 );
    } else {
      BTL_DEBUG_PRINT("Verify fail\n");
      
        // Blinks 5 times to signify that the image is KO
      btl_led_blink( 1000000, ( BTL_RED_LEDR | BTL_RED_LEDM ), 8 );
        
      reset_resetWithReason(BOOTLOADER_RESET_REASON_BADIMAGE);
    }
  }  
  // Blink 3 times to signify it will try boot to internal application    
  btl_led_blink( 1000000, ( BTL_RED_LEDR | BTL_RED_LEDM ), 6 );
  
  reset_resetWithReason(BOOTLOADER_RESET_REASON_GO);
}

extern const BootloaderStorageFunctions_t storageFunctions;

const SecondBootloaderTable_t secondBootloaderTable SL_ATTRIBUTE_SECTION(".boot_table") = {
  .type = BOOTLOADER_MAGIC_SECOND_STAGE,
  .version = {
    BOOTLOADER_VERSION_SECOND_STAGE_MAJOR,
    BOOTLOADER_VERSION_SECOND_STAGE_MINOR,
    BOOTLOADER_VERSION_SECOND_STAGE_PATCH,
    BOOTLOADER_VERSION_SECOND_STAGE_CUSTOMER
  },
  .size = BTL_SECOND_STAGE_SIZE,
  .appStart = (void *)(BTL_APP_SPACE_BASE),
  .endOfAppSpace = (void *)(BTL_APP_SPACE_BASE + BTL_APP_SPACE_SIZE),
  .capabilities = (0
                   | BOOTLOADER_CAPABILITY_EBL
                   | BOOTLOADER_CAPABILITY_EBL_SIGNATURE
                   | BOOTLOADER_CAPABILITY_EBL_ENCRYPTION
                   | BOOTLOADER_CAPABILITY_SECURE_BOOT
#ifdef BOOTLOADER_SUPPORT_STORAGE
                   | BOOTLOADER_CAPABILITY_STORAGE
#endif
#ifdef BOOTLOADER_SUPPORT_COMMUNICATION
                   | BOOTLOADER_CAPABILITY_COMMUNICATION
#endif
                   ),
  .init = &btl_init,
  .deinit = &btl_deinit,

  .storage = &storageFunctions
};

/**
* This function gets executed before ANYTHING got initialized.
* So, no using global variables here!
*/
void SystemInit2(void) 
{
  // Initialize debug before first debug print
  BTL_DEBUG_INIT();

  // Assumption: We should enter the app
  bool enterApp = true;
  // Assumption: The app should be verified
  bool verifyApp = true;

  // Check if we came from EM4. If any other bit than the EM4 bit it set, we
  // can't know whether this was really an EM4 reset, and we need to do further
  // checking.
  if(RMU->RSTCAUSE == RMU_RSTCAUSE_EM4RST) {
    // We came from EM4, app doesn't need to be verified
    verifyApp = false;
  } else if (btl_enterBootloader()) {
    // We want to enter the bootloader, app doesn't need to be verified
    enterApp = false;
    verifyApp = false;
  }
  
  // App should be verified
  if (verifyApp) {
    // If app verification fails, enter bootloader instead
    enterApp = btl_verifyApp();
  }

  if (enterApp) {
    
    BareBootTable_t * app = (BareBootTable_t *)(BTL_SBT->appStart);

    // Point to application vector table
    SCB->VTOR = ((uint32_t)(app));

    // Move SP to app
    __set_MSP((uint32_t)app->stackTop);
    __set_PSP((uint32_t)app->stackTop);

    // Move PC to app
    app->resetVector();

    // Should never return here
  }

  // Enter bootloader
}


/**
 * Check whether we should enter the bootloader
 *
 * @return True if the bootloader should be entered
 */
bool btl_enterBootloader(void) {
  if (btl_checkResetReason()) {
    return true;
  }

  // TODO: Add functionality for forcing the bootloader by pressing gpio

  return false;
}

/**
 * Check the reset reason left for us.
 */
bool btl_checkResetReason(void)
{
  BootloaderResetCause_t* resetCause = (BootloaderResetCause_t*) (RAM_MEM_BASE);

  BTL_DEBUG_PRINT("Rstc: 0x");
  BTL_DEBUG_PRINT_SHORT_HEX(resetCause->reason);
  BTL_DEBUG_PRINTC('\n');

  if (RMU->RSTCAUSE & RMU_RSTCAUSE_SYSREQRST) {
    // Check if we were asked to run the bootloader...
    switch (resetCause->signature) {
      case BOOTLOADER_RESET_SIGNATURE_VALID:
        switch (resetCause->reason) {
          case BOOTLOADER_RESET_REASON_BOOTLOAD:
          case BOOTLOADER_RESET_REASON_FORCE:
            // Asked to go into bootload mode
            return true;
          default:
            break;
        }
        break;
      default:
        // Not asked to boot into bootloader
        break;
    }
  }

  return false;
}
