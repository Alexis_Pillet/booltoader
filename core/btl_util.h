/***************************************************************************//**
 * @file btl_util.h
 * @brief Bootloader utilities.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef BTL_UTIL_H
#define BTL_UTIL_H

#if defined(__CC_ARM)
/** MDK-ARM compiler: Macro for handling section placement */
#define SL_ATTRIBUTE_SECTION(X) __attribute__ ((section(X)))
#define SL_INLINE_ASM(x) __ASM(x)
#elif defined(__ICCARM__)
/** IAR Embedded Workbench: Macro for handling section placement */
#define SL_ATTRIBUTE_SECTION(X) @ X
#define SL_INLINE_ASM(x) __asm volatile(x)
#elif defined(__GNUC__)
/** GCC: Macro for handling section placement */
#define SL_ATTRIBUTE_SECTION(X) __attribute__ ((section(X)))
#define SL_INLINE_ASM(x) __ASM(x)
#else
#error "Unsupported compiler. No SL_ATTRIBUTE_SECTION nor SL_INLINE_ASM macro available."
#endif

#endif