/**
 * @file btl_interface_storage.c
 * @brief Application interface to the storage plugin of the bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include BTL_CONFIG_FILE
#include "btl_interface.h"

#ifdef BOOTLOADER_SUPPORT_STORAGE

// -----------------------------------------------------------------------------
// Functions

void bootloader_getStorageInfo(BootloaderStorageInformation_t *info)
{
  BTL_SBT->storage->getInfo(info);
}

int32_t bootloader_getStorageSlotInfo(uint32_t slotId,
                                   BootloaderStorageSlot_t *slot)
{
  return BTL_SBT->storage->getSlotInfo(slotId, slot);
}

int32_t bootloader_readStorage(uint32_t slotId,
                            uint32_t offset,
                            uint32_t *buffer,
                            size_t length)
{
  return BTL_SBT->storage->read(slotId, offset, buffer, length);
}


int32_t bootloader_writeStorage(uint32_t slotId,
                             uint32_t offset,
                             uint32_t *buffer,
                             size_t length)
{
  return BTL_SBT->storage->write(slotId, offset, buffer, length);
}


int32_t bootloader_eraseStorageSlot(uint32_t slotId)
{
  return BTL_SBT->storage->erase(slotId);
}


int32_t bootloader_setAppImageToBootload(int slotId)
{
  return BTL_SBT->storage->setAppImageToBootload(slotId);
}


int32_t bootloader_setBootloaderImageToBootload(int bootloaderSlotId,
                                             int appSlotId)
{
  return BTL_SBT->storage->setBootloaderImageToBootload(bootloaderSlotId, appSlotId);
}

int32_t bootloader_verifyImage(uint32_t slotId)
{
  uint8_t context[BTL_STORAGE_VERIFICATION_CONTEXT_SIZE];
  int32_t retval;

  // Check that the bootloader has image verification capability
  if (BTL_SBT->storage == NULL) {
    return BOOTLOADER_ERROR_VERIFICATION_STORAGE;
  }

  retval = BTL_SBT->storage->initVerifyImage(slotId,
                                             (BootloaderVerificationContext_t *)context,
                                             BTL_STORAGE_VERIFICATION_CONTEXT_SIZE);
  if (retval != BOOTLOADER_OK) {
    return retval;
  }

  do {
    retval = BTL_SBT->storage->verifyImage((BootloaderVerificationContext_t *)context);
  } while (retval == BOOTLOADER_ERROR_VERIFICATION_CONTINUE);

  if (retval == BOOTLOADER_ERROR_VERIFICATION_SUCCESS) {
    return BOOTLOADER_OK;
  } else {
    return retval;
  }
}

bool bootloader_storageIsBusy(void)
{
  return BTL_SBT->storage->isBusy();
}

#ifdef BOOTLOADER_SUPPORT_RAW_STORAGE
int32_t bootloader_readRawStorage(uint32_t address,
                               uint8_t *buffer, 
                               size_t length)
{
  return BTL_SBT->storage->readRaw(address, buffer, length);
}

int32_t bootloader_writeRawStorage(uint32_t address,
                                uint8_t *buffer,
                                size_t length)
{
  return BTL_SBT->storage->writeRaw(address, buffer, length);
}

int32_t bootloader_eraseRawStorage(uint32_t address,
                                size_t length)
{
  return BTL_SBT->storage->eraseRaw(address, length);
}
#endif // BOOTLOADER_SUPPORT_RAW_STORAGE

#endif // BOOTLOADER_SUPPORT_STORAGE
