/***************************************************************************//**
 * @file btl_interface.h
 * @brief Application interface to the bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#ifndef BTL_INTERFACE_H
#define BTL_INTERFACE_H

#include BTL_CONFIG_FILE
#include "btl_reset_info.h"

// Include plugin-specific interfaces
#include "btl_interface_storage.h"

/***************************************************************************//**
 * @addtogroup Interface Application Interface
 * @brief Application interface to the bootloader
 * @details The application interface consists of functions that can be included
 *          into the customer application, and will communicate with the
 *          bootloader through the @ref SecondBootloaderTable_t. This table
 *          contains function pointers into the bootloader.
 *          Using the wrapper functions is preferred over accessing the
 *          bootloader table directly.
 * @{
 * @addtogroup CommonInterface Common Application Interface
 * @brief Generic application interface available on all versions of the
 *        bootloader, independently of which plugins are present.
 * @details
 * @{
 ******************************************************************************/

// --------------------------------
// Bootloader information typedefs

/// Type of bootloader
typedef enum {
  /// No bootloader present.
  NONE,
  /// Bootloader is a Silicon Labs bootloader.
  SL_BOOTLOADER
} BootloaderType_t;

/// Bootloader version
typedef struct {
  /// Major version number
  uint8_t major;
  /// Minor version number
  uint8_t minor;
  /// Patch number
  uint8_t patch;
  /// Customer-configurable version number
  uint8_t customer;
} BootloaderVersion_t;

/// Information about the current bootloader
typedef struct {
  /// The type of bootloader.
  BootloaderType_t type;
  /// Version number of the bootloader
  BootloaderVersion_t version;
  /// Capability mask for the bootloader.
  uint32_t capabilities;
} BootloaderInformation_t;

/// Bare boot table. Contains the first 7 words of the vector table.
typedef struct {
  /// Pointer to top of stack
  uint32_t *stackTop;
  /// Pointer to reset vector
  void     (*resetVector)(void);
  /// Pointers to fault handlers: NMI, HardFault, MemManageFault, BusFault, UsageFault
  uint32_t *faultVectors[5];
} BareBootTable_t;

/// Address table for the First Stage Bootloader
typedef struct {
  /// Magic word indicating First Stage Bootloader. See @ref BOOTLOADER_MAGIC_FIRST_STAGE
  uint32_t type;
  /// Version number of the First Stage Bootloader
  BootloaderVersion_t version;
  /// Start address of the Second Stage Bootloader
  uint32_t *secondStageStart;
  /// Location of the Second Stage Bootloader upgrade image
  uint32_t *upgradeLocationStart;
} FirstBootloaderTable_t;

/// Address table for the Second Stage Bootloader
typedef struct {
  /// Magic word indicating Second Stage Bootloader. See @ref BOOTLOADER_MAGIC_SECOND_STAGE
  uint32_t type;
  /// Version number of the Second Stage Bootloader
  BootloaderVersion_t version;
  /// Size of the Second Stage Bootloader
  uint32_t size;
  /// Start address of the application
  uint32_t *appStart;
  /// End address of the allocated application space
  uint32_t *endOfAppSpace;
  /// Capabilities of the bootloader
  uint32_t capabilities;
  // ------------------------------
  /// Initialize bootloader for use from application
  int32_t (*init)(void);
  /// Deinitialize bootloader after use from application
  int32_t (*deinit)(void);
  // ------------------------------
  /// Function table for storage plugin
  const BootloaderStorageFunctions_t *storage;
} SecondBootloaderTable_t;

// --------------------------------
// Error codes

/**
 * @addtogroup ErrorCodes Error Codes
 * @brief Bootloader error codes
 * @details
 * @{
 */

/// No error
#define BOOTLOADER_OK                               0

/// Initialization errors
#define BOOTLOADER_ERROR_INIT_BASE                  0x0100
/// Storage initialization error
#define BOOTLOADER_ERROR_INIT_STORAGE               (BOOTLOADER_ERROR_INIT_BASE | 0x01)

/// Image verification errors
#define BOOTLOADER_ERROR_VERIFICATION_BASE          0x0200
/// @brief Verification not complete, continue calling the
/// verification function
#define BOOTLOADER_ERROR_VERIFICATION_CONTINUE      (BOOTLOADER_ERROR_VERIFICATION_BASE | 0x01)
/// Verification failed
#define BOOTLOADER_ERROR_VERIFICATION_FAILED        (BOOTLOADER_ERROR_VERIFICATION_BASE | 0x02)
/// Verification successfully completed. Image is valid.
#define BOOTLOADER_ERROR_VERIFICATION_SUCCESS       (BOOTLOADER_ERROR_VERIFICATION_BASE | 0x03)
/// Bootloader has no storage, and cannot verify images.
#define BOOTLOADER_ERROR_VERIFICATION_STORAGE       (BOOTLOADER_ERROR_VERIFICATION_BASE | 0x04)
/// Verification context incompatible with verification function
#define BOOTLOADER_ERROR_VERIFICATION_CONTEXT       (BOOTLOADER_ERROR_VERIFICATION_BASE | 0x04)

/// Storage errors
#define BOOTLOADER_ERROR_STORAGE_BASE               0x0400
/// Invalid slot
#define BOOTLOADER_ERROR_STORAGE_INVALID_SLOT       (BOOTLOADER_ERROR_STORAGE_BASE | 0x01)
/// Invalid address
#define BOOTLOADER_ERROR_STORAGE_INVALID_ADDRESS    (BOOTLOADER_ERROR_STORAGE_BASE | 0x02)
/// The storage area needs to be erased before it can be used
#define BOOTLOADER_ERROR_STORAGE_NEEDS_ERASE        (BOOTLOADER_ERROR_STORAGE_BASE | 0x03)
/// The address or length needs to be aligned
#define BOOTLOADER_ERROR_STORAGE_NEEDS_ALIGN        (BOOTLOADER_ERROR_STORAGE_BASE | 0x04)

/** @} addtogroup ErrorCodes */

// --------------------------------
// Bootloader capabilities

/// Bootloader has the capability of parsing EBL files
#define BOOTLOADER_CAPABILITY_EBL             (1 << 0)
/// Bootloader has the capability of parsing signed EBL files
#define BOOTLOADER_CAPABILITY_EBL_SIGNATURE   (1 << 1)
/// Bootloader has the capability of parsing encrypted EBL files
#define BOOTLOADER_CAPABILITY_EBL_ENCRYPTION  (1 << 2)

/// @brief Bootloader has the capability of verifying a signed application image
/// before boot
#define BOOTLOADER_CAPABILITY_SECURE_BOOT     (1 << 4)

/// Bootloader has the capability of being upgraded
#define BOOTLOADER_CAPABILITY_UPGRADE         (1 << 5)

/// @brief Bootloader has the capability of storing data in an internal or external
/// storage medium
#define BOOTLOADER_CAPABILITY_STORAGE         (1 << 8)
/// @brief Bootloader has the capability of communicating with host processors using
/// a communication interface
#define BOOTLOADER_CAPABILITY_COMMUNICATION   (1 << 12)

// --------------------------------
// Magic constants for bootloader tables

/// Magic word indicating first stage bootloader table
#define BOOTLOADER_MAGIC_FIRST_STAGE          (0xB00710AD)
/// Magic word indicating second stage bootloader table
#define BOOTLOADER_MAGIC_SECOND_STAGE         (0x5ECDB007)

// --------------------------------
// Bootloader table access

#define BTL_FIRST_BOOTLOADER_TABLE_BASE     (BTL_FIRST_STAGE_BASE + sizeof(BareBootTable_t))
#define BTL_SECOND_BOOTLOADER_TABLE_BASE    (BTL_SECOND_STAGE_BASE + sizeof(BareBootTable_t))

#define BTL_FBT   ((FirstBootloaderTable_t *)BTL_FIRST_BOOTLOADER_TABLE_BASE)
#define BTL_SBT   ((SecondBootloaderTable_t *)BTL_SECOND_BOOTLOADER_TABLE_BASE)

// --------------------------------
// Functions

/***************************************************************************//**
 * Get information about the bootloader on this device.
 *
 * The information returned is fetched from the second stage bootloader
 * information table
 *
 * @param[out] info Pointer to bootloader information struct.
 ******************************************************************************/
void bootloader_getInfo(BootloaderInformation_t *info);

/***************************************************************************//**
 * Initialize components of the bootloader that need to be initialized
 * for the app to use the interface. This typically includes initializing
 * serial peripherals for communication with external SPI flashes, etc.
 *
 * @return Error code. @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_INIT_BASE range.
 ******************************************************************************/
int32_t bootloader_init(void);

/***************************************************************************//**
 * De-initialize components of the bootloader that were previously initialized.
 * This typically includes powering down external SPI flashes, and de-initializing
 * the serial peripheral used for communication with the external flash.
 *
 * @return Error code. @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_INIT_BASE range.
 ******************************************************************************/
int32_t bootloader_deinit(void);

/***************************************************************************//**
 * Reboot into the bootloader, with the purpose of installing something.
 * 
 * If a storage plugin is present, and a slot is marked for bootload, install
 * the image in that slot after verifying it.
 *
 * If a communication plugin is present, open the communication channel and receive
 * an image to be installed.
 ******************************************************************************/
void bootloader_rebootAndInstall(void);

/** @} // addtogroup CommonInterface */
/** @} // addtogroup Interface */

#endif // BTL_INTERFACE_H