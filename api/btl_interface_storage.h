/***************************************************************************//**
 * @file btl_interface_storage.h
 * @brief Application interface to the storage plugin of the bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef BTL_INTERFACE_STORAGE_H
#define BTL_INTERFACE_STORAGE_H

#include BTL_CONFIG_FILE

#ifdef BOOTLOADER_SUPPORT_STORAGE

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/***************************************************************************//**
 * @addtogroup Interface
 * @{
 * @addtogroup StorageInterface Application Storage Interface
 * @brief Application interface for interfacing with the bootloader storage.
 * @details The Storage Interface is only available on bootloaders that declare
 *          they support @ref BOOTLOADER_CAPABILITY_STORAGE.
 * @{
 ******************************************************************************/

// -----------------------------------------------------------------------------
// Typedefs

/// Context for the bootloader image verification routine.
/// If allocated by the application for use with the bootloader application API,
/// at least @ref BTL_STORAGE_VERIFICATION_CONTEXT_SIZE bytes need to be allocated.
typedef struct BootloaderVerificationContext BootloaderVerificationContext_t;

/// Possible storage types
typedef enum {
  /// Storage backend is a SPI flash
  SPIFLASH,
  /// Storage backend is internal flash
  INTERNAL_FLASH,
  /// Storage backend is custom
  CUSTOM
} BootloaderStorageType_t;

/// Information about a storage slot
typedef struct {
  /// Address of the slot.
  uint32_t address;
  /// Size of the slot.
  uint32_t length;
} BootloaderStorageSlot_t;

/// Information about the bootlaoder storage implementation
typedef struct {
  /// The version of this data structure
  uint16_t version;
  /// A bitmask describing the capabilites of this particular storage
  uint16_t capabilitiesMask;
  /// Maximum time it takes to erase a page. (in milliseconds)
  uint32_t pageEraseMs;
  /// Maximum time it takes to erase the entire part. (in milliseconds)
  uint32_t partEraseMs;
  /// The size of a single erasable page in bytes
  uint32_t pageSize;
  /// The total size of the storage in bytes
  uint32_t partSize;
  /// Pointer to a string describing the attached storage
  const char * const partDescription;
  /// The number of bytes in a word for the storage
  uint8_t wordSizeBytes;
} BootloaderStorageImplementationInformation_t;

/// Information about the bootloader storage
typedef struct {
  /// The version of this data structure
  uint32_t version;
  /// The capabilities of the storage plugin
  uint32_t capabilities;
  /// Type of storage
  BootloaderStorageType_t storageType;
  /// Number of storage slots
  uint32_t numStorageSlots;
  /// Detailed information about the attached storage
  const BootloaderStorageImplementationInformation_t *info;
} BootloaderStorageInformation_t;

/// Storage API accessible from the application
typedef struct BootloaderStorageFunctions {
  /// Get information about the storage -- capabilities, layout, configuration
  void (*getInfo)(BootloaderStorageInformation_t *info);
  /// Get information about storage slot -- size, location
  int32_t (*getSlotInfo)(uint32_t slotId, BootloaderStorageSlot_t *slot);
  /// Read bytes from slot into buffer
  int32_t (*read)(uint32_t slotId, uint32_t offset, uint32_t *buffer, size_t length);
  /// Write bytes from buffer into slot
  int32_t (*write)(uint32_t slotId, uint32_t offset, uint32_t *buffer, size_t length);
  /// Erase an entire slot
  int32_t (*erase)(uint32_t slotId);
  // ------------------------------
  /// Mark a slot for bootload
  int32_t (*setAppImageToBootload)(int slotId);
  /// Mark two slots for bootloader + app upgrade
  int32_t (*setBootloaderImageToBootload)(int bootloaderSlotId, int appSlotId);
  // ------------------------------
  /// Start image verification
  int32_t (*initVerifyImage)(uint32_t slotId, BootloaderVerificationContext_t *context, size_t contextSize);
  /// Continue image verification
  int32_t (*verifyImage)(BootloaderVerificationContext_t *context);
  /// Check whether the bootloader storage is busy
  bool (*isBusy)(void);
#ifdef BOOTLOADER_SUPPORT_RAW_STORAGE
  /// Read raw bytes from storage
  int32_t (*readRaw)(uint32_t address, uint8_t *buffer, size_t length);
  /// Write bytes to raw storage
  int32_t (*writeRaw)(uint32_t address, uint8_t *buffer, size_t length);
  /// Erase storage
  int32_t (*eraseRaw)(uint32_t address, size_t length);
#endif
} BootloaderStorageFunctions_t;

// -----------------------------------------------------------------------------
// Defines

/// Current version of the BootloaderStorageImplementationInformation_t struct
#define BOOTLOADER_STORAGE_IMPL_INFO_VERSION                    (0x0201)
/// Current major version of the BootloaderStorageImplementationInformation_t struct
#define BOOTLOADER_STORAGE_IMPL_INFO_VERSION_MAJOR              (0x0200)
/// Major version mask for @ref BOOTLOADER_STORAGE_IMPL_INFO_VERSION
#define BOOTLOADER_STORAGE_IMPL_INFO_VERSION_MAJOR_MASK         (0xFF00)

/// Spiflash capability indicating that it supports erase
#define BOOTLOADER_STORAGE_IMPL_CAPABILITY_ERASE_SUPPORTED      (1 << 0)
/// @brief Spiflash capability indicating it requires full page erases before
/// new data can be written
#define BOOTLOADER_STORAGE_IMPL_CAPABILITY_PAGE_ERASE_REQUIRED  (1 << 1)
/// Spiflash capability indicating that the write function is blocking
#define BOOTLOADER_STORAGE_IMPL_CAPABILITY_BLOCKING_WRITE       (1 << 2)
/// Spiflash capability indicating that the erase function is blocking
#define BOOTLOADER_STORAGE_IMPL_CAPABILITY_BLOCKING_ERASE       (1 << 3)

// -----------------------------------------------------------------------------
// Functions

/***************************************************************************//**
 * Get information about the storage plugin.
 *
 * @param[out] info Information about the storage plugin.
 ******************************************************************************/
void bootloader_getStorageInfo(BootloaderStorageInformation_t *info);

/***************************************************************************//**
 * Get information about a storage slot.
 *
 * @param[in]  slotId ID of the slot to get information about
 * @param[out] slot Information about the storage slot.
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t bootloader_getStorageSlotInfo(uint32_t slotId,
                                      BootloaderStorageSlot_t *slot);

/***************************************************************************//**
 * Read data from a storage slot.
 *
 * @param[in]  slotId  ID of the slot
 * @param[in]  offset Offset into the slot to start reading from
 * @param[out] buffer Buffer to store the data
 * @param[in]  length How much data to read
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t bootloader_readStorage(uint32_t slotId,
                               uint32_t offset,
                               uint32_t *buffer,
                               size_t length);

/***************************************************************************//**
 * Write data to a storage slot.
 *
 * @param[in] slotId ID of the slot
 * @param[in] offset Offset into the slot to start writing to
 * @param[in] buffer Buffer to read data to write from
 * @param[in] length How much data to write
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t bootloader_writeStorage(uint32_t slotId,
                                uint32_t offset,
                                uint32_t *buffer,
                                size_t length);

/***************************************************************************//**
 * Erase all contents of a storage slot.
 *
 * @param[in] slotId ID of the slot
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t bootloader_eraseStorageSlot(uint32_t slotId);

/***************************************************************************//**
 * Mark a storage slot for bootload. The last call to this function
 * determines which slot will be installed when @ref bootloader_rebootAndInstall
 * is called. Call with slotId -1 to mark no image for bootload.
 *
 * @note If another slot has been set previously, it has to be explicitly
 *       unset using bootloader_setAppImageToBootload(-1) before another slot
 *       can be registered.
 *
 * @param[in] slotId ID of the slot
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t bootloader_setAppImageToBootload(int slotId);

/***************************************************************************//**
 * Mark two storage slots for bootload of new bootloader and app. The last call
 * to this function determines which slots will be installed when
 * @ref bootloader_rebootAndInstall is called. Call with slotId -1 to mark no
 * image for bootload.
 *
 * @note If another slot has been set previously, it has to be explicitly
 *       unset using bootloader_setAppImageToBootload(-1) before another slot
 *       can be registered.
 *
 * @param[in] bootloaderSlotId ID of the slot for bootloader upgrade
 * @param[in] appSlotId        ID of the slot for app upgrade
 *
 * @return @ref BOOTLOADER_OK on success, else error code in
 *         @ref BOOTLOADER_ERROR_STORAGE_BASE range
 ******************************************************************************/
int32_t bootloader_setBootloaderImageToBootload(int bootloaderSlotId,
                                                int appSlotId);

/***************************************************************************//**
 * Verify that the image in the given storage slot is valid
 *
 * @param[in] slotId Id of the slot to check
 *
 * @return @ref BOOTLOADER_OK on if the image is valid, else error code in
 *         @ref BOOTLOADER_ERROR_VERIFICATION_BASE range
 ******************************************************************************/
int32_t bootloader_verifyImage(uint32_t slotId);

/***************************************************************************//**
 * Check whether the bootloader storage is busy
 *
 * @return True if the storage is busy
 ******************************************************************************/
bool bootloader_storageIsBusy(void);


#ifdef BOOTLOADER_SUPPORT_RAW_STORAGE
int32_t bootloader_readRawStorage(uint32_t address,
                                  uint8_t *buffer, 
                                  size_t length);

int32_t bootloader_writeRawStorage(uint32_t address,
                                   uint8_t *buffer,
                                   size_t length);

int32_t bootloader_eraseRawStorage(uint32_t address, size_t length);
#endif // BOOTLOADER_SUPPORT_RAW_STORAGE
#endif // BOOTLOADER_SUPPORT_STORAGE

/** @} // addtogroup StorageInterface */
/** @} // addtogroup Interface */

#endif // BTL_INTERFACE_STORAGE_H