/***************************************************************************//**
 * @file btl_interface.c
 * @brief Application interface to the bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "btl_interface.h"

void bootloader_getInfo(BootloaderInformation_t *info)
{
  if (BTL_SBT->type == BOOTLOADER_MAGIC_SECOND_STAGE) {
    info->type = SL_BOOTLOADER;
    info->version.major = BTL_SBT->version.major;
    info->version.minor = BTL_SBT->version.minor;
    info->version.patch = BTL_SBT->version.patch;
    info->version.customer = BTL_SBT->version.customer;
    info->capabilities = BTL_SBT->capabilities;
  } else {
    info->type = NONE;
    info->capabilities = 0;
  }
}

int32_t bootloader_init(void)
{
  return BTL_SBT->init();
}

int32_t bootloader_deinit(void)
{
  return BTL_SBT->deinit();
}

void bootloader_rebootAndInstall(void)
{
  // Clear resetcause
  RMU->CMD = RMU_CMD_RCCLR;

  // Set reset reason to bootloader entry
  BootloaderResetCause_t* resetCause = (BootloaderResetCause_t*) (RAM_MEM_BASE);
  resetCause->reason = BOOTLOADER_RESET_REASON_BOOTLOAD;
  resetCause->signature = BOOTLOADER_RESET_SIGNATURE_VALID;

  // Trigger a software system reset
  RMU->CTRL = (RMU->CTRL & ~_RMU_CTRL_SYSRMODE_MASK) | RMU_CTRL_SYSRMODE_FULL;
  NVIC_SystemReset();
}
