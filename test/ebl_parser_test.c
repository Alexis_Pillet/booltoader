/***************************************************************************//**
 * @file btl_second_stage.c
 * @brief Main file for Second Stage Bootloader.
 * @author Silicon Labs
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "api/btl_interface.h"

#include "core/btl_util.h"
#include "plugin/debug/btl_debug.h"

#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"

// Get EBL array
#include "ebl/test.c"
#include "plugin/parser/ebl/btl_ebl_parser.h"

// Get size_t
#include <stddef.h>

#include <string.h>
#include <stdio.h>

char stringBuffer[20];

void parseMetadata(uint32_t offset, uint8_t* buffer, size_t length, void *cbContext)
{
  (void) offset;
  (void) buffer;
  (void) cbContext;
  sprintf(stringBuffer, "%d\n", length);
  BTL_DEBUG_PRINT("Metadata length ");
  BTL_DEBUG_PRINT(stringBuffer);
}

void parseImageData(uint32_t address, uint8_t* buffer, size_t length, BtlWriteMode_t mode, void *cbContext)
{
  (void) buffer;
  (void) mode;
  (void) address;
  (void) length;
  (void) cbContext;
/*
  BTL_DEBUG_PRINT("Image data: @0x");
  sprintf(stringBuffer, "%8lx ", address);
  BTL_DEBUG_PRINT(stringBuffer);
  sprintf(stringBuffer, "length %d\n", length);
  BTL_DEBUG_PRINT(stringBuffer);
*/
}

void testEblParser(void) 
{
  // Test the EBL parser with a file that has been compiled in
  size_t offset = 0;
  uint32_t retval = 0;
  EblParserContext_t parserContext;
  BtlImageDataCallbacks_t parserCallbacks = {NULL,parseImageData,parseMetadata};
  BtlImageProperties_t imageProperties = {0,false,false,false,false};
  btl_startParseImage((void*)&parserContext);
  
  while(offset < sizeof(dataArray)) {
    if(offset < sizeof(dataArray) - 64) {
      retval = btl_parseImageData((void*)&parserContext, &imageProperties, (uint8_t*)&(dataArray[offset]), 64, &parserCallbacks);
      offset += 64 / sizeof(dataArray[0]);
    } else {
      retval = btl_parseImageData((void*)&parserContext, &imageProperties, (uint8_t*)&(dataArray[offset]), 4, &parserCallbacks);
      offset += 4 / sizeof(dataArray[0]);
    }
    
    if(retval != 0) {
      BTL_DEBUG_PRINT("Error: 0x");
      sprintf(stringBuffer, "%8lx\n", retval);
      BTL_DEBUG_PRINT(stringBuffer);
      break;
    } else if(imageProperties.imageCompleted) {
      BTL_DEBUG_PRINT("Completed!\n");
      if(imageProperties.imageVerified) {
        BTL_DEBUG_PRINT("Verified!\n");
      }
      break;
    } else {
    }
  }
  BTL_DEBUG_PRINT("Done!\n");
}

int main(void)
{
  BTL_DEBUG_INIT();
  BTL_DEBUG_PRINT("Test init\n");

  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);

  GPIO_PinModeSet(gpioPortF, 5, gpioModePushPull, 1);
  
  testEblParser();

  while (1) {
    for (volatile int i = 0; i < 1000000; i++);
    GPIO_PinOutClear(gpioPortF, 5);
    for (volatile int i = 0; i < 1000000; i++);
    GPIO_PinOutSet(gpioPortF, 5);
  }
}

const SecondBootloaderTable_t secondBootloaderTable SL_ATTRIBUTE_SECTION(".boot_table") = {
  .type = 0x5ecdb007UL,
  .version = {
    BOOTLOADER_VERSION_SECOND_STAGE_MAJOR,
    BOOTLOADER_VERSION_SECOND_STAGE_MINOR,
    BOOTLOADER_VERSION_SECOND_STAGE_PATCH,
    BOOTLOADER_VERSION_SECOND_STAGE_CUSTOMER
  },
  .size = BTL_SECOND_STAGE_SIZE,
  .appStart = (void *)(BTL_SECOND_STAGE_BASE + BTL_SECOND_STAGE_SIZE),
  .endOfAppSpace = (void *)(BTL_APP_SPACE_BASE + BTL_APP_SPACE_SIZE)
  // TODO: Add pointer to function table
};
